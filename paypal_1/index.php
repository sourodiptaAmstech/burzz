<style>
 html, body {
    font-family: Verdana,sans-serif;
    font-size: 15px;
    line-height: 1.5;
}
/*.container{
	padding: 20px;
}
.container > h1{
	text-align: center;
	font-size: 2em;
	color: #888;
	font-weight: 600;
}*/
.subs-box{
	padding: 16px;
	color: #000;
    background-color: #f1f1f1;
	box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
	width: 350px;
	margin: auto;
}
.form-group {
    margin-bottom: 1rem;
}
.form-group label {
    display: inline-block;
    margin-bottom: .5rem;
}
.form-group select {
    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.form-group select:focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0,123,255,.25);
}
.buy-btn{
	display: inline-block;
    font-weight: 400;
    color: #000080;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	cursor: pointer;
}
.buy-btn {
    color: #fff;
    background-color: #3387E5;
    border-color: #3387E5;
}
.buy-btn:hover {
    color: #fff;
    background-color: #3387E5;
    border-color: #3387E5;
}

.status{
	padding: 15px;
	color: #000;
    background-color: #f1f1f1;
	box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
	margin-bottom: 20px;
}
.status h1{
	font-size: 1.5em;
}
.status h4{
	font-size: 1.2em;
	margin-bottom: 0;
}
.status p{
	font-size: 1em;
	margin-bottom: 0;
    margin-top: 8px;
}
.btn-link{
	display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	text-decoration: none;
}
.btn-link {
    color: #007bff;
    background-color: transparent;
    border-color: #007bff;
}
.btn-link:hover, .btn-link:active, .btn-link:focus {
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
	text-decoration: none;
}
.success{
	color: #34A853;
}
.error{
	color: #EA4335;
}
</style>
<?php 
// Include configuration file  
include_once 'config.php';  
  
// Include database connection file  
include_once 'dbConnect.php'; 
 
// Start session 
session_start(); 
 
// Get logged-in user ID from sesion 
// Session name need to be changed as per your system 
$loggedInUserID = !empty($_SESSION['userID'])?$_SESSION['userID']:0; 
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="subs-box">
				<div class="form-group">
					<label>Subscription Validity:</label>
					<select name="validity" onchange="getSubsPrice(this);">
						<option value="1" selected="selected">1 Month</option>
						<option value="3">3 Month</option>
						<option value="6">6 Month</option>
						<option value="9">9 Month</option>
						<option value="12">12 Month</option>
					</select>
				</div>
				<div class="form-group">
					<p><b>Total Price:</b> <span id="subPrice">$25 USD</span></p>
				</div>
				
				<!-- Buy button -->
				<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
					<!-- Identify your business so that you can collect the payments -->
					<input type="hidden" name="business" value="www.burzz.online">
					<!-- Specify a subscriptions button. -->
					<input type="hidden" name="cmd" value="_xclick-subscriptions">
					<!-- Specify details about the subscription that buyers will purchase -->
					<input type="hidden" name="item_name" value="Membership Subscription">
					<input type="hidden" name="item_number" value="MS123456">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="a3" id="paypalAmt" value="25">
					<input type="hidden" name="p3" id="paypalValid" value="1">
					<input type="hidden" name="t3" value="M">
					<!-- Custom variable user ID -->
					<input type="hidden" name="custom" value="1">
					<!-- Specify urls -->
					<input type="hidden" name="cancel_return" value="http://dev.appsquadz.com/paypal_1/cancel.php">
					<input type="hidden" name="return" value="http://dev.appsquadz.com/paypal_1/success.php">
					<input type="hidden" name="notify_url" value="http://dev.appsquadz.com/paypal_1/paypal_ipn.php">
					<!-- Display the payment button -->
					<input class="buy-btn" type="submit" value="Buy Subscription">
				</form>
			</div>
            </div>
        </div>
    </div>
</section>


<script>
function getSubsPrice(obj){
	var month = obj.value;
	var price = (month * <?php echo $itemPrice; ?>);
	document.getElementById('subPrice').innerHTML = '$'+price+' USD';
	document.getElementById('paypalValid').value = month;
	document.getElementById('paypalAmt').value = price;
}
</script>