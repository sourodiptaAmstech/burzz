<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");

$checkSum = "";
$paramList = array();
$REFID = time();
// Create an array having all required parameters for creating checksum.
$paramList["MID"] = $_GET['MID'];
$paramList["ORDERID"] = $_GET['ORDERID']; //get during paytm transaction response
$paramList["TXNTYPE"] = 'REFUND';
$paramList["REFUNDAMOUNT"] = $_GET['REFUNDAMOUNT'];
$paramList["TXNID"] = $_GET['TXNID']; // get during paytm transaction response
$paramList["REFID"] = "$REFID"; // ref id should be unique every time.

//echo "<pre>";

//Here checksum string will return by getChecksumFromArray() function.
$checkSum = getRefundChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);

$paramList["CHECKSUM"] = urlencode($checkSum);

$data_string = 'JsonData='.json_encode($paramList);
                         // initiate curl
$ch = curl_init();                    
//$url = PAYTM_REFUND_URL;
$url = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/REFUND?";


curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string); // define what you want to post
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
$headers = array();
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$output = curl_exec ($ch); // execute
 $info = curl_getinfo($ch);
//echo $output;

$data = json_decode($output, true);
echo json_encode($data);