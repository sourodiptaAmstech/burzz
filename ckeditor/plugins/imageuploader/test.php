<?php
error_reporting(E_ALL);
ob_start();
include 'index.php';
ob_end_clean();
$CI = &get_instance();
echo base_url;

//echo FCPATH;
include '../../../aws/aws-autoloader.php';
use Aws\S3\S3Client;
//echo '<br>';
$CI->load->library('session'); //if it's not autoloaded in your CI setup
print_r($CI->session->userdata());

amazon_s3_upload('$name', '$aws_path');

function amazon_s3_upload($name, $aws_path) {

	$_FILES['file'] = $name;
	//include base_url . '/aws/aws-autoloader.php';

	$s3Client = new S3Client([
		'version' => 'latest',
		'region' => 'ap-south-1',
		'credentials' => [
			'key' => AMS_S3_KEY,
			'secret' => AMS_SECRET,
		],
	]);
	$result = $s3Client->putObject(array(
		'Bucket' => AMS_BUCKET_NAME,
		'Key' => $aws_path . '/' . rand(0, 7896756) . str_replace([':', ' ', '/', '*', '#', '@', '%'], "_", $_FILES["file"]['name']),
		'SourceFile' => $_FILES["file"]['tmp_name'],
		'ContentType' => 'image',
		'ACL' => 'public-read',
		'StorageClass' => 'REDUCED_REDUNDANCY',
		'Metadata' => array('param1' => 'value 1', 'param2' => 'value 2'),
	));
	$data = $result->toArray();
	return $data['ObjectURL'];
}
?>