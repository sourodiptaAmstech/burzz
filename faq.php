<?php
// Here is the data we will be sending to the service

$curl = curl_init();
// You can also set the URL you want to communicate with by doing this:
// $curl = curl_init('http://localhost/echoservice');

// We POST the data
curl_setopt($curl, CURLOPT_POST, 1);
// Set the url path we want to call
curl_setopt($curl, CURLOPT_URL, 'http://3.215.188.72/index.php/data_model/Static_pages/get_faqs');
// Make it so the data coming back is put into a string
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

// You can also bunch the above commands into an array if you choose using: curl_setopt_array

// Send the request
$result = curl_exec($curl);
// Free up the resources $curl is using
curl_close($curl);

$info = json_decode($result)->data;
// echo '<pre>';
// print_r($info);
// echo '</pre>';
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<style type="text/css">
    /* accordion css */

.accordion-container > h2{
    text-align: center;
    color: #fff;
    padding-bottom: 5px;
    margin-bottom: 20px;
    padding-bottom: 15px;
    border-bottom: 1px solid #ddd;
}
.set{
    position: relative;
    width: 100%;
    height: auto;
    /* background-color: #f5f5f5; */
}
.set > a{
    display: block;
    padding: 10px 15px;
    text-decoration: none;
    color: #000;
    font-weight: 600;
    border-bottom: 1px solid #ddd;
    -webkit-transition:all 0.2s linear;
    -moz-transition:all 0.2s linear;
    transition:all 0.2s linear;
}
.row{
    margin-left: 0;
    margin-right: 0;
}
.set > a i{
    float: right;
    margin-top: 2px;
}
.content{
    background-color: #fff;
    border-bottom: 1px solid #ddd;
    display:none;
    padding: 5px 0;
}

.content p{
    padding: 10px 15px;
    margin: 0;
    color: #333;
}
</style>
<body>
    <section>
        <div class="row">
            <div class="col-12">
                <div class="accordion-container">
                 <?php foreach ($info as $faq) {?>
                    <div class="set">
                    <a class="faq" href="JavaScript:Void(0);"><?php echo $faq->question;?><i class="fa fa-angle-down"></i></a>
                    <div class="content">
                        <p><?php echo $faq->description;?></p>
                    </div>
                    </div>
                  <?php }?>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $(".set > a").on("click", function() {
                if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".content")
                    .slideUp(200);
                $(".set > a i")
                    .removeClass("fa-angle-up")
                    .addClass("fa-angle-down");
                } else {
                $(".set > a i")
                    .removeClass("fa-angle-up")
                    .addClass("fa-angle-down");
                $(this)
                    .find("i")
                    .removeClass("fa-angle-down")
                    .addClass("fa-angle-up");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp(200);
                $(this)
                    .siblings(".content")
                    .slideDown(200);
                }
            });
        });
        $(".faq").on("click",function(e){
    e.preventDefault();
});
    </script>
</body>
</html>