<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');


if (!function_exists('pre')) {
	function pre($array) {
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
}

if (!function_exists('is_json')) {

    function is_json($string, $return_data = false) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

}




if (!function_exists('get_setting_constants')) {

    function get_setting_constants($array=array()) {        
        $CI =& get_instance();
        $CI->load->database();
        $final = array();
        if(isset($array['name']) && !empty($array['name'])){
          $CI->db->where('name',$array['name']);
        }
        $result = $CI->db->get('settings')->result();
        if($result){
          foreach ($result as $key => $value) {
              $final[$value->name] = $value->value;
           } 
        }
        return $final;
    }
}

if (!function_exists('get_lang_text')) {

    function get_lang_text($key,$lang="",$file="") {  
        $CI =& get_instance();       
        if(empty($lang)){
          $idoms    = 'english';
        }        
        if(empty($file)){
          $file    = 'data_model_api_response';
        }
        $CI->lang->load($file,$idoms);
        return $CI->lang->line($key);
    }
}


if (!function_exists('get_input_controls')) {

    function get_input_controls($filter=array()) {        
        $CI =& get_instance();
        $CI->load->database();
        if(isset($filter['select']) && !empty($filter['select']) ){
          $CI->db->select($filter['select']);
        }
        if(isset($filter['module']) && !empty($filter['module']) ){
          $CI->db->where('module',$filter['module']);
        }
        if(isset($filter['functionality']) && !empty($filter['functionality']) ){
          $CI->db->where('functionality',$filter['functionality']);
        }
        if(isset($filter['input_name']) && !empty($filter['input_name']) ){
          $CI->db->where('input_name',$filter['input_name']);
        }
        if(isset($filter['input_names']) && !empty($filter['input_names']) ){
          $CI->db->where_in('input_name',$filter['input_names']);
        }
        $input_control = $CI->db->get('settings_input_controls')->result();
        if($input_control){
          foreach ($input_control as $key => $value) { //pre($value);
                foreach ($value as $key1 => $value1) { //pre($value1);
                    $final[$value->input_name][$key1] = $value1;
                }
           }
           $input_control =  $final;
        }
        return $input_control;
    }
}



if (!function_exists('upload_file')) {
  function upload_file($file_array, $path, $validations=array()) {
      //$ext    = pathinfo($file_array['name'], PATHINFO_EXTENSION);
      $filename    = time().'_'.urlencode($file_array['name']);
      //$filename  = md5($img).'.'.$ext;
      if(move_uploaded_file($file_array['tmp_name'], $path.$filename) ) {
          return $filename;
      }
  }
}

if (!function_exists('insert_master_image')) {
  function insert_master_image($module,$module_id, $image) {
      $CI =& get_instance();
      $data = array("module"=>$module, "module_id" => $module_id, "image" => $image,'created_on'=>date('Y-m-d H:i:s'),'modified_on'=>date('Y-m-d H:i:s'));
      $CI->db->insert('master_images',$data);
      return true;
  }
}

if (!function_exists('add_address')) {
  function add_address($input) {
      $CI =& get_instance();
      if(isset($input['id'])&& !empty($input['id']) ){
          $input['modified']=time();
          $CI->db->where('id',$input['id']);
         $CI->db->update('address_master',$input);  
         return $input['id'];
      }else{
          $input['created']=time();
          $CI->db->insert('address_master',$input);  
          return $CI->db->insert_id();
      }      
      return true;
  }
}

if (!function_exists('delete_attached_address')) {
   function delete_attached_address($data) {
        $CUSTOM =& get_instance();
        $query = "DELETE "
        . "FROM `address_master` "
        . "WHERE `address_master`.`id` = (SELECT `address` "
                                            . "FROM `".$data['table']."` "
                                            . "WHERE bank_details.id = ".$data['ref_id'].")"
        . "";
        $CUSTOM->db->query($query);
        return true;
    }
}



if (!function_exists('set_mapped_modules_session')) {

    function set_mapped_modules_session() { 
        
        $CI =& get_instance();
        $session_data = $CI->session->userdata;
        if(isset($session_data['active_backend_user_id'])){
            $CI->load->database();       
            $roles = $my_module = array();
            $buid = $CI->session->userdata['active_backend_user_id'];
            $CI->db->select( "module_type, group_concat(module_id) mids");
            $CI->db->where('backend_user_id',$buid);
            $CI->db->group_by('module_type');
            $mapping = $CI->db->get('backend_user_mapping')->result();
            foreach($mapping as $m){
                $roles[] = $m->module_type;
                $my_module[$m->module_type] = $m->mids;
            }
            $CI->session->set_userdata(array("user_roles"=>$roles));
            $CI->session->set_userdata(array("my_module"=>$my_module));
        }        
    }

}




if (!function_exists('check_inputs')) {

    function check_inputs() {
        $json = file_get_contents('php://input');
        if ($json) {
            return ((array) json_decode($json));
        } else if ($_POST) {
            //echo'dddd'.pre($_POST);
            return $_POST;
        } else {
            //pre($_POST);
            echo json_encode(array("status" => false, "message" => "Invalid Input", "Result" => array()));
        }
    }

}
if (!function_exists('create_log_file')) {

    function create_log_file($data) { //mobile and message.  
        $log = "Time: " . date('Y-m-d, H:i:s') . PHP_EOL;
        $log = $log . "url: " . base_url($data['url']) . PHP_EOL;
        $log = $log . "Request " . json_encode($data['request']) . PHP_EOL;
        if(isset($data['response'])){
          $log = $log . "Responce " . json_encode(array("Result" => $data['response'])) . PHP_EOL;
        }
        $log = $log .PHP_EOL . PHP_EOL;
        $filename[] = 'logs/' . $data['url'] . '.txt';
        $file_data = (file_exists($filename))?file_get_contents($filename):"";
        #file_put_contents($filename, $log);
        file_put_contents($filename, $log.$file_data) ;
        #file_put_contents('logs/' . $data['url'] . '.txt', $log, FILE_APPEND);
    }

}

if (!function_exists('return_data')) {

	function return_data($status=false,$message="",$data = array(),$error = ""){
		echo json_encode(array('status'=>$status,'message'=>$message,'data' => $data ,'error'=>$error)); 
		die;	
	}
}


if (!function_exists('post_check')){
	function post_check() {
		if ($_SERVER['REQUEST_METHOD'] != 'POST'){
			echo json_encode(array('status'=>false,'message'=>"Invalid input parameter.Please use post method.",'data' => array() ,'error'=>array())); 
			die;	
		}
		// put in redis channel 
		// $CI = & get_instance();
		// $array = array('api_info'=>'demo'); 
		// $array['url'] = current_url();
		// $array['hash'] = md5(current_url());
		// $CI->redis_magic->PUBLISH('API_LIVE_VIEW',json_encode($array));
		
	}
}

if (!function_exists('file_curl_contents')) {

    function file_curl_contents($document) {
        $CI = & get_instance();
        $header = array();
        if (array_key_exists("jwt",$document)){
          $header[] = 'Jwt:'.$document['jwt']; 
        }
        if($CI->session->userdata('jwt') != ""){
          $header[] = 'Jwt:'.$CI->session->userdata('jwt');
        }
        $header[] = 'Lang:1';
        $ch = curl_init();                                                             
        curl_setopt($ch, CURLOPT_URL, $document['file_url']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
        curl_setopt($ch, CURLOPT_POST, 1);
        unset($document['file_url']); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $document);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //pre($server_output);
        curl_close($ch);
        $data = json_decode($server_output, true);
        // print_r($data);exit;
        if(array_key_exists("auth_code",$data)){
          if($data['auth_code'] == '100100'){
             $CI->session->sess_destroy();
             redirect('/web/Home');   
             die;
          }
        }
        return $data;
    } 
}



if (!function_exists('call_server_api')) {

    function call_server_api($url,$document) {  //echo $url; pre($document);      
        $ch = curl_init();                                                             
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $document);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($server_output, true);
        return $data;
        //pre($server_output);
        //die('amitddddddddddddd');  
    } 
}


if (!function_exists('add_url_to_master')) {
  function add_url_to_master($array){
    $ci = & get_instance();
    if(!empty($array['element_type']) && !empty($array['url']) && !empty($array['element_id'])){
      $count_record =  $ci->db->where($array)->count_all_results('url_master');
      if($count_record == 0){
         $ci->db->insert('url_master', $array);
      }else{
         $ci->db->where('element_id',$array['element_id']);
         $ci->db->update('url_master', $array);
      } 
    }
  }
}

if (!function_exists('milliseconds')){
	function milliseconds() {
	    $mt = explode(' ', microtime());
	    return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
	}
}

if(!function_exists('is_comma_seprated')){
	function is_comma_seprated($string = "" , $return = "" ){
		
		if ($string != "" && count(explode(",",$string)) > 0) {
			if($return === True){
				return explode(',',$string);
			}
  			return true;
		}else{
			if($return === false){
				return array();
			}
			return false;			
		}
	}
}

if (!function_exists('send_email')) { //die('hello');
  function send_email($input) { //die('hello');
  //pre($input); die;
      if(isset($input['name'])){
        $name = $input['name'];
      }        
      $subject = "SNU EMAIL NOTIFICATION!";
      if(isset($input['subject'])){
        $subject = $input['subject'];
      }
      $message = "SNU EMAIL NOTIFICATION DESCRIPTION.";
      if(isset($input['message'])){
        $message = $input['message'];
      }
      $message.= '<p style="margin-top:100px;color:red;"><center>***Please do not reply to this mail.***</center></p>';

      require_once './phpmailerclass/class.phpmailer.php';             
      $mail               = new PHPMailer(true); 

      //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
      $mail->isSMTP(); // Set mailer to use SMTP
      $mail->Host         = 'relay.dev.webapps.snu.edu.in'; // Specify main and backup SMTP servers
      $mail->SMTPAuth     = false; // Enable SMTP authentication
      //$mail->Username = 'AKIA24WCBEZUTEIWBZRL'; //'AKIAI5E32DPBZY43A32A'; // SMTP username
      //$mail->Password = 'BLl27euKv4GA6+HH2KlHXQ1sTmWa7U4EQPWFXH3od2XI'; //'Ai0ae8B3hTr+L1g5zGVltQMYAVG7XYKUgNSpRMvc1oDv'; // SMTP password
      //$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
      $mail->Port         = 25; // TCP port to connect to

      //Recipients

      //$mail->setfrom('noreply@snu.edu.in', 'SNU');
      $mail->setfrom('noreply@snu.edu.in', 'SNU');
      $mail->addaddress(DEFAULT_MAIL_RECIEVING_TO_EMAIL_ID, DEFAULT_MAIL_RECIEVING_TO_NAME); // Add a recipient

      if(isset($input['email'])){
        $mail->addBCC($input['email']); 
      }
      if(isset($input['emails'])){
        foreach ($input['emails'] as $email) {
           $mail->addBCC($email); // Add a recipient
        }
      }     
      if(isset($input['attachments'])){
        foreach ($input['attachments'] as $attachment) {
           $mail->addAttachment($attachment);
        }
      }     
      //$mail->WordWrap     = 80; 
      $mail->Subject      = $subject;
      $mail->Body         = $message;
      $mail->AltBody      = $message;
      echo $mail->Send(); 
      echo "-";
  }
}



if (!function_exists('services_helper_user_basic')) {
	function services_helper_user_basic($user_id) {
		$CI = & get_instance();
		$CI->db->where('id', $user_id);
		return $CI->db->get('users')->row_array();
	}
}



if (!function_exists('notification_logger')) {
	function notification_logger($data) {
		$CI = & get_instance();
		$CI->db->insert('notification_logger',$data);
		return $CI->db->insert_id();
	}
}

//User activity rewards manipulation
if(!function_exists('activity_rewards')){
	function activity_rewards($user_id , $area,$do="+"){
		if ($user_id != "" && $area != ""){	
			$data['area'] = $area;
			// fetch points w.r.t. area 
			$points = get_db_meta_key($area); 
			/* on post like */
			if($area == "POST_LIKE" && $do == "-"){ $data['area'] = "POST_DISLIKE"; }
			//COMMENT_ADD
			if($area == "COMMENT_ADD" && $do == "-"){ $data['area'] = "COMMENT_DELETE"; }
			//USER_FOLLOW
			if($area == "USER_FOLLOW" && $do == "-"){ $data['area'] = "USER_UNFOLLOW"; }
			//COURSE_REVIEW_POINTS
			if($area == "COURSE_REVIEW" && $do == "-"){ $data['area'] = "COURSE_REVIEW_REMOVE"; }
			//COURSE_INSTRUTOR_REVIEW
			if($area == "COURSE_INSTRUCTOR_REVIEW" && $do == "-"){ $data['area'] = "COURSE_INSTRUCTOR_REVIEW_REMOVE"; }
			
			if($points > 0 ){
				$data['user_id'] = $user_id;
				$data['reward']  = ($do=="-")?'-'.$points:$points;
				$data['creation_time'] = milliseconds();
				$CI = & get_instance();
				$CI->db->insert('user_activity_rewards',$data);
				$CI->db->query("Update users set reward_points = (SELECT sum(reward) as total FROM `user_activity_rewards` WHERE user_id = $user_id) where id = $user_id");
			}
		}
	}
         if (!function_exists('send_sms')) {

//    function send_sms($data) { //mobile and message. 
//        //echo "working"; die; 
//          //$mobile = $data['mobile'];
////          $mobile = "+".$data['c_code'] . $data['mobile'];
//          $mobile = "+".$data['country_code'] . $data['mobile'];
//          $message = $data['message'];
//          require_once(APPPATH . 'third_party/twilio-php/Services/Twilio.php');
//          $account_sid = 'ACeb4944e7c6f1292d1e25feb7468dc56b';
//          $auth_token = 'b36be33c94aef32a2e15445f9daa9fd3';
//          $client = new Services_Twilio($account_sid, $auth_token); //echo  $mobile;
//          $message = $client->account->messages->create(array(
//          'To' => $mobile,
//          'From' => "+15164730342",
//          'Body' => $message));
//          if ($message->sid != '') {
//          return true;
//          } else {
//          return false;
//          }
//    }
//}
}
}
