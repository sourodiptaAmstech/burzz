<?php
 
 
   /*
      $input = array();
      $input['image_path'] = "URL"; // if image source is url
      $input['source'] = "url";   
      $input['quality'] = 100; // quality of image 0-100
      $input['widths'] = array('300','150',100);  
   */
   function resize_picture($input){
      /* get image */
      if($input['image_path'] == "URL"){
         $sTempFileName = $input['source'];
      }   
      $aSize = getimagesize($sTempFileName);
      /* quality of image */
      $image_ori_width = $aSize[0];
      $image_ori_height = $aSize[1];

      switch($aSize[2]) {
         case IMAGETYPE_JPEG:
         $sExt = '.jpg';
         $vImg = @imagecreatefromjpeg($sTempFileName);
         $info_source_base_64 = "data:image/jpeg;base64,";
         break;
         case IMAGETYPE_PNG:
         $sExt = '.png';
         $vImg = @imagecreatefrompng($sTempFileName);
         $info_source_base_64 = "data:image/png;base64,";
         break;
      }
      $return = array();
      $iJpgQuality = $input['quality'];
      /* required width */
      foreach($input['widths'] as $max_widths){
         $iWidth = $max_widths;
         $iHeight = $max_widths / ($image_ori_width/$image_ori_height); 
         // create a new truee color image
         $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
         // copy and resize part of an image with resampling
         $data = imagecopyresampled($vDstImg, $vImg, 0, 0, 0, 0, $iWidth, $iHeight, $image_ori_width, $image_ori_height);
         ob_start ();
         imagejpeg($vDstImg,null, $iJpgQuality);
         $image_data = ob_get_contents ();
         ob_end_clean ();
         $source_base_64 = $info_source_base_64.base64_encode($image_data);
         $return[$max_widths] = $source_base_64;
      }
      return $return;  
   }
   
   
   
    function featch($title,$id,$selected){
    	 $CI = & get_instance();
			  	$CI->db->select('id,title');
				$category2= $CI->db->get_where('master_category',array('parent_id'=>$id))->result_array();
					if(empty($category2))
					return;
					else
					foreach($category2 as $cat2){
						$s= $title.'>'.$cat2['title'].'<br>';
						echo"<option value='".$cat2['id']."' ". ($selected==$cat2['id']?'selected':'').">$s</option>";
					featch($s,$cat2['id'],$selected);	
					}
					
				}