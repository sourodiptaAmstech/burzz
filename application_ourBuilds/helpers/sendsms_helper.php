<?php
 function sendsms1($mobile,$messa){
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => "https://d7networks.com/api/verifier/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => array('mobile' => $mobile,'sender_id' => 'SMSINFO','message' => 'Your otp code for Burzz is {code}','expiry' => '900'),
  CURLOPT_HTTPHEADER => array(
    "Authorization: Token a0e9c109f3458145ded7bc29168a2448531ab248"
  ),
));

$response = curl_exec($curl);
curl_close($curl);
//$result   = json_decode($response);
return $response;
}
function verify_otp($otp_id,$otp_code){
//print_r($otp_id);
//print_r($otp_code);die;
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://d7networks.com/api/verifier/verify",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => array('otp_id' => $otp_id,'otp_code' => $otp_code),
  CURLOPT_HTTPHEADER => array(
    "Authorization: Token a0e9c109f3458145ded7bc29168a2448531ab248"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
}
function sendsms($mobile,$message){
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => "https://rest-api.d7networks.com/secure/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => json_encode(array('to' => $mobile,'from' => 'BURZZ',
                                       'content' => $message,
                                       'dlr' => 'yes',
                                       'dlr-method'=>'GET',
                                       'dlr-level'=>2,
                                       'dlr-url'=>'http://yourcustompostbackurl.com')),
    CURLOPT_HTTPHEADER => array(
            "Authorization: Basic c2ZndDU2NTk6a1RNRmlWWEE="
    ),
  ));
$response = curl_exec($curl);
//echo $response;
}
