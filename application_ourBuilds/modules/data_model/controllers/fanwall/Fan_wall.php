<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'third_party/firebase/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class Fan_wall extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		//$this->load->model("fan_wall_model");
		// get agent
		$this->load->library('user_agent');
		$serviceAccount = ServiceAccount::fromJsonFile(APPPATH . 'third_party/google-services.json');
		$firebase = (new Factory)
			->withServiceAccount($serviceAccount)
			->withDatabaseUri('https://burzz-3d854.firebaseio.com/')
			->create();
		$this->firebase = $firebase->getDatabase();
		$this->load->model('Swap_model');

	}
	public function getSwapLimit (){
		//find is already exist or not
		$details = $this->Swap_model->get_user($this->input->post('user_id'));

		$data['user_id'] = $this->input->post('user_id');
		$data['getToDaysCrossLimit'] = $this->input->post('getToDaysCrossLimit');
		$data['pageNo'] = $this->input->post('pageNo');
		$data['timeAndDate'] = $this->input->post('timeAndDate');
		$data['isSwap'] = false;

		if($details != ''){
			//update User
			$updateSwapLimit = $this->Swap_model->update_user($data);
		}else{
			$insertIntoDB = $this->Swap_model->save_otp($data);
		}

		return_data(true, 'Your swap limit is over for 24 hours.', array($data));
	}

	public function nextSwapIsAllowOrNot(){
		$user_id = $this->input->post('user_id');

		$getUserSwapDetail = $this->Swap_model->get_user($user_id);

		if($getUserSwapDetail != ''){
			//check date is cross 24hours or not

			$now = date_create(date("Y-m-d H:i:s"));

			$replydue = date_create($getUserSwapDetail['timeAndDate']);

			$timetoreply = date_diff($replydue, $now);

			$dayDiff = $timetoreply->d;

			if($dayDiff > 0){
				$data['isSwap'] = true;
				$data['pageNo'] = $getUserSwapDetail['pageNo'] + 1;

				return_data(false, 'You are allow for next swap.', array($data));
			}else{
				$data['isSwap'] = false;
				$data['pageNo'] = $getUserSwapDetail['pageNo'];
				return_data(false, 'You are not allow for next swap.', array($data));
			}

		}else{
			$data['isSwap'] = true;
			$data['pageNo'] =  1;
			return_data(true, 'You are allow for next swap.', array($data));
		}
	}
	public function get_fan_wall_for_user() {
		$this->validate_get_fan_wall_for_user();
		$request_user_id = $this->input->post('user_id');

		/* for pagination */
		if (!array_key_exists('last_post_id', $_POST)) {
			$last_post_id = "";
		} else {
			$last_post_id = $this->input->post('last_post_id');
		}
		/* get all posts */
		$post = array();
		$post_meta['post_data'] = "";
		$user_setting = $this->db->get_where('users', array('id' => $request_user_id))->row_array();
		$settings = $this->db->get_where('user_profile_detail', array('user_id' => $request_user_id))->row_array();

		$this->db->select('follow_id');
		$never_followed_ids = $this->db->get_where('never_followed', array('user_id' => $request_user_id))->result_array();
		$never_followed_ids = array_column($never_followed_ids, 'follow_id');

		$this->db->select('follow_id');
		$followed_ids = $this->db->get_where('followed', array('user_id' => $request_user_id))->result_array();
		$followed_ids = array_column($followed_ids, 'follow_id');
		$never_followed_ids = array_merge($followed_ids, $never_followed_ids);

		$this->db->select('blocked_id');
		$blocked_ids = $this->db->get_where('blocked', array('user_id' => $request_user_id))->result_array();
		$blocked_ids = array_column($blocked_ids, 'blocked_id');
		$blocked_ids = array_merge($never_followed_ids, $blocked_ids);
		$blocked_ids = (implode(',', $blocked_ids) == '' ? 0 : implode(',', $blocked_ids));

		if (!empty($settings)) {
			$where_new = "";
			if ($settings['interest'] == 1) {
				$where_new = " and  u.gender=" . $settings['interest_in'];
			}
			$sql = "SELECT
    upd.*,u.name,u.profile_picture,u.gender,u.location,u.latitude,u.longitude,u.dob,u.instagram,u.quickblox_id,u.spotify,
    round (( 3959 * acos ( cos ( radians(" . $user_setting['latitude'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $user_setting['longitude'] . ") ) + sin ( radians(" . $user_setting['latitude'] . ") ) * sin( radians( latitude ) ) ) )) AS distance,
    (SELECT title FROM zodiac WHERE id = upd.zodiac) AS zodiac_text,
    (SELECT title FROM politics WHERE id = upd.politics) AS politics_text,
    (SELECT title FROM drinking WHERE id = upd.drinking) AS drinking_text,
    (SELECT title FROM education_level WHERE id = upd.education_level) AS education_level_text,
    (SELECT title FROM exercise WHERE id = upd.exercise) AS exercise_text,
    (SELECT title FROM industry WHERE id = upd.industry) AS industry_text,
    (SELECT title FROM kids WHERE id = upd.kids) AS kids_text,
    (SELECT title FROM looking_for WHERE id = upd.looking_for) AS looking_for_text,
    (SELECT title FROM looking_for_d WHERE id = upd.looking_for_d) AS looking_for_d_text,
    (SELECT title FROM looking_for_f WHERE id = upd.looking_for_f) AS looking_for_f_text,
    (SELECT title FROM new_to_area WHERE id = upd.new_to_area) AS new_to_area_text,
    (SELECT title FROM pets WHERE id = upd.pets) AS pets_text,
    (SELECT title FROM releationship WHERE id = upd.releationship) AS releationship_text,
    (SELECT title FROM religion WHERE id = upd.religion) AS religion_text,
    (SELECT title FROM smoking WHERE id = upd.smoking) AS smoking_text
FROM
    users AS u,
    user_profile_detail AS upd
WHERE
    u.id = upd.user_id AND
u.id NOT IN ($blocked_ids) and
 u.id <> $request_user_id $where_new and interest=" . $settings['interest'];// . " and
    //round (( 3959 * acos ( cos ( radians(" . $user_setting['latitude'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $user_setting['longitude'] . ") ) + sin ( radians(" . $user_setting['latitude'] . ") ) * sin( radians( latitude ) ) ) ))  < " . $settings['distance'];
			$users = $this->db->query($sql)->result_array();

			//implement pagination
			$page = ! empty ($this->input->post('page')) ? (int) $this->input->post('page') : 1;
			$total = count($users);
			$limit = 20;
			$totalPages = ceil($total / $limit);
			$page = max($page, 1);
			$page = min($page , $totalPages);
			$offset = ($page - 1) * $limit;
			if($offset < 0) {
				$offset = 0;
			}
			$users = array_slice($users, $offset, $limit);
			//end 


			if (empty($users)) {
				return_data(false, 'No User Found', array('users' => $users));
			}
			$response = [];
			$i = 0;
			$this->db->select('icon');
			$master = $this->db->get('master_relation')->row_array();
			$master = array_column($master, 'icon');
			foreach ($users as $user) {

				$type = $this->input->post('type');//$user['interest'];
				$preference=array();
				$masters = $this->db->query("SELECT master_table,title,icon FROM `master_relation` WHERE find_in_set($type ,type)")->result_array();
				foreach ($masters as $master) {
					$table = $master['master_table'] . '_text';

				//	$preference[] = array('text' => ($user[$table] == NULL ? '' : $user[$table]), 'icon' => base_url('icon/' . $master['icon']));

				

				if ($user['interest'] == 1) {
					$preference[] = array('text' => ($user[$table] == NULL ? '' : $user[$table]), 'icon' => base_url('icon/' . $master['icon']));
				//	$preference[] = array('text' => ($user['looking_for_text'] == NULL ? '' : $user['looking_for_text']), 'icon' => base_url('icon/' . $master['icon']));

				}
				if ($user['interest'] == 2) {
					$preference[] = array('text' => ($user[$table] == NULL ? '' : $user[$table]), 'icon' => base_url('icon/' . $master['icon']));
					//$preference[] = array('text' => ($user['looking_for_text'] == NULL ? '' : $user['looking_for_text']), 'icon' => base_url('icon/' . $master['icon']));

				}
				if ($user['interest'] == 3) {
					$preference[] = array('text' => ($user[$table] == NULL ? '' : $user[$table]), 'icon' => base_url('icon/' . $master['icon']));
					//$preference[] = array('text' => ($user['looking_for_text'] == NULL ? '' : $user['looking_for_text']), 'icon' => base_url('icon/' . $master['icon']));

				}
			}

				$new_data['preference'] = array_values($preference);

				$new_data['user_id'] = $user['user_id'];
				$new_data['instagram'] = $user['instagram'];
				$new_data['quickblox_id'] = $user['quickblox_id'];

				$new_data['spotify'] = $user['spotify'];
				$new_data['distance'] = $user['distance'];
				$new_data['images'] = $this->db->get_where('user_images', array('user_id' => $user['user_id']))->result_array();
				$new_data['insta_images'] = $this->db->get_where('insta_images', array('user_id' => $user['user_id']))->result_array();
				$new_data['name'] = ($user['name'] == NULL ? '' : $user['name']);
				$new_data['profile_picture'] = ($user['profile_picture'] == NULL ? '' : $user['profile_picture']);
				$new_data['gender'] = ($user['gender'] == NULL ? '' : $user['gender']);
				$new_data['location'] = ($user['location'] == NULL ? '' : $user['location']);
				$new_data['latitude'] = ($user['latitude'] == NULL ? '' : $user['latitude']);
				$new_data['longitude'] = ($user['longitude'] == NULL ? '' : $user['longitude']);
				$new_data['dob'] = ($user['dob'] == NULL ? '' : $user['dob']);
				$new_data['dislikes'] = ($user['dislikes'] == NULL ? '' : $user['dislikes']);
				$new_data['interest_in'] = ($user['interest_in'] == NULL ? '' : $user['interest_in']);
				$new_data['likes'] = ($user['likes'] == NULL ? '' : $user['likes']);
				$new_data['experience'] = ($user['experience'] == NULL ? '' : $user['experience']);
				$new_data['education_level'] = ($user['education_level_text'] == NULL ? '' : $user['education_level_text']);
				$new_data['industry'] = ($user['industry_text'] == NULL ? '' : $user['industry_text']);
				$new_data['looking_for'] = ($user['looking_for_text'] == NULL ? '' : $user['looking_for_text']);
				$new_data['looking_for_d'] = ($user['looking_for_d_text'] == NULL ? '' : $user['looking_for_d_text']);
				$new_data['looking_for_f'] = ($user['looking_for_f_text'] == NULL ? '' : $user['looking_for_f_text']);
				$new_data['new_to_area'] = ($user['new_to_area_text'] == NULL ? '' : $user['new_to_area_text']);
				$new_data['releationship'] = ($user['releationship_text'] == NULL ? '' : $user['releationship_text']);
				$new_data['headline'] = ($user['headline'] == NULL ? '' : $user['headline']);
				$new_data['about_me'] = ($user['about_me'] == NULL ? '' : $user['about_me']);
				$new_data['job'] = ($user['job'] == NULL ? '' : $user['job']);
				$new_data['company'] = ($user['company'] == NULL ? '' : $user['company']);
				$new_data['interest'] = ($user['interest'] == NULL ? '' : $user['interest']);

				$this->db->select('image_url');
				$new_data['images'] = $this->db->get_where('user_images', array('user_id' => $user['user_id']))->result_array();
				$new_data['education_data'] = $this->db->get_where('education_detail', array('user_id' => $user['user_id']))->result_array();
				$new_data['job_data'] = $this->db->get_where('company_desc', array('user_id' => $user['user_id']))->result_array();
				//echo json_encode($details);die;
				array_unshift($new_data['images'], array('image_url' => $new_data['profile_picture']));

				$response[$i] = $new_data;
				$i++;
			}

			return_data(true, 'Users List', array('users' => $response));
		} else {
			return_data(false, 'Invalid User', array('users' => array()));
		}

	}

	private function validate_get_fan_wall_for_user() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	/* Not Interested*/
	public function interested_in_user() {
		$this->validate_interested_in_user();
		$input = $this->input->post();
		$this->db->insert('followed', array('follow_id' => $input['user_id_interested'], 'user_id' => $input['user_id'], 'creation_date' => milliseconds()));

		$this->db->select('quickblox_id');
		$this->db->join('users', 'users.id=followed.user_id');
		$is_matched = $this->db->get_where('followed', array('follow_id' => $input['user_id'], 'user_id' => $input['user_id_interested']))->row_array();

		if (!empty($is_matched)) {

/*----Chat Session Start-------*/
			$dailog_id = 0; //$this->testsession($input['user_id'], $is_matched['quickblox_id']);
			//$this->create_friend($input['user_id'], $input['user_id_interested']);

/*----Chat Session End-------*/
/*firebase start*/
			// $this->create_friend($input['user_id'], $input['user_id_interested']);
			// $inputs = array('user_id' => $input['user_id'], 'member_id' => $input['user_id_interested']);
			// $check_exist['message'] = 'start chat';
			// $this->sent_message($inputs, $check_exist);
			/*firebase end*/

			$this->db->insert('matches', array('user_id' => $input['user_id'], 'match_id' => $input['user_id_interested'], 'dailog_id' => $dailog_id, 'creation_time' => time()));
			//$this->update_profile_firebase(array('dailog_id' => $dailog_id), $input['user_id']);
			$push_array = array($input['user_id_interested'], $input['user_id']);
			modules::run('data_model/pusher/Push_match/push_on_match', $push_array);
			$user_data = modules::run('data_model/user/registration/complete_user_profile', $input['user_id_interested']);
			return_data(true, 'Matched', $user_data);
		} else {
			$push_array = array($input['user_id_interested'], $input['user_id']);
			modules::run('data_model/pusher/Push_match/push_on_interest', $push_array);

			return_data(true, 'Followed', json_decode("{}"));
		}
	}

	private function create_friend($user_id, $member_id) {
		$app_type = "chat/";

		//from user list management
		$this->firebase->getReference('friends/' . $app_type . $user_id . "/" . $member_id)->set([
			'date' => date("M d,Y h:i A"),
		]);

		//to user list management
		$this->firebase->getReference('friends/' . $app_type . $member_id . "/" . $user_id)->set([
			'date' => date("M d,Y h:i A"),
		]);
	}

	private function update_profile_firebase($user_data, $user_id) {
		foreach ($user_data as $key => $value) {
			$this->firebase->getReference('users/' . $user_id . '/' . $key)->set($value);
		}
	}

	private function sent_message($input, $check_exist) {
		$structure = "social_private";
		//sending message
		if ($check_exist['message']) {
			$this->firebase->getReference('chat_module/' . $structure . '/' . $input['member_id'] . "/" . $input['user_id'])->push([
				'time' => $this->milliseconds(),
				'from' => $input['user_id'],
				'seen' => FALSE,
				'message' => $check_exist['message'],
				'type' => 'message',
				'status' => '1',
			]);

			$this->firebase->getReference('chat_module/' . $structure . '/' . $input['user_id'] . "/" . $input['member_id'])->push([
				'time' => $this->milliseconds(),
				'from' => $input['member_id'],
				'seen' => FALSE,
				'message' => $check_exist['message'],
				'type' => 'message',
				'status' => '1',
			]);
		}

		//seen management of message
		$this->firebase->getReference('seen_management/' . $structure . '/' . $input['user_id'] . "/" . $input['member_id'])->set([
			'time_stamp' => $this->milliseconds(),
			'seen' => FALSE,
		]);

		$this->firebase->getReference('seen_management/' . $structure . '/' . $input['member_id'] . "/" . $input['user_id'])->set([
			'time_stamp' => $this->milliseconds(),
			'seen' => FALSE,
		]);
	}

	private function milliseconds() {
		$mt = explode(' ', microtime());
		return ((int) $mt[1]) * 1000 + ((int) round($mt[0] * 1000));
	}

	private function validate_interested_in_user() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('user_id_interested', 'user_id_interested', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function testsession($user_name, $user_ids) {
		//$user_ids = '96775893';
		$timestamp = time();
		$user_name = 'Burzz' . $user_name;
		$password = '12345678';
		$nonce = rand();
		$signature = $this->generate_signature($timestamp, $nonce);
		$auth_key = AUTH_KEY;
		$application_id = APPLICATION_ID;
// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.quickblox.com/session.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "application_id=$application_id&auth_key=$auth_key&timestamp=$timestamp&nonce=$nonce&signature=$signature");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Quickblox-Rest-Api-Version: 0.1.0';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$result = json_decode($result, true);
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = $result['session']['token'];
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.quickblox.com/login.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('login' => $user_name, 'password' => $password)));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'QuickBlox-REST-API-Version: 0.1.0';
		$headers[] = "Qb-Token: " . $token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		$result = json_decode($result, true);
		//print_r($result);
		if (array_key_exists('user', $result)) {

			return $this->create_dilog($user_ids, $token);
		} else {
			return false;
		}

	}

	private function create_dilog($user_ids, $data) {
		$token = $data;
		$post_fiels = json_encode(array('type' => 3, 'name' => 'Chat with Bob, Sam', 'occupants_ids' => $user_ids), true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.quickblox.com/chat/Dialog.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fiels);
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Qb-Token: ' . $token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		$result = json_decode($result, true);
		return $result['_id'];
	}
	private function generate_signature($timestamp, $nonce) {
		$signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp;
		return $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
	}

	/* Not Interested*/

	public function not_interested_in_user() {
		$this->validate_interested_in_user();
		$input = $this->input->post();
		$insert = $this->db->insert('never_followed', array('follow_id' => $input['user_id_interested'], 'user_id' => $input['user_id'], 'creation_date' => time()));

		if ($insert) {
			return_data(true, 'Success', json_decode("{}"));
		} else {
			return_data(false, 'something went wrong', json_decode("{}"));
		}
	}

	private function validate_unmatch() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('unfollow_user_id', 'unfollow_user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
	public function unmatch() {
		$this->validate_unmatch();
		$input = $this->input->post();
		$sql = "delete from matches where (user_id=" . $input['user_id'] . " and match_id=" . $input['unfollow_user_id'] . ") OR (user_id=" . $input['unfollow_user_id'] . " and match_id=" . $input['user_id'] . ")";

		//$this->db->where('id',$input['match_id']);
		$insert = $this->db->query($sql);
		$sql = "delete from followed where (user_id=" . $input['user_id'] . " and follow_id=" . $input['unfollow_user_id'] . ") OR (user_id=" . $input['unfollow_user_id'] . " and follow_id=" . $input['user_id'] . ")";
		$insert = $this->db->query($sql);

		if ($insert) {
			return_data(true, 'Success', json_decode("{}"));
		} else {
			return_data(false, 'something went wrong', json_decode("{}"));
		}
	}

	private function validate_block_user() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('blocked_id', 'blocked_id', 'trim|required');
		$this->form_validation->set_rules('reason', 'reason', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function block_user() {
		$this->validate_block_user();
		$input = $this->input->post();
		$insert = $this->db->insert('blocked', array('blocked_id' => $input['blocked_id'], 'user_id' => $input['user_id'], 'reason' => $input['reason'], 'description' => $input['description'], 'creation_time' => time()));

		if ($insert) {
			return_data(true, 'user blocked', json_decode("{}"));
		} else {
			return_data(false, 'something went wrong', json_decode("{}"));
		}
	}

	private function validate_matches() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
	private function validate_chat_notification() {
		post_check();

		$this->form_validation->set_rules('sender_id', 'sender_id', 'trim|required');
		$this->form_validation->set_rules('recipient_id', 'recipient_id', 'trim|required');
		$this->form_validation->set_rules('msg', 'msg', 'trim');
		$this->form_validation->set_rules('type', 'msg', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function chat_notification() {
		$this->validate_chat_notification();
		$input = $this->input->post();
		$u_dtl = $this->db->where_in('id', array($input['recipient_id'], $input['sender_id']))->get('users')->result_array();
		foreach ($u_dtl as $value) {
			if ($value['id'] == $input['recipient_id']) {
				$recipient_dtl = $value;
				if ($recipient_dtl['notification'] == 1) {
					return_data(false, 'user turned off notification', json_decode("{}"));
				}
			} else {
				$sender_dtl = $value;
			}
		}

		$push_array = array($input, $recipient_dtl, $sender_dtl);
		modules::run('data_model/pusher/Push_match/push_on_chat', $push_array);
		return_data(true, ' notification sent', json_decode("{}"));

	}

	public function matches() {
		$this->validate_matches();
		$input = $this->input->post();

		$users = $this->db->query("select id,is_new,user_id,match_id,dailog_id from matches where  `user_id` = " . $input['user_id'] . " OR match_id=" . $input['user_id'])->result_array();
		$no_of_users = count($users);
		$user1 = array_column($users, 'user_id');
		$user2 = array_column($users, 'match_id');
		$users_id = array_merge($user1, $user2);
		for ($i = 0; $i < $no_of_users; $i++) {
			if (($key = array_search($input['user_id'], $users_id)) !== false) {

				unset($users_id[$key]);
			}
		}

		if (!empty($users_id)) {
			$users_string = implode(',', $users_id);
			$sql = "SELECT `users`.`id`, `users`.`name`,`users`.`device_token`, `users`.`profile_picture`,users.quickblox_id FROM `users` WHERE `id` IN ($users_string)";
			$response = $this->db->query($sql)->result_array();
			$new = $old = [];
			if ($response) {
				foreach ($response as $resp) {

					foreach ($users as $user) {
						if (($resp['id'] == $user['user_id']) || ($resp['id'] == $user['match_id'])) {
							$resp['dailog_id'] = $user['dailog_id'];
							$resp['match_id'] = $user['id'];
							if ($user['is_new'] == 0) {
								array_push($new, $resp);
							} else {
								array_push($old, $resp);
							}
							break;
						}
					}
				}
				$result['new'] = $new;
				$result['old'] = $old;
				$time = time() * 1000;
				$sql = "select * from transaction_record where user_id=" . $input['user_id'] . " and transaction_status=1 and validity > $time";
				$packages = $this->db->query($sql)->result_array();
				$result['pro_user'] = 0;
				if (count($packages) > 0) {
					$result['pro_user'] = 1;
				}

				return_data(true, 'Users List', $result);
			} else {
				return_data(false, 'something went wrong', json_decode("{}"));
			}
		} else {
			return_data(false, 'no match', json_decode("{}"));
		}
	}

	private function validate_update_match() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('match_id', 'match_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	private function validate_check_pro_user() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		//$this->form_validation->set_rules('match_id', 'match_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

//	public function check_pro_user() {//live
//		$this->validate_check_pro_user();
//		$input = $this->input->post();
//
//		$time = time() * 1000;
//		$sql = "select * from transaction_record where user_id=" . $input['user_id'] . " and transaction_status=1 and validity > $time";
//		$packages = $this->db->query($sql)->result_array();
//		$result['pro_user'] = 0;
//		if (count($packages) > 0) {
//			$result['pro_user'] = 1;
//		}
//
//		return_data(true, 'Users List', $result);
//	}
        public function check_pro_user() {
		$this->validate_check_pro_user();
		$input = $this->input->post();

		$time = time() * 1000;
		$sql = "select * from transaction_record where user_id=" . $input['user_id'] . " and transaction_status=1 and validity > $time";
		$packages = $this->db->query($sql)->result_array();
		$result['pro_user'] = 0;
                $data = count($packages);
		if (count($packages) > 0) {
			$result['pro_user'] = 1;
		}
		return_data(true, 'Users List', $result);
	}

	public function update_match() {
		$this->validate_update_match();
		$input = $this->input->post();

		$match_dtl = $this->db->get_where('matches', array('id' => $input['match_id']))->row_array();
		/*firebase start*/
		$this->create_friend($match_dtl['user_id'], $match_dtl['match_id']);
		$inputs = array('user_id' => $match_dtl['user_id'], 'member_id' => $match_dtl['match_id']);
		$check_exist['message'] = '';
		$this->sent_message($inputs, $check_exist);
		/*firebase end*/

		$this->db->where('id', $input['match_id']);
		$update = $this->db->update('matches', array('is_new' => 1));
		if ($update) {
			return_data(true, 'updated successfully', json_decode("{}"));
		} else {
			return_data(false, 'something went wrong', json_decode("{}"));
		}
	}
         public function my_profile_like_users() {
		$this->validate_my_profile_like_users();
		$input = $this->input->post();
                $this->db->select("users.id as user_id,users.name as liked_users_name,users.profile_picture as profile_picture,"
                                . "users.email as email,users.mobile as mobile,users.location as location,"
                                . "users.creation_time as creation_time");
                $this->db->where('user_id',$input['user_id']);
                $this->db->join('users','followed.follow_id = users.id');
                $my_profile_like_users = $this->db->get('followed')->result();
                if(!empty($my_profile_like_users)){
                    return_data(true, 'List of Liked Users For My Profile', $my_profile_like_users);
                }
                return_data(false, 'Currently No User have liked your profile picture ', $my_profile_like_users);
            }
          private function validate_my_profile_like_users() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

}