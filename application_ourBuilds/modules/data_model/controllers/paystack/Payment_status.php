<?php
defined('BASEPATH') OR exit('No direct script access allowed');//live

class Payment_status extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->library('session');
		$this->load->model("notification_model");
	}

    public function check_payment_status(){
        $this->validate_check_payment_status();
        $pre_transaction_id = $this->input->post();
        $this->db->where('pre_transaction_id',$pre_transaction_id);
        $transaction = $this->db->get('transaction_record')->row();
        if( $transaction['transaction_status']==0){
            return_data(true, "Payment intialized.", array('transaction_status' =>$transaction ));

        }
    }

    private function validate_check_payment_status() {
        post_check();
        $this->form_validation->set_rules('pre_transaction_id', 'pre_transaction_id', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }
}
