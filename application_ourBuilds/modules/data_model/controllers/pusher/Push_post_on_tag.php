<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_post_on_tag extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 501 ******************************/

	public function push_on_tag($data){

		$user_id = $data['user_id'];
		$post_id = $data['post_id'];
		$reciver_id = $data['tagged_user_id'];

		$this->db->where('id',$user_id);
		$user_who_tagged = $this->db->get("users")->row_array();

		$this->db->where('id',$reciver_id);
		$user_who_got_tagged = $this->db->get("users")->row_array();
		

		$push_data = json_encode(
			array(
				'notification_code' => 501,
				'message' => $user_who_tagged['name'] ." tagged you in post.",
				'data' => array('post_id' => $post_id)
			)
		);
		
		if($user_id != $reciver_id){

			if($user_who_got_tagged['device_type'] == 1){
				/* android */
				$token = $user_who_got_tagged['device_tokken'];
				$device = "android";
				$result = generatePush($device, $token, $push_data);
				//logger 
				//log_message('error', "android push notification . token is $token and data is ".json_encode($push_data).$result);
			}
			if($user_who_got_tagged['device_type'] == 2){
				/* ios */
				$token = $user_who_got_tagged['device_tokken'];
				$device = "ios";
				generatePush($device, $token, $push_data);
				//logger 
				//log_message('error', "ios push notification . token is $token and data is ".json_encode($push_data));
			}
		}

	}

}	