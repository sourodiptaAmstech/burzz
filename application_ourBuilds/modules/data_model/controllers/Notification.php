<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->library('session');
		$this->load->model("notification_model");
	}

	public function get_notification_list(){ //user_id
		$this->validate_notification_list();
		$user_id=$this->input->post('user_id');
		$result=$this->notification_model->get_notification_list($this->input->post());
		return_data(true,'Success.',$result);
		
	}
	private function validate_notification_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_notification_detail(){ //user_id,post_id,post_type
		$this->validate_notification_detail();
		$user_id=$this->input->post('user_id');
		$result=$this->notification_model->get_notification_detail($this->input->post());
		return_data(true,'Success.',$result);
		
	}
	private function validate_notification_detail(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('post_type','post_type', 'trim|required');
		$this->form_validation->set_rules('post_id','post_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}
}
