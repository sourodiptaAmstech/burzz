<?php //dev
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends MX_Controller {//developement
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->library('session');
		$this->load->model("notification_model");
	}

    public function products(){
        $this->validate_products();
        $array = array(
            'name'=>'Video Streaming Service',
            'description'=>'Video Streaming Service',
            'type'=>'SERVICE',
            'category'=>'SOFTWARE',
            'image_url'=>'https://example.com/streaming.jpg',
            "home_url"=>'https://example.com/home'
        );
        $input = json_decode()
        $ch = curl_init("https://api.sandbox.paypal.com/v1/catalogs/products");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Bearer '. PAYPAL_ACCESS_TOKEN));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'PayPal-Request-Id'=>PROD-1234));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result   = json_decode($result);
        if($result){
           return_data(true, $result->message,array());
        }
    }
     private function validate_products() {
        post_check();
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();

        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }
}
