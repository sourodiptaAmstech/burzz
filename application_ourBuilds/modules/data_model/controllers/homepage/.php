<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Video_control extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('video_model');
		$this->load->model('Bhajan_model');
		$this->load->model('News_model');
		$this->load->model('Guru_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	// public function get_videos(){
	// 	$this->validate_video();
	// 	$user_id=$this->input->post('user_id');
	// 	$result=$this->video_model->get_videos(array('user_id'=>$user_id));
	// 	if($result){
	// 		return_data(true,'Success.',$result);
	// 	}else{
	// 		return_data(false,'Failed.',array());
	// 	}
	// }

	public function get_live_channels() {
		$this->validate_video();
		$data = $this->input->post();
		$result = $this->video_model->get_live_channels($data);
		if ($result) {
			return_data(true, 'Success.', $result);
		}
	}

	public function get_banners() {
		$this->validate_banners();
		$data = $this->input->post();
		$result = $this->video_model->get_banners($data);
		return_data(true, 'Success.', $result);
	}

	private function validate_banners() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function search_videos() {
		//search_content, video_category, last_video_id ,user_id
		$this->validate_search_video();
		$data = $this->input->post();
		$result = $this->video_model->get_videos_by_category_search_and_video_id($data);
		if ($result) {
			return_data(true, 'Success.', $result);
		}
	}

	private function validate_search_video() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		//$this->form_validation->set_rules('search_content','search_content', 'trim|required');
		//$this->form_validation->set_rules('video_category','video_category', 'trim|required');
		//$this->form_validation->set_rules('last_video_id','last_video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function home_page_videos() {

		$this->validate_video();
		$user_id = $this->input->post('user_id');
		$home_video = $this->video_model->get_home_page_videos();

		$videos = $this->video_model->get_home_videos(array('user_id' => $user_id));
		$channels = $this->video_model->get_live_channels(array('user_id' => $user_id));
		$bhajan = $this->Bhajan_model->get_bhajan_list(array('user_id' => $user_id));
		$news = $this->News_model->get_news_list(array("last_news_id" => ''));
		$guru = $this->Guru_model->get_guru_list(array('user_id' => $user_id, 'last_guru_id' => ''));
		$promotion_data = $this->db->get('promotions')->row_array();

		$promotion = array('video_url' => $promotion_data['video_url'], 'extension_type' => $promotion_data['extension_type'], 'promotionType' => $promotion_data['promotion_type']);
		if ($videos) {
			echo json_encode(array('status' => true, 'message' => 'Success', 'video' => $videos, 'channel' => $channels, 'bhajan' => $bhajan, 'guru' => $guru, 'promotion' => $promotion, 'news' => $news, 'home_video' => $home_video->video_url, 'error' => []));
			//return_data2(true, 'Success.', $channels, $videos,$bhajan);
		} else {
			echo json_encode(array('status' => false, 'message' => 'Failed.', 'video' => [], 'channel' => [], 'bhajan' => [], 'guru' => [], 'promotion' => '{}', 'news' => [], 'home_video' => [], 'error' => $error));
			//return_data2(false, 'Failed.', array());
		}
	}

	public function sankirtan_home() {
		// user_id
		$this->validate_video();
		$user_id = $this->input->post('user_id');
		$videos = $this->video_model->get_home_sankirtan(array('user_id' => $user_id));
		if ($videos) {
			return_data(true, 'Success.', $videos);
		} else {
			return_data(false, 'Failed.', array());
		}
	}

	public function view_all_sankirtan_by_category() {
//total_received_count,video_category,user_id
		$this->validate_view_all_sankirtan();
		$user_id = $this->input->post('user_id');
		$result = $this->video_model->view_all_sankirtan_by_category($this->input->post());
		if ($result) {
			return_data(true, 'Success.', $result);
		} else {
			return_data(false, 'Failed.', array());
		}
	}

	private function validate_view_all_sankirtan() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('video_category', 'video_category', 'trim|required');
		$this->form_validation->set_rules('total_received_count', 'total_received_count', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function home_page_view_all_videos() {
//total_received_count,video_category,user_id
		$this->validate_view_all_videos();
		$user_id = $this->input->post('user_id');
		$result = $this->video_model->home_page_view_all_videos($this->input->post());
		if ($result) {
			return_data(true, 'Success.', $result);
		} else {
			return_data(false, 'Failed.', array());
		}
	}

	private function validate_video() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	private function validate_view_all_videos() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('video_category', 'video_category', 'trim|required');
		$this->form_validation->set_rules('total_received_count', 'total_received_count', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function update_recent_view_video() {
		//fk_video_id , fk_user_id
		$this->validate_recent_view();
		$result = $this->video_model->update_recent_view_video($this->input->post());
		if ($result) {
			return_data(true, 'Recent video successfully updated.', array());
		} else {
			return_data(false, 'Some error in updation.', array());
		}
	}

	private function validate_recent_view() {
		post_check();
		$this->form_validation->set_rules('fk_video_id', 'Video ID', 'required');
		$this->form_validation->set_rules('fk_user_id', 'User ID', 'required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();
		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function view_video() {

		$this->validate_view_video();
		// first check if already view
		if ($this->video_model->is_already_view_video($this->input->post())) {
			return_data(false, 'User Already viewed.');
		}

		$this->video_model->view_video($this->input->post());
		return_data(true, 'User viewed.');
	}

	public function like_video() {

		$this->validate_view_video();
		// first check if already view
		if ($this->video_model->is_already_like_video($this->input->post())) {
			return_data(false, 'User Already liked.');
		}

		$this->video_model->like_video($this->input->post());
		return_data(true, 'User liked.');
	}

	public function unlike_video() {

		$this->validate_view_video();
		$this->video_model->unlike_video($this->input->post());

		return_data(true, 'Unlike Successfully.');
	}

	private function validate_view_video() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('video_id', 'video_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function recent_views() {
		$this->validate_recent_views();
		$data['creation_time'] = milliseconds();
		$user_id = $this->input->post('user_id');
		$type = $this->input->post('type');
		$id = $this->input->post('media_id');
		$update_data = array('user_id' => $user_id, 'media_id' => $id, 'type' => $type, 'creation_time' => $data['creation_time']);
		$count = $this->is_meta_exist(array('user_id' => $user_id, 'type' => $type, 'media_id' => $id));
		if ($count > 0) {
			$this->video_model->update_user_meta($update_data);
			return_data(true, 'Success.', array());
		} else {
			$this->video_model->insert_user_meta($update_data);
			if ($type == 2) {
				$this->video_model->view_video(array('user_id' => $user_id, 'video_id' => $id));
			}

			return_data(true, 'Success.', array());
		}
		return_data(false, 'Failed.', array());
	}

	private function validate_recent_views() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		$this->form_validation->set_rules('media_id', 'media_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function is_meta_exist($data) {
		$result = $this->video_model->get_meta_count_of_media_and_user($data);
		return $result;
	}

	public function get_related_guru_videos() {
		$this->validate_get_related_guru_videos();
		$user_id = $this->input->post('user_id');
		$guru_id = $this->input->post('guru_id');
		$result = $this->video_model->get_related_guru_videos(array('user_id' => $user_id, 'guru_id' => $guru_id));
		return_data(true, 'Success.', $result);
	}

	private function validate_get_related_guru_videos() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('guru_id', 'guru_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function get_related_videos() {
		$this->validate_get_related_videos();
		$user_id = $this->input->post('user_id');
		$guru_id = $this->input->post('video_id');
		$result = $this->video_model->get_related_videos(array('user_id' => $user_id, 'video_id' => $guru_id));
		if ($result) {
			return_data(true, 'Success.', $result);
		}
		return_data(false, 'empty data.', array());
	}

	private function validate_get_related_videos() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('video_id', 'video_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	/*     * *
		     *                   _      _    _____                                           _
		     *         /\       | |    | |  / ____|                                         | |
		     *        /  \    __| |  __| | | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_
		     *       / /\ \  / _` | / _` | | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __|
		     *      / ____ \| (_| || (_| | | |____| (_) || | | | | || | | | | ||  __/| | | || |_
		     *     /_/    \_\\__,_| \__,_|  \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__|
		     *
		     *
	*/

	public function add_comment() {
		//user_id, video_id,commment

		$this->validate_add_comment();
		$comment_data['user_id'] = $this->input->post('user_id');
		$comment_data['video_id'] = $this->input->post('video_id');
		$comment_data['comment'] = $this->input->post('comment');
		/* save and get comment id */
		$comment_id = $this->video_model->add_comment($comment_data);
		return_data(true, 'Comment added.', array());
	}

	private function validate_add_comment() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('video_id', 'video_id', 'trim|required');
		$this->form_validation->set_rules('comment', 'comment', 'trim|required');
		/* parent comment id  if key not found */

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	/*     * *
		     *       _____                                           _     _       _       _
		     *      / ____|                                         | |   | |     (_)     | |
		     *     | |      ___   _ __ ___   _ __ ___    ___  _ __  | |_  | |      _  ___ | |_
		     *     | |     / _ \ | '_ ` _ \ | '_ ` _ \  / _ \| '_ \ | __| | |     | |/ __|| __|
		     *     | |____| (_) || | | | | || | | | | ||  __/| | | || |_  | |____ | |\__ \| |_
		     *      \_____|\___/ |_| |_| |_||_| |_| |_| \___||_| |_| \__| |______||_||___/ \__|
		     *
		     *
	*/

	public function get_video_comment() {
		//video_id, last_comment_id

		$this->validate_get_video_comment();

		if (!array_key_exists('last_comment_id', $_POST)) {
			$last_comment_id = "";
		} else {
			$last_comment_id = $this->input->post('last_comment_id');
		}

		$info = array(
			"video_id" => $this->input->post('video_id'),
			'last_comment_id' => $last_comment_id,
		);

		$comments = $this->video_model->get_post_comment($info);
		return_data(true, 'Comment details.', $comments);
	}

	private function validate_get_video_comment() {

		post_check();
		$this->form_validation->set_rules('video_id', 'video_id', 'trim|required');
		/* parent comment id  if key not found */

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function get_related_bhajan() {
		//$this->validate_get_related_videos();
		$user_id = $this->input->post('user_id');
		$guru_id = $this->input->post('bhajan_id');

		$result = $this->Bhajan_model->get_related_guru_audios(array('user_id' => $user_id, 'guru_id' => $guru_id));
		if ($result) {
			return_data(true, 'Success.', $result);
		}
		return_data(false, 'empty data.', array());
	}
	public function get_playlist() {
		$this->get_playlist_validation();
		$user_id = $this->input->post('user_id');
		$result = $this->Bhajan_model->playlist_get($user_id);
		if ($result) {
			return_data(true, 'Success.', $result);
		}
		return_data(false, 'empty data.', array());
	}
	public function get_playlist_validation() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		/* parent comment id  if key not found */

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
	public function playlist() {
		$this->playlist_validation();
		$user_id = $this->input->post('user_id');
		$bhajan_id = $this->input->post('bhajan_id');
		$type = $this->input->post('type');
		//1 add playlist
		//2 removeplaylist
		if ($type == 1 || $type == 2) {
			$this->Bhajan_model->playlist($user_id, $bhajan_id, $type);
			return_data(true, 'Success.', array());

		} else {
			return_data(false, 'Invalid type', array());
		}
		/* $result = $this->Bhajan_model->playlist_get($user_id);
			        if ($result) {
			            return_data(true, 'Success.', $result);
		*/

	}
	public function playlist_validation() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('bhajan_id', 'bhajan_id', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		/* parent comment id  if key not found */

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function search_home_data() {
		// echo "FSdfsxs";die;
		// $this->validate_search_home_data();
		$user_id = $this->input->post('user_id');
		$keyword = $this->input->post('keyword');

		// $home_video = $this->video_model->get_home_page_videos_keyword($keyword);

		$videos[] = $this->video_model->get_home_videos_keyword(array('user_id' => $user_id, 'keyword' => $keyword)); //done

		$channels = $this->video_model->get_live_channels_keyword(array('user_id' => $user_id, 'keyword' => $keyword));

		$bhajan[] = $this->Bhajan_model->get_bhajan_list_keyword(array('user_id' => $user_id, 'keyword' => $keyword)); //done

		$news = $this->News_model->get_news_list_keyword(array('user_id' => $user_id, 'keyword' => $keyword));

		$guru = $this->Guru_model->get_guru_list_keyword(array('user_id' => $user_id, 'keyword' => $keyword));

		echo json_encode(array('status' => true, 'message' => 'Success', 'video' => $videos, 'channel' => $channels, 'bhajan' => $bhajan, 'news' => $news, 'guru' => $guru, 'error' => []));

	}
	public function Channel() {

		$this->Channel_validation();
		$user_id = $this->input->post('user_id');

		$channels = $this->video_model->get_live_channels(array('user_id' => $user_id));

		echo json_encode(array('status' => true, 'message' => 'Success', 'channel' => $channels, 'error' => []));
	}
	public function Channel_validation() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');

		/* parent comment id  if key not found */

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
}
