<?php //dev
defined('BASEPATH') OR exit('No direct script access allowed');
class Check_rest_api extends MX_Controller {//developement
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->library('session');
		$this->load->model("notification_model");
	}

    public function check(){
        
        //$input = $this->input->post();
        $ch = curl_init("https://api.sandbox.paypal.com/v1/payment-experience/web-profiles/XP-8YTH-NNP3-WSVN-3C76/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Bearer '. PAYPAL_ACCESS_TOKEN));
      //  curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result   = json_decode($result);
        if($result){
           return_data(true, $result->message,array());
        }
    }
}
