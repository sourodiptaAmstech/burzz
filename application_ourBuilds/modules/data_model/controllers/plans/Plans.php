<?php //live
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('plan_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	 public function get_plan_list() {
        $this->validate_plan_list();
        $input = $this->input->post();
        //file_put_contents('logs/get_plan_list.txt', json_encode($this->input->post()). json_encode(getallheaders()) .PHP_EOL , FILE_APPEND) ;
        $user_id = $this->input->post('user_id');
        if ($user_id) {
            $input = $this->input->post();
            $plans = $this->plan_model->get_plan_list($input);
            //print_r($plans);die;
            //if($plans && isset($input['is_subscribed']) && !empty($input['is_subscribed'])){
            if ($plans) {
                $final = array();
                foreach ($plans as $plan) {
                    $user = $this->db->get_where('users',array('id'=>$user_id))->row_array();
                    //print_r($user);
                    if($user['free_plan_used'] != 0 && $plan['is_free'] == 1){
                        continue;
                    }
                    $time = time() * 1000;
                    $sql = "select * from transaction_record where user_id=" . $user_id . " and package_id=" . $plan['id'] . " and transaction_status=1 and plan_status=1 and validity > ".$time." Order by id desc";
                    $exist = $this->db->query($sql)->row();
                    //print_r($exist);die;
if(is_null($exist)){
$exist=array();
}


                    $plan['is_subscribed'] = 0; $plan['purchased_on'] = 0;
                    if (count($exist) > 0) {
                        $plan['is_subscribed'] = 1;
                        $plan['purchased_on'] = date('d-M,Y',($exist->creation_time)/1000);  
                    }
                    if ($plans && isset($input['is_subscribed']) && !empty($input['is_subscribed'])) {
                        if (count($exist) > 0) {
                            $final[] = $plan;
                        }
                    } else {
                        if(!count($exist) > 0){                            
                            $final[] = $plan;
                        }
                    }
                }
                $plans = $final;
            }
            return_data(true, 'Success.', $plans);
        } else {
            return_data(false, 'User ID required.', array());
        }
    }


	private function validate_plan_list() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	private function validate_initialize_transaction() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('package_id', 'package_id', 'trim|required');
		$this->form_validation->set_rules('package_price', 'package_price', 'trim|required');
		$this->form_validation->set_rules('currency', 'currency', 'trim|required');
		$this->form_validation->set_rules('pay_via', 'pay_via', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
		if ($this->input->get('pay_via') != "") {
			if ($this->input->get('pay_via') !== "PAY_STACK" || $this->input->get('pay_via') !== "FLUTTER_WAVE" || $this->input->get('PAY_PAL') !== "PAY_PAL") {
				return_data(false, "Please provide valid payment gateway !", array(), array());
			}

		}
	}

	public function initialize_transaction() {
        $this->validate_initialize_transaction();
        $authorization_url = "";
        $user_id = $this->input->post('user_id');
        $package_id = $this->input->post('package_id');
        $user = $this->db->select('users.*')->where('id', $user_id)->get('users')->row_array();
        $plan = $this->db->select('packages.*')->where('id', $package_id)->get('packages')->row_array();
        $reference = 'u' . $user['id'] . 'p' . $plan['id'] . 't' . time();
        if ($this->input->post('pay_via') == 'paystack') {
            $postdata = array(
                'email' => $user['email'],
                'amount' => $plan['price_2'],
                "reference" => $reference,
                "plan" => $plan['plan_code']
            );

            $url = "https://api.paystack.co/transaction/initialize";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Authorization: Bearer ' . PAYSTACK_SECRET_KEY,
                'Content-Type: application/json',
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $request = curl_exec($ch);

            curl_close($ch);
            if ($request) {
                $result = json_decode($request, true);
                if ($result['status']) {
                    //print_r($result);
                    $authorization_url = $result['data']['authorization_url'];
                } else {
                    return_data(false, $result['message'], array());
                }
            } else {
                return_data(false, 'Something went wrong', array());
            }
            //print_r($result);
        }

        $cv = $this->db->select('validity')->where('id', $package_id)->get('packages')->row();

        $array = array(
            'user_id' => $user_id,
            'package_id' => $package_id,
            'pre_transaction_id' => $reference,
            'package_price' => $this->input->post('package_price'),
            'currency' => $this->input->post('currency'),
            'creation_time' => milliseconds(),
            'plan_status'=>1
        );
        if ($cv->validity > 0) {
            $array['validity'] = $array['creation_time'] + ($cv->validity * 24 * 60 * 60 * 1000);
        }
        /*
         * get course instructor and add his share w.r.t. course
         */

        if ($this->input->post('pay_via') == "PAY_STACK") {
            $array['pay_via'] = 'PAY_STACK';
        }
        if ($this->input->post('pay_via') == "FLUTTER_WAVE") {
            $array['pay_via'] = 'FLUTTER_WAVE';
        }
        if ($this->input->post('pay_via') == "PAY_PAL") {
            $array['pay_via'] = 'PAY_PAL';
        } else {
            $array['pay_via'] = 'OTHER';
        }

        $this->db->insert('transaction_record', $array);
        return_data(true, "Payment initialized.", array('pre_transaction_id' => $reference, 'authorization_url' => $authorization_url));
    }


   private function validate_apple_transaction() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('product_id', 'product_id', 'trim|required');
		$this->form_validation->set_rules('post_transaction_id', 'post_transaction_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}


	public function apple_payment() {
		$this->validate_apple_transaction();
		$user_id = $this->input->post('user_id');
		$package_id = $this->input->post('product_id');
		$key = rand(111, 999) . substr(md5(time()), 5);
		$cv = $this->db->select('validity,id,price')->where('apple_product_id', $package_id)->get('packages')->row();

		$array = array(
			'user_id' => $user_id,
			'package_id' => $cv->id,
			'pre_transaction_id' => $key,
			'package_price' => $cv->price,
			'currency' => 'USD',
			'creation_time' => milliseconds(),
		);
		if ($cv->validity > 0) {
			$array['validity'] = $array['creation_time'] + ($cv->validity * 24 * 60 * 60 * 1000);
		}
			$array['pay_via'] = 'APPLE';
			$array['transaction_status']=1;
			$array['post_transaction_id']=$this->input->post('post_transaction_id');	
		$this->db->insert('transaction_record', $array);
		return_data(true, "Payment Success.", array('pre_transaction_id' => $key));
	}



	private function validate_complete_transaction() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('pre_transaction_id', 'pre_transaction_id', 'trim|required');
		$this->form_validation->set_rules('post_transaction_id', 'post_transaction_id', 'trim|required');
		//$this->form_validation->set_rules('package_id', 'package_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function complete_transaction() {
		$this->validate_complete_transaction();
		$user_id = $this->input->post('user_id');
		$pre_transaction_id = $this->input->post('pre_transaction_id');
		$post_transaction_id = $this->input->post('post_transaction_id');
		$array = array(
			'post_transaction_id' => $post_transaction_id,
			'transaction_status' => 1,
		);

		$this->db->where('pre_transaction_id', $pre_transaction_id);
		$this->db->update('transaction_record', $array);

		/* update learner for this course we have course id */

		/* send message on mobile about it */
		//modules::run("data_model/user/mobile_auth/send_message_on_purchase", array('user_id' => $user_id));
		/* send message on email about it */
		//modules::run("data_model/emailer/payment_email/payment_done", array('user_id' => $user_id, 'pre_transaction_id' => $pre_transaction_id));

		return_data(true, "Payment complete.", array());
	}

}
