<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Static_pages extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users_model');
		$this->load->helper("services");
		$this->load->helper("message_sender");
		$this->load->library('session');
		//	$this->load->library('aws_s3_file_upload');
	}

	public function get_policy() {
		post_check();
		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}

		$data = $this->db->get_where('static_pages', array('id' => $this->input->post('id')))->result_array();
		return_data(true, 'data', $data);
	}

	public function get_faqs() {

		$data = $this->db->get('faqs')->result_array();
		return_data(true, 'data', $data);
	}

	public function contact_us() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('subject', 'subject', 'trim|required');
		$this->form_validation->set_rules('message', 'message', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
		$input = array('user_id' => $this->input->post('user_id'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message'),

		);
		$data = $this->db->insert('feedback', $input);
		return_data(true, 'updated successfully', json_decode("{}"));
	}

}