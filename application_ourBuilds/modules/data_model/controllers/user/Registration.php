<?php
ini_set('display_errors', 0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'third_party/firebase/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class Registration extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users_model');
		$this->load->model('OTP_model');
		$this->load->helper("services");
		$this->load->helper("message_sender");
		$this->load->library('session');
		$this->load->helper('sendsms_helper');
		$serviceAccount = ServiceAccount::fromJsonFile(APPPATH . 'third_party/google-services.json');
		$firebase = (new Factory)
			->withServiceAccount($serviceAccount)
			->withDatabaseUri('https://burzz-3d854.firebaseio.com/')
			->create();
		$this->firebase = $firebase->getDatabase();
		//	$this->load->library('aws_s3_file_upload');

		$this->load->helper('form');
		
	}

	public function twilio_sms($mobile, $message) {
		//$mobile = '+' . '917905464748';
		// $mobile = "+".$data['c_code'] . $data['mobile'];
		//$message = 'hello test for twilio';
		require_once APPPATH . 'third_party/twilio-php/Services/Twilio.php';
// $account_sid = 'ACd315d0688552a5827ddc9385f4e5abd9';
		// $auth_token = '6d1d9b4b8a0b8371f843b1acf4a978a2';
		$account_sid = 'ACbc1c35943f5a3bc0577019cb0e35c5ad';
		$auth_token = 'c6bcd1ac7fef1cd3a965d0b07d925f5f';
		$client = new Services_Twilio($account_sid, $auth_token); //echo $mobile;
		$message = $client->account->messages->create(array(
			'To' => $mobile,
			'From' => "+12028582976",
			'Body' => $message));
		if ($message->sid != '') {
			return true;
//return_data('true','messsage sent successfully',array());
		} else {
			return true;
		}
	}

/***
 *       _____  _
 *      / ____|(_)
 *     | (___   _   __ _  _ __   _   _  _ __
 *      \___ \ | | / _` || '_ \ | | | || '_ \
 *      ____) || || (_| || | | || |_| || |_) |
 *     |_____/ |_| \__, ||_| |_| \__,_|| .__/
 *                  __/ |              | |
 *                 |___/               |_|
 */

	public function signup() {
          
		$this->validate_signup();
		$data = $this->input->post();

		// $log = "Time: " . date('Y-m-d, H') . PHP_EOL;
		// $log = $log . "Request " . json_encode($data) . PHP_EOL;
		// file_put_contents('logs/signup.txt', $log, FILE_APPEND);

		//Something to write to txt log

//Save string to log, use FILE_APPEND to append.
		//	file_put_contents('./log_' . date("j.n.Y") . '.log', $log, FILE_APPEND);

		if ($data['login_type'] == 0) {
			if ($data['email'] != '') {
				$check_email_exist = $this->users_model->get_user_from_email(array('email' => $data['email']));
				if (!empty($check_email_exist)) {

					return_data(false, 'This email is already registerd with other user please use different email.', json_decode("{}"));
				}
			}
			if ($data['mobile'] != '') {
				$check_mobile_exist = $this->users_model->get_custum_user(array('mobile' => $data['mobile']));
				if (!empty($check_mobile_exist)) {

					return_data(false, 'This mobile is already registerd with other user please use different mobile.', json_decode("{}"));
				}
			}
			/*if ($data['username'] != '') {
				$check_mobile_exist = $this->users_model->get_custum_username(array('username' => $data['username']));
				if (!empty($check_mobile_exist)) {

					return_data(false, 'This username is already registerd with other user please use different username.', json_decode("{}"));
				}
			}*/

		}
		if ($data['login_type'] == 1) {
			if (isset($data['social_tokken']) && ($data['social_tokken'] != '')) {
				$check_social_exist = $this->users_model->check_social_exist(array('social_tokken' => $data['social_tokken']));

				if (empty($check_social_exist)) {
					//not exist

					if ($data['mobile'] != '') {

						$check_mobile_exist = $this->users_model->get_custum_user(array('mobile' => $data['mobile']));
						if (empty($check_mobile_exist)) {

							if ($data['otp'] == '' && $data['otp_verify'] == 0) {
								$otp = (string) rand(1001, 9999);
								$mobile = $data['c_code'] . $data['mobile'];
								$message = 'Your Burzz OTP is ' . $otp;
                                                                sendsms($mobile, $message);
								return_data(true, 'OTP SENT.', array('otp' => $otp));
							}
							if ($data['otp'] != '' && $data['otp_verify'] == 1) {
								$data['creation_time'] = milliseconds();
								$this->db->insert('users', $data);
								$user_id = $this->db->insert_id();
								$user_profile_detail['user_id'] = $user_id;
								$quickblox_id = $this->quickbloxsignup($data, $user_id);

								//firebase
								$this->register_user(array('id' => $user_id, 'name' => $data['name'], 'profile_picture' => ($data['profile_picture'] != '' ? $data['profile_picture'] : 'default'), 'device_token' => $data['device_token'], 'quickblox_id' => $quickblox_id));
								$this->db->insert('user_profile_detail', $user_profile_detail);
								$this->db->where('id', $user_id);
								$this->db->update('users', array('quickblox_id' => $quickblox_id));
								$userdata = $this->complete_user_profile($user_id);
								return_data(true, 'Registered Successfully', $userdata);

							}

							//	$this->db->insert('users', $data);
							//	return_data(true, 'Registered Successfully', $check_mobile_exist);
						} else {
							return_data(false, 'This mobile is already registerd with other user please use different mobile.', json_decode("{}"));
						}
					} else {
						return_data(false, 'mobile  no required', json_decode("{}"));
					}

				} else {

					if ($check_social_exist['status'] == 2) {
						return_data(true, 'Login Disabled', json_decode("{}"));
					}

					return_data(true, 'Login Successfully', $this->complete_user_profile($check_social_exist['id']));
				}
			} else {
				return_data(true, 'social_tokken required', json_decode("{}"));
			}

		}

		if ($data['otp_verify'] == 1) {
			if ($data['password'] == "") {
				return_data(false, 'Password Required', json_decode("{}"));
			}
			$data['password'] = md5($data['password']);
			$user_id = $this->users_model->save_user($data);
			$user_profile_detail['user_id'] = $user_id;

			$quickblox_id = $this->quickbloxsignup($data, $user_id);
			$this->register_user(array('id' => $user_id, 'name' => $data['name'], 'profile_picture' => ($data['profile_picture'] != '' ? $data['profile_picture'] : 'default'), 'device_token' => $data['device_token'], 'quickblox_id' => $quickblox_id));

			$this->db->insert('user_profile_detail', $user_profile_detail);
			$this->db->where('id', $user_id);
			$this->db->update('users', array('quickblox_id' => $quickblox_id));
			$userdata = $this->complete_user_profile($user_id);
			return_data(true, 'Registered Successfully', $userdata);
		} else {
			$otp = (string) rand(1001, 9999);
			//Load email library
			/*$this->load->library('email');
			$config['charset'] = 'utf-8';
			$config['newline'] = "\r\n";
			$config['mailtype'] = 'html'; // text or html
			$config['validation'] = TRUE; // bool whether to validate email or not
			$this->email->initialize($config);
			$from_email = "sobhan.brainium@gmail.com";
			
			
			$this->email->from($from_email, 'Burzz');
			$this->email->to($to_email);
			$this->email->subject('Registration OTP');
			$this->email->message($emailMessage); */

			//SMTP & mail configuration
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'emailbrainium@gmail.com',
				'smtp_pass' => 'test@email',
				'mailtype' => 'html',
				'charset' => 'iso-8859-1'
			);
			$to_email = $data['email'];
			$otp = (string) rand(1001, 9999);
			$emailMessage = "Hi, Your OTP is $otp. Please verify it and complete the registration process.";
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from("donotreply@burzz.com", 'Burzz!');
			$this->email->to($to_email);
			$this->email->subject('Burzz::Registration OTP');
			$this->email->message($emailMessage);
			$this->email->set_mailtype("html");
		   // $this->email->send();

		//	echo $this->email->print_debugger(); exit;

			/** ** */
			
			//save otp to db
			$addOTP['email'] = $data['email'];
			$addOTP['otp'] = $otp;
			$addOTP['status'] = 1;
			$addedOTPResponse = $this->OTP_model->save_otp($addOTP);
			
			//end
			//var_dump($this->email->send());
			//print_r($a);exit;
			if($this->email->send()){
				return_data(true, 'Please check your email.We have sent OTP.', array('otp' => $otp));
			}
			else{
				$mobile = $data['c_code'] . $data['mobile'];
				$message = 'Your Burzz OTP is ' . $otp;
				sendsms($mobile, $message);
				return_data(true, 'OTP SENT.', array('otp' => $otp));
			}
		}

	}
	public function verifyOTP(){
		$data['email'] = $this->input->post('email');
		$data['otp'] = $this->input->post('otp');

		$check_otp_verified = $this->OTP_model->check_otp_exist($data);
		if (!empty($check_otp_verified)) {

			return_data(true, 'OTP verified successfully.', json_decode("{}"));
		}else{
			return_data(false, 'OTP verified failed.', json_decode("{}"));
		}
	}

	private function register_user($user_data) {
		$this->firebase->getReference('users/' . $user_data['id'])->set([
			'user_id' => $user_data['id'],
			'name' => $user_data['name'],
			'profile_picture' => $user_data['profile_picture'],
			'device_token' => $user_data['device_token'],
			'quickblox_id' => $user_data['quickblox_id'],
			'is_online' => "online",
		]);
	}

	private function update_profile_firebase($user_data, $user_id) {
		foreach ($user_data as $key => $value) {
			$this->firebase->getReference('users/' . $user_id . '/' . $key)->set($value);
		}
	}

	public function complete_user_profile($id) {

		$this->db->select('users.*,upd.`headline`, upd.`about_me`, upd.`job`, upd.`education`, upd.`company`, upd.`interest`, upd.`looking_for`,`looking_for_f`,`looking_for_d`, upd.`industry`, upd.`education_level`, upd.`experience`, upd.`drinking`, upd.`smoking`, upd.`pets`, upd.`kids`, upd.`new_to_area`, upd.`releationship`, upd.`interest_in`, upd.`likes`, upd.`dislikes`, `exercise`, `religion`, `zodiac`,`politics`, `height`, distance,min_age,max_age,(select count(id) from education_detail where user_id=' . $id . ') as education_count,(select count(id) from company_desc where user_id=' . $id . ') as job_count,
    (SELECT title FROM zodiac WHERE id = upd.zodiac) AS zodiac_text,
    (SELECT title FROM politics WHERE id = upd.politics) AS politics_text,
    (SELECT title FROM drinking WHERE id = upd.drinking) AS drinking_text,
    (SELECT title FROM education_level WHERE id = upd.education_level) AS education_level_text,
    (SELECT title FROM exercise WHERE id = upd.exercise) AS exercise_text,
    (SELECT title FROM industry WHERE id = upd.industry) AS industry_text,
    (SELECT title FROM kids WHERE id = upd.kids) AS kids_text,
    (SELECT title FROM looking_for WHERE id = upd.looking_for) AS looking_for_text,
    (SELECT title FROM looking_for_d WHERE id = upd.looking_for_d) AS looking_for_d_text,
    (SELECT title FROM looking_for_f WHERE id = upd.looking_for_f) AS looking_for_f_text,
    (SELECT title FROM new_to_area WHERE id = upd.new_to_area) AS new_to_area_text,
    (SELECT title FROM pets WHERE id = upd.pets) AS pets_text,
    (SELECT title FROM releationship WHERE id = upd.releationship) AS releationship_text,
    (SELECT title FROM religion WHERE id = upd.religion) AS religion_text,
    (SELECT title FROM smoking WHERE id = upd.smoking) AS smoking_text');
		$this->db->join('user_profile_detail as upd', 'upd.user_id=users.id');
		$details = $this->db->get_where('users', array('users.id' => $id))->row_array();

		$icons = $this->db->get_where('master_relation')->result_array();
		foreach ($icons as $icon) {

			$details[$icon['master_table'] . '_icon'] = ($icon['icon'] != '' ? base_url('icon/') . $icon['icon'] : '');
		}
		$this->db->select('image_url');
		$details['images'] = $this->db->get_where('user_images', array('user_id' => $id))->result_array();
		//$preference = [];
		$type = ($details['interest'] == '' ? 1 : $details['interest']);
		$masters = $this->db->query("SELECT master_table,title,icon FROM `master_relation` WHERE find_in_set($type ,type)")->result_array();
		foreach ($masters as $master) {
			$table = $master['master_table'] . '_text';

			$preference[] = array('text' => ($details[$table] == NULL ? '' : $details[$table]), 'icon' => base_url('icon/' . $master['icon']));

		}

		// if ($details['interest'] == 1) {
		// 	$preference[] = array('text' => ($details['looking_for_text'] == NULL ? '' : $details['looking_for_text']), 'icon' => base_url($master['icon']));

		// }
		// if ($details['interest'] == 2) {
		// 	$preference[] = array('text' => ($details['looking_for_text'] == NULL ? '' : $details['looking_for_text']), 'icon' => base_url($master['icon']));

		// }
		// if ($details['interest'] == 3) {
		// 	$preference[] = array('text' => ($details['looking_for_text'] == NULL ? '' : $details['looking_for_text']), 'icon' => base_url($master['icon']));

		// }

		$details['preference'] = array_values($preference);
		/* array(
			array('text' => ($details['zodiac_text'] == NULL ? '' : $details['zodiac_text']), 'icon' => base_url('icon/zodiac.png')),
			array('text' => ($details['politics_text'] == NULL ? '' : $details['politics_text']), 'icon' => base_url('icon/politics.png')),
			array('text' => ($details['drinking_text'] == NULL ? '' : $details['drinking_text']), 'icon' => base_url('icon/drink.png')),
			array('text' => ($details['education_level_text'] == NULL ? '' : $details['education_level_text']), 'icon' => base_url('icon/education.png')),
			array('text' => ($details['smoking_text'] == NULL ? '' : $details['smoking_text']), 'icon' => base_url('icon/smoking.png')),
			array('text' => ($details['exercise_text'] == NULL ? '' : $details['exercise_text']), 'icon' => base_url('icon/exercise.png')),
			array('text' => ($details['height'] == NULL ? '' : $details['height']), 'icon' => base_url('icon/height.png')),
			array('text' => ($details['kids_text'] == NULL ? '' : $details['kids_text']), 'icon' => base_url('icon/kids.png')),
			array('text' => ($details['pets_text'] == NULL ? '' : $details['pets_text']), 'icon' => base_url('icon/pets.png')),
			array('text' => ($details['religion_text'] == NULL ? '' : $details['religion_text']), 'icon' => base_url('icon/religion.png')),
			array('text' => ($details['looking_for_text'] == NULL ? '' : $details['looking_for_text']), 'icon' => base_url('icon/religion.png')),
			array('text' => ($details['new_to_area_text'] == NULL ? '' : $details['new_to_area_text']), 'icon' => base_url('icon/religion.png')),
			array('text' => ($details['releationships_text'] == NULL ? '' : $details['releationships_text']), 'icon' => base_url('icon/religion.png')),

		);*/
		$details['education_data'] = $this->db->get_where('education_detail', array('user_id' => $details['id']))->result_array();
		$details['job_data'] = $this->db->get_where('company_desc', array('user_id' => $details['id']))->result_array();

		//echo json_encode($details);die;
		array_unshift($details['images'], array('image_url' => $details['profile_picture']));
		$this->db->select('insta_img_url');
		$details['insta_images'] = $this->db->get_where('insta_images', array('user_id' => $id))->result_array();
		return $details;
	}
	public function get_data() {
		return_data(true, 'user Profile Updated', $this->complete_user_profile($this->input->post('id')));
	}

	private function validate_signup() {

		post_check();
		//$this->form_validation->set_message('is_unique', '%s already exist.');
		//$this->form_validation->set_rules('username','username', 'trim|required');
		//$this->form_validation->set_rules('email','Email', 'trim|valid_email');
		$this->form_validation->set_rules('device_token', 'device_token', 'trim|required');
		$this->form_validation->set_rules('login_type', 'login_type', 'trim|required');
		$this->form_validation->set_rules('otp_verify', 'otp_verify', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], json_decode("{}"));
		}
	}

	public function update_profile() {
		$this->validate_update_profile();
		$data = $this->input->post();
		//$log = "Time: " . date('Y-m-d, H') . PHP_EOL;
		//$log = $log . "Request " . json_encode($data) . PHP_EOL;
		//file_put_contents('update.txt', $log, FILE_APPEND);

		//file_put_contents('./log_' . date("j.n.Y") . '.log', $log, FILE_APPEND);

		$userdata = [];
		$user_profile_data = [];

		if (isset($data['id'])) {
			$userdata['id'] = $data['id'];
		}if (isset($data['name'])) {
			$this->update_profile_firebase(array('name' => $data['name']), $data['id']);
			$userdata['name'] = $data['name'];
		}if (isset($data['profile_picture'])) {
			$userdata['profile_picture'] = $data['profile_picture'];
			$this->update_profile_firebase(array('profile_picture' => $data['profile_picture']), $data['id']);
		}if (isset($data['dob'])) {
			$userdata['dob'] = $data['dob'];
		}if (isset($data['latitude'])) {
			$userdata['latitude'] = $data['latitude'];
		}if (isset($data['longitude'])) {
			$userdata['longitude'] = $data['longitude'];
		}
		if (isset($data['notification'])) {
			$userdata['notification'] = $data['notification'];
		}

		if (isset($data['email'])) {
			if ($data['email'] != '') {
				$check_email_exist = $this->users_model->get_user_from_email(array('email' => $data['email']));
				if (!empty($check_email_exist) && ($check_email_exist['id'] != $data['id'])) {
					return_data(false, 'This email is already registerd with other user please use different email', json_decode("{}"));
				}
			}
			$userdata['email'] = $data['email'];
		}
		if (isset($data['recovery_email'])) {
			$userdata['recovery_email'] = $data['recovery_email'];
		}

		if (isset($data['gender'])) {
			$userdata['gender'] = $data['gender'];
		}if (isset($data['location'])) {
			$userdata['location'] = $data['location'];
		}
		if (isset($data['instagram'])) {
			$userdata['instagram'] = $data['instagram'];
		}
		if (isset($data['quickblox_id'])) {
			$userdata['quickblox_id'] = $data['quickblox_id'];
		}
		if (isset($data['spotify'])) {
			$userdata['spotify'] = $data['spotify'];
		}

		if (isset($data['headline'])) {
			$user_profile_data['headline'] = $data['headline'];
		}if (isset($data['about_me'])) {
			$user_profile_data['about_me'] = $data['about_me'];
		}if (isset($data['job'])) {
			$user_profile_data['job'] = $data['job'];
		}if (isset($data['education'])) {
			$user_profile_data['education'] = $data['education'];
		}if (isset($data['company'])) {
			$user_profile_data['company'] = $data['company'];
		}if (isset($data['interest'])) {
//intrest_type  1-dating
			$user_profile_data['interest'] = $data['interest'];
		}if (isset($data['looking_for'])) {
			$user_profile_data['looking_for'] = $data['looking_for'];
		}
		if (isset($data['looking_for_d'])) {
			$user_profile_data['looking_for_d'] = $data['looking_for_d'];
		}
		if (isset($data['looking_for_f'])) {
			$user_profile_data['looking_for_f'] = $data['looking_for_f'];
		}
		if (isset($data['industry'])) {
			$user_profile_data['industry'] = $data['industry'];
		}if (isset($data['education_level'])) {
			$user_profile_data['education_level'] = $data['education_level'];
		}if (isset($data['experience'])) {
			$user_profile_data['experience'] = $data['experience'];
		}if (isset($data['drinking'])) {
			$user_profile_data['drinking'] = $data['drinking'];
		}if (isset($data['smoking'])) {
			$user_profile_data['smoking'] = $data['smoking'];
		}if (isset($data['pets'])) {
			$user_profile_data['pets'] = $data['pets'];
		}if (isset($data['kids'])) {
			$user_profile_data['kids'] = $data['kids'];
		}if (isset($data['new_to_area'])) {
			$user_profile_data['new_to_area'] = $data['new_to_area'];
		}if (isset($data['releationship'])) {
			$user_profile_data['releationship'] = $data['releationship'];
		}if (isset($data['interest_in'])) {
			//male/female
			$user_profile_data['interest_in'] = $data['interest_in'];
		}
		if (isset($data['dislikes'])) {
			$user_profile_data['dislikes'] = $data['dislikes'];
		}if (isset($data['likes'])) {
			$user_profile_data['likes'] = $data['likes'];
		}if (isset($data['exercise'])) {
			$user_profile_data['exercise'] = $data['exercise'];
		}if (isset($data['religion'])) {
			$user_profile_data['religion'] = $data['religion'];
		}if (isset($data['zodiac'])) {
			$user_profile_data['zodiac'] = $data['zodiac'];
		}
		if (isset($data['politics'])) {
			$user_profile_data['politics'] = $data['politics'];
		}
		if (isset($data['height'])) {
			$user_profile_data['height'] = $data['height'];
		}
		if (isset($data['min_age'])) {
			$user_profile_data['min_age'] = $data['min_age'];
		}if (isset($data['max_age'])) {
			$user_profile_data['max_age'] = $data['max_age'];
		}if (isset($data['distance'])) {
			$user_profile_data['distance'] = $data['distance'];
		}
		if (isset($data['images'])) {
			$imgdata['id'] = $data['id'];
			$imgdata['images'] = $data['images'];
			//array('user_id' => 1,'images' => array('http://url1', 'http://url1', 'http://url1', 'http://url1'));
			$result = $this->users_model->save_images_ios($imgdata);

		}
		if (isset($data['insta_images'])) {
			$insta['id'] = $data['id'];
			$insta['insta_images'] = $data['insta_images'];
			//array('user_id' => 1,'images' => array('http://url1', 'http://url1', 'http://url1', 'http://url1'));
			$result = $this->users_model->save_insta_images($insta);

		}

		$user_detail = $this->users_model->get_user($data['id']);
		if (!empty($user_detail)) {
			$result = $this->users_model->update_user($userdata);
			if (!empty($user_profile_data)) {
				$this->db->where('user_id', $data['id']);
				$update_user_details = $this->db->update('user_profile_detail', $user_profile_data);
			}
			return_data(true, 'user Profile Updated', $this->complete_user_profile($data['id']));
		} else {

			return_data(false, 'user not Found', json_decode("{}"));
		}

	}

	private function validate_update_profile() {

		post_check();
		//$this->form_validation->set_message('is_unique', '%s already exist.');
		$this->form_validation->set_rules('id', 'id', 'trim|required');
		//$this->form_validation->set_rules('email','Email', 'trim|required|valid_email');
		//$this->form_validation->set_rules('mobile','Mobile', 'trim|required');
		//$this->form_validation->set_rules('gender','Gender', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
	public function send_otp($mobile, $otp, $message) {
		$authkey = "275020AhBtumebb5ccc1daa";
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?otp_length=4&authkey=$authkey&message=$message&sender=611332&mobile=$mobile&otp=$otp&email=&otp_expiry=&template=",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "",
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
		} else {
			return $response;

		}

	}

/***
 *      _                    _
 *     | |                  (_)
 *     | |      ___    __ _  _  _ __
 *     | |     / _ \  / _` || || '_ \
 *     | |____| (_) || (_| || || | | |
 *     |______|\___/  \__, ||_||_| |_|
 *                     __/ |
 *                    |___/
 */
	private function generate_signature($timestamp, $nonce) {
		// Generate signature

		//$timestamp = time(); // time() method must return current timestamp in UTC but seems like hi is return timestamp in current time zone
		$signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp;
		//. "&user[login]=" . USER_LOGIN . "&user[password]=" . USER_PASSWORD;
		//echo "stringForSignature: " . $signature_string . "<br><br>";
		return $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
	}

	public function quickbloxsignup($data, $user_id) {
		//	$name='ashwani';
		//	$user_id = 1419;
		//$user_ids = '96775893';
		$timestamp = time();
		$user_name = 'Burzz' . $user_id;
		$password = '12345678';
		$nonce = rand();
		$signature = $this->generate_signature($timestamp, $nonce);
		$auth_key = AUTH_KEY;
		$application_id = APPLICATION_ID;
// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.quickblox.com/session.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "application_id=$application_id&auth_key=$auth_key&timestamp=$timestamp&nonce=$nonce&signature=$signature");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Quickblox-Rest-Api-Version: 0.1.0';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$result = json_decode($result, true);
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = $result['session']['token'];

		/*--------------SIGNUP------------------*/
		$data_arra = array('user' => array('login' => $user_name, 'password' => '12345678', 'external_user_id' => $user_id
			, 'full_name' => $data['name'], 'email' => $data['email']));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.quickblox.com/users.json');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_arra, true));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		//$headers[] = 'QuickBlox-REST-API-Version: 0.1.0';
		$headers[] = "Qb-Token: " . $token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		//print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);

		$result = json_decode($result, true);

		if (array_key_exists('user', $result)) {
			return $result['user']['id'];
			//print_r($result);die;
		} else {
			return false;
		}
	}

	public function test() {

		$x = '{"user": {"login": "Elizabeth4", "password": "12345678", "email": "1212@dd.kkkk", "external_user_id": "3","full_name": "mohita", "phone": "123498561"}}';
		$c = json_decode($x, true);
		print_r($c['user']['']);
	}

	public function login() {
		$this->login_validate();
		$data = $this->input->post();
		///$log = "Time: " . date('Y-m-d, H') . PHP_EOL;
		//$log = $log . "Request " . json_encode($data) . PHP_EOL;
		// $log = $log . "file_Request_as_file " . json_encode($_FILES) . PHP_EOL;
		//$log = $log . "file_Request_as_post " . json_encode($this->input->post('profile_picture')) . PHP_EOL;
		//	file_put_contents('logs/login.txt', $log, FILE_APPEND);

		$sql = 'select * from users where password="' . md5($data['password']) . '" AND (email="' . $data['username'] . '" OR mobile="' . $data['username'] . '"
OR username="' . $data['username'] . '")';
		$record = $this->db->query($sql)->row_array();
               // print_r($record);die;
		if ($record) {
			if ($record['status'] == 0) {
                            if ($this->input->post('device_type') == 1 || $this->input->post('device_type') == 2) {
				$tokken = array(
					"id" => $record['id'],
					"device_type" => $this->input->post('device_type'),
					"device_token" => $this->input->post('device_token'),
				);
				$this->users_model->update_user($tokken);
				$this->update_profile_firebase(array('device_token' => $tokken['device_token']), $tokken['id']);
                                
			}
                        //print_r("okk");die;
			return_data(true, 'User authentication successful.', $this->complete_user_profile($record['id']));
                        }
                      
			if ($record['status'] ==1) {
				return_data(false, 'Login Disabled', json_decode("{}"));
			}
			if ($record['status'] == 2) {
				return_data(false, 'Your Account is Deleted', json_decode("{}"));
			}
			
		} else {
			return_data(false, 'User authentication failed.', json_decode("{}"));
		}

	}
	private function login_validate() {
		post_check();
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('device_token', 'device_token', 'trim|required');
		$this->form_validation->set_rules('device_type', 'device_type', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], json_decode("{}"));
		}
	}
	public function login_authentication() {
		//mobile,login_type,country_code
		$this->validate_login_authentication();
		$country_code = $this->input->post("country_code");
		$data = array(
			"mobile" => $this->input->post("mobile"),
		);
		$otp = (string) rand(1001, 9999);
		$msg = "Dear User, Your OTP for Sanskaar TV mobile verification is " . $otp;
		$login = $this->users_model->get_custum_user($data);
		if ($login) {
			if ($login['status'] == 0) {
				$login['user'] = 'login';
				$login['otp'] = $otp;
				//$sent_sms = $this->send_otp($data['mobile'], $otp, $msg);

				return_data(true, 'OTP has been sent for login.', $login);
			} elseif ($login['status'] == 1) {
				return_data(false, 'Your account has been disabled', array());
			} else {
				return_data(false, 'Your account has been deleted.', array());
			}
		} else {
			$check_email_exist = $this->users_model->get_user_from_email(array('email' => $this->input->post('email')));
			if ($check_email_exist) {
				return_data(false, 'This email is already registerd with other user please use different email.', array());
			} else {
				$return_id = $this->users_model->save_user($this->input->post());
				$signup = $this->users_model->get_user($return_id);
				$signup['user'] = 'signup';
				$signup['otp'] = $otp;
				if ($return_id) {
					$signup['id'] = (string) $return_id;
					//$sent_sms = $this->send_otp($country_code . $data['mobile'], $otp, $msg);
					return_data(true, 'OTP has been sent for user registration', $signup);
				} else {
					return_data(false, 'unsuccessful', array());
				}
			}

		}
	}

	private function validate_login_authentication() {
		post_check();
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('country_code', 'Country Code', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function send_sms($data) {
		//mobile and message.
		$mobile = $data['mobile'];
		$message = $data['message'];
		if ($mobile and $message) {
			return true;
		} else {
			return false;
		}

	}

/***
 *      ______                        _                                                       _
 *     |  ____|                      | |                                                     | |
 *     | |__  ___   _ __  __ _   ___ | |_    _ __    __ _  ___  ___ __      __ ___   _ __  __| |
 *     |  __|/ _ \ | '__|/ _` | / _ \| __|  | '_ \  / _` |/ __|/ __|\ \ /\ / // _ \ | '__|/ _` |
 *     | |  | (_) || |  | (_| ||  __/| |_   | |_) || (_| |\__ \\__ \ \ V  V /| (_) || |  | (_| |
 *     |_|   \___/ |_|   \__, | \___| \__|  | .__/  \__,_||___/|___/  \_/\_/  \___/ |_|   \__,_|
 *                        __/ |             | |
 *                       |___/              |_|
 */

	public function forget_password() {
		if ((isset($_POST['username'])) && ($_POST['username'] == '')) {

			return_data(false, 'Please enter mobile', json_decode("{}"));
		} else {
			$this->db->or_where("mobile", $_POST['username']);
			$this->db->or_where("email", $_POST['username']);
			$this->db->or_where("username", $_POST['username']);
			$result = $this->db->get('users')->row();
			if (empty($result)) {
				return_data(false, 'Mobile does not exist.', json_decode("{}"));
			} else {
				if (isset($_POST['otp'])) {
					if ($_POST['otp'] == '') {
						$otp = rand(1000, 9999);
						$mobile = $result->c_code . $result->mobile;
						$message = 'Your Burzz OTP is ' . $otp;
                                                sendsms($mobile, $message);
						return_data(true, 'OTP has been sent for user registration', array('otp' => $otp));
					} else {
						if ((isset($_POST['password'])) && ($_POST['password'] != '')) {

							$this->db->Where("id", $result->id);
							$this->db->update('users', array('password' => md5($_POST['password'])));
							return_data(true, 'Password Reset Successfully', json_decode("{}"));
						} else {
							return_data(false, 'Please enter password', json_decode("{}"));
						}

					}
				}
			}
		}
	}

/***
 *                                   _     _____          __                   _  _    _       _      _
 *         /\                       | |   |_   _|        / _|                 (_)| |  | |     (_)    | |
 *        /  \    __ _   ___  _ __  | |_    | |   _ __  | |_  ___   __      __ _ | |_ | |__    _   __| |
 *       / /\ \  / _` | / _ \| '_ \ | __|   | |  | '_ \ |  _|/ _ \  \ \ /\ / /| || __|| '_ \  | | / _` |
 *      / ____ \| (_| ||  __/| | | || |_   _| |_ | | | || | | (_) |  \ V  V / | || |_ | | | | | || (_| |
 *     /_/    \_\\__, | \___||_| |_| \__| |_____||_| |_||_|  \___/    \_/\_/  |_| \__||_| |_| |_| \__,_|
 *                __/ |
 *               |___/
 */
	public function validate_get_masters() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		//$this->form_validation->set_rules('update_key', 'update_key', 'trim|required');
		//$this->form_validation->set_rules('update_value', 'update_value', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function get_masters() {
		$this->validate_get_masters();
		$user_id = $this->input->post('user_id');
		$type = $this->input->post('type');
		$update_key = $this->input->post('update_key');
		$update_value = $this->input->post('update_value');

		if ($update_key != "" && $update_value != "") {
			$this->db->where('user_id', $user_id);
			$this->db->update('user_profile_detail', array($update_key => $update_value));
		}

		$result = $this->users_model->get_master($type, $user_id);

		return_data3(true, 'Master Data.', $result, (int) $this->db->get_where("users", array('id' => $user_id))->row()->is_verified);
	}

	public function validate_get_active_user() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function save_images() {
		$data['id'] = $this->input->post("id");
		$data['images'] = $this->input->post("images");
		//array('user_id' => 1,'images' => array('http://url1', 'http://url1', 'http://url1', 'http://url1'));
		$result = $this->users_model->save_images($data);

		return_data(true, 'updated Successfully.', $this->complete_user_profile($data['id']));
	}

	public function save_images_ios() {
		$data['id'] = $this->input->post("id");
		$data['images'] = $this->input->post("images");
		//array('user_id' => 1,'images' => array('http://url1', 'http://url1', 'http://url1', 'http://url1'));
		$result = $this->users_model->save_images_ios($data);

		return_data(true, 'updated Successfully.', $this->complete_user_profile($data['id']));
	}

	public function validate_user_deactivate() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function user_deactivate() {
		$this->validate_user_deactivate();
		$this->db->where("user_id", $this->input->post("user_id"));
		$result = $this->db->delete('user_images');
		$this->db->where("user_id", $this->input->post("user_id"));
		$result = $this->db->delete('user_profile_detail');
		$this->db->where("user_id", $this->input->post("user_id"));
		$result = $this->db->delete('insta_images');
		$this->db->where("id", $this->input->post("user_id"));
		$result = $this->db->delete('users');

		return_data(true, 'Deactivated Successfully.', json_decode("{}"));
	}

	public function user_activate($id) {
		$this->db->where("id", $id);
		$result = $this->db->update('users', array('status' => 0));
		return_data(true, 'Login Successfully', $this->complete_user_profile($id));
		//return_data(true, 'Deactivated Successfully.', json_decode("{}"));
	}

	public function get_user_education_detail() {
		$this->validate_user_deactivate();
		$user_id = $this->input->post('user_id');

		$this->db->where('user_id', $user_id);
		$result = $this->db->get('education_detail')->result_array();

		return_data(true, 'education details.', $result);
	}

	public function validate_post_user_education_detail() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('study', 'study', 'trim|required');
		$this->form_validation->set_rules('institution', 'institution', 'trim|required');
		$this->form_validation->set_rules('year', 'year', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function post_user_education_detail() {
		$this->validate_post_user_education_detail();
		$data['user_id'] = $this->input->post('user_id');
		$data['study'] = $this->input->post('study');
		$data['institution'] = $this->input->post('institution');
		$data['year'] = $this->input->post('year');

		$result = $this->db->insert('education_detail', $data);

		return_data(true, 'education details.', $this->complete_user_profile($this->input->post('user_id')));
	}

	public function validate_edit_user_education_detail() {
		post_check();

		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('study', 'study', 'trim|required');
		$this->form_validation->set_rules('institution', 'institution', 'trim|required');
		$this->form_validation->set_rules('year', 'year', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function edit_user_education_detail() {
		$this->validate_edit_user_education_detail();
		$data['user_id'] = $this->input->post('user_id');
		$data['study'] = $this->input->post('study');
		$data['institution'] = $this->input->post('institution');
		$data['year'] = $this->input->post('year');

		$this->db->where('id', $this->input->post('id'));
		$result = $this->db->update('education_detail', $data);

		return_data(true, 'job details.', $this->complete_user_profile($this->input->post('user_id')));
	}

	public function delete_user_education_detail() {
		$this->validate_user_deactivate();
		$user_id = $this->input->post('user_id');
		$id = $this->input->post('id');

		$this->db->where('user_id', $user_id);
		$this->db->where('id', $id);
		$this->db->delete('education_detail');

		return_data(true, 'education details.', $this->complete_user_profile($user_id));
	}
	private function education_detail($user_id) {
		$this->db->where('user_id', $user_id);
		return $this->db->get('education_detail')->result_array();
	}

	public function get_user_job_detail() {
		$this->validate_user_deactivate();
		$user_id = $this->input->post('user_id');

		$this->db->where('user_id', $user_id);
		$result = $this->db->get('company_desc')->result_array();

		return_data(true, 'company_desc details.', $result);
	}

	public function validate_post_user_job_detail() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('company', 'company', 'trim|required');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('duration', 'duration', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function post_user_job_detail() {
		$this->validate_post_user_job_detail();
		$data['user_id'] = $this->input->post('user_id');
		$data['company'] = $this->input->post('company');
		$data['title'] = $this->input->post('title');
		$data['duration'] = $this->input->post('duration');

		$result = $this->db->insert('company_desc', $data);

		return_data(true, 'job details.', $this->complete_user_profile($this->input->post('user_id')));
	}

	public function validate_edit_user_job_detail() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->set_rules('company', 'company', 'trim|required');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('duration', 'duration', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function edit_user_job_detail() {
		$this->validate_edit_user_job_detail();
		$data['user_id'] = $this->input->post('user_id');
		$data['company'] = $this->input->post('company');
		$data['title'] = $this->input->post('title');
		$data['duration'] = $this->input->post('duration');

		$this->db->where('id', $this->input->post('id'));
		$result = $this->db->update('company_desc', $data);

		return_data(true, 'job details.', $this->complete_user_profile($this->input->post('user_id')));
	}

	public function delete_user_job_detail() {
		$this->validate_user_deactivate();
		$user_id = $this->input->post('user_id');
		$id = $this->input->post('id');

		$this->db->where('user_id', $user_id);
		$this->db->where('id', $id);
		$this->db->delete('company_desc');

		return_data(true, 'job details.', $this->complete_user_profile($user_id));
	}
	private function job_detail($user_id) {
		$this->db->where('user_id', $user_id);
		return $this->db->get('company_desc')->result_array();
	}

	public function validate_user_detail() {
		post_check();
		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}
	public function get_user_detail() {
		$this->validate_user_detail();
		$request_user_id = $this->input->post('id');

		/* get all posts */
		$post = array();
		$post_meta['post_data'] = "";
		$user_setting = $this->db->get_where('users', array('id' => $request_user_id))->row_array();
		$settings = $this->db->get_where('user_profile_detail', array('user_id' => $request_user_id))->row_array();

		if (!empty($settings)) {
			$sql = "SELECT
    upd.*,
    u.name,
    u.profile_picture,
    u.gender,
    u.location,
    u.latitude,
    u.longitude,
    u.dob,
     u.instagram,
      u.quickblox_id,
     u.spotify,
    round (( 3959 * acos ( cos ( radians(" . $user_setting['latitude'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $user_setting['longitude'] . ") ) + sin ( radians(" . $user_setting['latitude'] . ") ) * sin( radians( latitude ) ) ) )) AS distance,
    (SELECT title FROM zodiac WHERE id = upd.zodiac) AS zodiac_text,
    (SELECT title FROM politics WHERE id = upd.politics) AS politics_text,
    (SELECT title FROM drinking WHERE id = upd.drinking) AS drinking_text,
    (SELECT title FROM education_level WHERE id = upd.education_level) AS education_level_text,
    (SELECT title FROM exercise WHERE id = upd.exercise) AS exercise_text,
    (SELECT title FROM industry WHERE id = upd.industry) AS industry_text,
    (SELECT title FROM kids WHERE id = upd.kids) AS kids_text,
    (SELECT title FROM looking_for WHERE id = upd.looking_for) AS looking_for_text,
    (SELECT title FROM looking_for_d WHERE id = upd.looking_for_d) AS looking_for_d_text,
    (SELECT title FROM looking_for_f WHERE id = upd.looking_for_f) AS looking_for_f_text,
    (SELECT title FROM new_to_area WHERE id = upd.new_to_area) AS new_to_area_text,
    (SELECT title FROM pets WHERE id = upd.pets) AS pets_text,
    (SELECT title FROM releationship WHERE id = upd.releationship) AS releationships_text,
    (SELECT title FROM religion WHERE id = upd.religion) AS religion_text,
    (SELECT title FROM smoking WHERE id = upd.smoking) AS smoking_text

FROM
    users AS u,
    user_profile_detail AS upd
WHERE
    u.id = upd.user_id AND
 u.id = $request_user_id and

    round (( 3959 * acos ( cos ( radians(" . $user_setting['latitude'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $user_setting['longitude'] . ") ) + sin ( radians(" . $user_setting['latitude'] . ") ) * sin( radians( latitude ) ) ) ))  < " . $settings['distance'] . " and interest=" . $settings['interest'];
			$user = $this->db->query($sql)->row_array();
			if (empty($user)) {
				return_data(false, 'No User Found', array('users' => $user));
			}

			$i = 0;
			$this->db->select('icon');
			$master = $this->db->get('master_relation')->row_array();
			$master = array_column($master, 'icon');
			// foreach ($users as $user) {
			$new_data['user_id'] = $user['user_id'];
			$new_data['instagram'] = $user['instagram'];
			$new_data['quickblox_id'] = $user['quickblox_id'];

			$new_data['spotify'] = $user['spotify'];
			$new_data['distance'] = $user['distance'];
			$new_data['images'] = $this->db->get_where('user_images', array('user_id' => $user['user_id']))->result_array();
			$new_data['insta_images'] = $this->db->get_where('insta_images', array('user_id' => $user['user_id']))->result_array();
			$new_data['name'] = ($user['name'] == NULL ? '' : $user['name']);
			$new_data['profile_picture'] = ($user['profile_picture'] == NULL ? '' : $user['profile_picture']);
			$new_data['gender'] = ($user['gender'] == NULL ? '' : $user['gender']);
			$new_data['location'] = ($user['location'] == NULL ? '' : $user['location']);
			$new_data['latitude'] = ($user['latitude'] == NULL ? '' : $user['latitude']);
			$new_data['longitude'] = ($user['longitude'] == NULL ? '' : $user['longitude']);
			$new_data['dob'] = ($user['dob'] == NULL ? '' : $user['dob']);
			$new_data['dislikes'] = ($user['dislikes'] == NULL ? '' : $user['dislikes']);
			$new_data['interest_in'] = ($user['interest_in'] == NULL ? '' : $user['interest_in']);
			$new_data['likes'] = ($user['likes'] == NULL ? '' : $user['likes']);
			$new_data['experience'] = ($user['experience'] == NULL ? '' : $user['experience']);
			$new_data['education_level'] = ($user['education_level_text'] == NULL ? '' : $user['education_level_text']);
			$new_data['industry'] = ($user['industry_text'] == NULL ? '' : $user['industry_text']);
			$new_data['looking_for'] = ($user['looking_for_text'] == NULL ? '' : $user['looking_for_text']);
			$new_data['looking_for_d'] = ($user['looking_for_d_text'] == NULL ? '' : $user['looking_for_d_text']);
			$new_data['looking_for_f'] = ($user['looking_for_f_text'] == NULL ? '' : $user['looking_for_f_text']);
			$new_data['new_to_area'] = ($user['new_to_area_text'] == NULL ? '' : $user['new_to_area_text']);
			$new_data['releationship'] = ($user['releationships_text'] == NULL ? '' : $user['releationships_text']);
			$new_data['headline'] = ($user['headline'] == NULL ? '' : $user['headline']);
			$new_data['about_me'] = ($user['about_me'] == NULL ? '' : $user['about_me']);
			$new_data['job'] = ($user['job'] == NULL ? '' : $user['job']);
			$new_data['company'] = ($user['company'] == NULL ? '' : $user['company']);
			$new_data['interest'] = ($user['interest'] == NULL ? '' : $user['interest']);
			$new_data['preference'] = array(
				array('text' => ($user['zodiac_text'] == NULL ? '' : $user['zodiac_text']), 'icon' => base_url('icon/zodiac.png')),
				array('text' => ($user['politics_text'] == NULL ? '' : $user['politics_text']), 'icon' => base_url('icon/politics.png')),
				array('text' => ($user['drinking_text'] == NULL ? '' : $user['drinking_text']), 'icon' => base_url('icon/drink.png')),
				array('text' => ($user['education'] == NULL ? '' : $user['education']), 'icon' => base_url('icon/education.png')),
				array('text' => ($user['smoking_text'] == NULL ? '' : $user['smoking_text']), 'icon' => base_url('icon/smoking.png')),
				array('text' => ($user['exercise_text'] == NULL ? '' : $user['exercise_text']), 'icon' => base_url('icon/exercise.png')),
				array('text' => ($user['height'] == NULL ? '' : $user['height']), 'icon' => base_url('icon/height.png')),
				array('text' => ($user['kids_text'] == NULL ? '' : $user['kids_text']), 'icon' => base_url('icon/kids.png')),
				array('text' => ($user['pets_text'] == NULL ? '' : $user['pets_text']), 'icon' => base_url('icon/pets.png')),
				array('text' => ($user['religion_text'] == NULL ? '' : $user['religion_text']), 'icon' => base_url('icon/religion.png')),

			);
			$this->db->select('image_url');
			$new_data['images'] = $this->db->get_where('user_images', array('user_id' => $user['user_id']))->result_array();
			$new_data['education_data'] = $this->db->get_where('education_detail', array('user_id' => $user['user_id']))->result_array();
			$new_data['job_data'] = $this->db->get_where('company_desc', array('user_id' => $user['user_id']))->result_array();
			//echo json_encode($details);die;
			array_unshift($new_data['images'], array('image_url' => $new_data['profile_picture']));

			$response = $new_data;

			//}

			return_data(true, 'Users List', $response);
		} else {
			return_data(false, 'Invalid User', $response);
		}

	}

	public function is_verified() {
		$this->validate_user_detail();
		$id = $this->input->post('id');
		$response['is_verified'] = $this->users_model->get_user_field('is_verified', $id);
		return_data(true, 'Success', $response);
	}

}