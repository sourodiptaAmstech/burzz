<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Guru_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_guru_list($data) {
		$where_id = '';
		$result = array();
		$with_is_follow = array();
		if ($data['last_guru_id'] != '') {
			$where_id = " and id < " . $data['last_guru_id'];
		}
		$result = $this->db->query("SELECT * from guru where 1=1 and status=0 $where_id order by id desc limit 0,10")->result_array();
		if ($result) {
			foreach ($result as $guru) {
				$liked = $this->is_already_like_guru(["user_id" => $data['user_id'], "guru_id" => $guru['id']]);
				$guru['is_like'] = '0';
				if ($liked) {
					$guru['is_like'] = '1';
				}
				$followed = $this->is_already_follow_guru(["user_id" => $data['user_id'], "guru_id" => $guru['id']]);
				$guru['is_follow'] = '0';
				if ($followed) {
					$guru['is_follow'] = '1';
				}

				$with_is_follow[] = $guru;
			}
			$result = $with_is_follow;
		}
		return $result;
	}

	public function get_guru_list_keyword($data) {
		$keyword = $data['keyword'];
		$result = array();
		$with_is_follow = array();

		$result = $this->db->query("SELECT * from guru where status=0  and  MATCH(`name`, `description`) AGAINST ('" . $keyword . "' IN NATURAL LANGUAGE MODE) order by id desc ")->result_array();
		if ($result) {
			foreach ($result as $guru) {
				$liked = $this->is_already_like_guru(["user_id" => $data['user_id'], "guru_id" => $guru['id']]);
				$guru['is_like'] = '0';
				if ($liked) {
					$guru['is_like'] = '1';
				}
				$followed = $this->is_already_follow_guru(["user_id" => $data['user_id'], "guru_id" => $guru['id']]);
				$guru['is_follow'] = '0';
				if ($followed) {
					$guru['is_follow'] = '1';
				}

				$with_is_follow[] = $guru;
			}
			$result = $with_is_follow;
		}
		return $result;
	}

	public function guru_follow($data) {

		$data['creation_time'] = milliseconds();

		$this->db->insert('guru_follow', $data);
		$this->db->insert_id();

		$count = $this->db->query('select count(id) as total from guru_follow where guru_id =' . $data['guru_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['guru_id']);
		$this->db->set('followers_count', $count);
		$this->db->update("guru");
	}

	public function is_already_follow_guru($data) {
		$this->db->where('guru_id', $data['guru_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("guru_follow")->row_array();
		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function unfollow_guru($data) {
		$this->db->where('guru_id', $data['guru_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->delete("guru_follow");

		$count = $this->db->query('select count(id) as total from guru_follow where guru_id =' . $data['guru_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['guru_id']);
		$this->db->set('followers_count', $count);
		$this->db->update("guru");

		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function guru_like($data) {

		$data['creation_time'] = milliseconds();

		$this->db->insert('guru_likes', $data);
		$this->db->insert_id();

		$count = $this->db->query('select count(id) as total from guru_likes where guru_id =' . $data['guru_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['guru_id']);
		$this->db->set('likes_count', $count);
		$this->db->update("guru");
	}

	public function is_already_like_guru($data) {
		$this->db->where('guru_id', $data['guru_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("guru_likes")->row_array();
		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function unlike_guru($data) {
		$this->db->where('guru_id', $data['guru_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->delete("guru_likes");

		$count = $this->db->query('select count(id) as total from guru_likes where guru_id =' . $data['guru_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['guru_id']);
		$this->db->set('likes_count', $count);
		$this->db->update("guru");

		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

}