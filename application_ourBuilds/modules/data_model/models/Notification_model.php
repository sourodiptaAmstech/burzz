<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Notification_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	public function get_notification_list($data){
		$result=array();
		$user_id=$data['user_id'];
		$result=$this->db->query("SELECT *,
									CASE
								    WHEN post_type = 'news' THEN (select image from news where news.id=post_id)
    								WHEN post_type = 'bhajan' THEN (select image from bhajan where bhajan.id=post_id)
									END as image
									from notification
									where user_id=$user_id
									order by creation_time desc limit 0,10")->result_array();
		return $result;
	}

	public function get_notification_detail($data){
		$result=array();
		$user_id=$data['user_id'];
		$post_type=$data['post_type'];
		$post_id=$data['post_id'];
		if($post_type=='news'){
			$result=$this->db->query("SELECT * from news where id=$post_id ")->row_array();
		}
		if($post_type=='bhajan'){
			$result=$this->db->query("SELECT * from bhajan where id=$post_id ")->row_array();
			if($result){
				$liked = $this->is_already_like_bhajan(["user_id"=>$user_id,"bhajan_id"=>$post_id]);
				$result['is_like']= '0';
				if($liked){
					$result['is_like']= '1';
				}	
			}
		}
		return $result;
	}

	public function is_already_like_bhajan($data){
		$this->db->where('bhajan_id',$data['bhajan_id']);
		$this->db->where('user_id',$data['user_id']);
		$result = $this->db->get("bhajan_likes")->row_array();
		if($result){
			return TRUE;
		}
		return FALSE;
	}

}