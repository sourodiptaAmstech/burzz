<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class News_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    
    public function insert_news($data){

     $result = $this->db->insert('news',$data);
        if($result){
                return true;
            }
            else{
                return false;
            }
    }    
    
    public function update_news($id,$update_data){
        $this->db->where('id',$id);
		$this->db->update('news',$update_data);
        return true;
       }   

    public function delete_news($id){
    $this->db->where('id',$id);
	$this->db->delete('news');
    return true;
       }  

}