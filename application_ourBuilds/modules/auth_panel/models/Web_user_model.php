<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Web_user_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_user_basic_detail($id) {
        $this->db->where('id', $id);
        return $query = $this->db->get('users')->row_array();
    }

    public function get_user_profile($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('users')->row_array();
        return $query;
    }

    public function get_user_registered_details($id) {
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_registration_data')->row_array();
        return $query;
    }

    public function get_user_profile_details($id) {

        //$this->db->join('course_instructor_information', 'users.id = request_helping_hand.request_id');
        //	$this->db->where('user_id', $id);
        //	$query = $this->db->get('user_profile_detail')->row_array();

        $sql = "SELECT
    upd.*,
    u.name,
    u.profile_picture,
    u.gender,
    u.location,
    u.latitude,
    u.longitude,
    u.dob,
    (SELECT title FROM zodiac WHERE id = upd.zodiac) AS zodiac_text,
    (SELECT title FROM drinking WHERE id = upd.drinking) AS drinking_text,
    (SELECT title FROM education_level WHERE id = upd.education_level) AS education_level_text,
    (SELECT title FROM exercise WHERE id = upd.exercise) AS exercise_text,
    (SELECT title FROM industry WHERE id = upd.industry) AS industry_text,
    (SELECT title FROM kids WHERE id = upd.kids) AS kids_text,
    (SELECT title FROM looking_for WHERE id = upd.looking_for) AS looking_for_text,
    (SELECT title FROM new_to_area WHERE id = upd.new_to_area) AS new_to_area_text,
    (SELECT title FROM pets WHERE id = upd.pets) AS pets_text,
    (SELECT title FROM releationship WHERE id = upd.releationship) AS releationships_text,
    (SELECT title FROM religion WHERE id = upd.religion) AS religion_text,
    (SELECT title FROM smoking WHERE id = upd.smoking) AS smoking_text

FROM
    users AS u,
    user_profile_detail AS upd
WHERE
    u.id = upd.user_id AND u.id =$id";
        $query = $this->db->query($sql)->row_array();

        if ($query) {
            //echo "<pre>";print_r($query);die;
            return $query;
        } else {
            return false;
        }
    }

    public function get_instructor_rating_details_by_id($id) {
        $this->db->select("course_instructor_rating.*,users.name,DATE_FORMAT(FROM_UNIXTIME(course_instructor_rating.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time");
        $this->db->join('users', 'users.id=course_instructor_rating.user_id');
        $this->db->where('course_instructor_rating.id', $id);
        return $this->db->get('course_instructor_rating')->row_array();
    }

    public function update_user_status($status, $id) {
        if ($status == 'delete') {
            $this->db->where('id', $id);
            $this->db->update('users',array('status'=>2));
            return true;            
            
           // $data = array('status' => 2, 'device_token' => '');
           // $this->db->where('id', $id);
           // return $this->db->update('users', $data);
        } elseif ($status == 'disable') {
            $data = array('status' => 1, 'device_token' => '');
            $this->db->where('id', $id);
            return $this->db->update('users', $data);
        } elseif ($status == 'enable') {
            $data = array('status' => 0);
            $this->db->where('id', $id);
            return $this->db->update('users', $data);
        }
    }

    public function add_instructor_id($id) {
        $data = array('user_id' => $id);
        $this->db->where('user_id', $id);
        $checkStatus = $this->db->get('course_instructor_information')->row_array();
        if (empty($checkStatus)) {
            $result = $this->db->insert("course_instructor_information", $data);
        } else {
            
        }
    }

    public function delete_review($id) {

        $this->db->where('id', $id);
        $this->db->delete('course_instructor_rating');
        return true;
    }

    public function update_user_name($data) {
        //print_r($data);
        $this->db->set('name', $data['name']);
        $this->db->where('id', $data['id']);
        $this->db->update('users');
        //echo $this->db->last_query();
        return true;
    }

    public function update_verify_status($id) {
        $this->db->where('id', $id);
        return $this->db->update('users', array('is_verified' => 1));
    }

    // this->db->set('last_login','current_login',false);
    // $this->db->where('id','some_id');
    // $this->db->update('login_table',$data);
}
