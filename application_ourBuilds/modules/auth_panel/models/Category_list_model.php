<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Category_list_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    
    public function get_category_list() {
      	return  $this->db->get("master_category")->result_array();
    }

    public function get_sub_category_list_by_id($id) {
    	$this->db->where('id',$id);
    	return  $this->db->get("master_category")->row_array();
    }


    public function get_sub_category_list() {
    	return  $this->db->get("master_category")->result_array();
    }

    public function get_sub_sub_category_list_by_id($id) {
    	$this->db->where('id',$id);
    	return  $this->db->get("master_category")->row_array();
    }
}