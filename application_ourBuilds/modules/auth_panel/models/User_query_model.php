<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class User_query_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }


    public function getUserQueryDetailById($id) {
        //get User id from user query table..
        $sql = $this->db->select('user_id')
        ->where('id',$id)
        ->get('user_queries')->row_array();
        $id = $sql['user_id'];
        // get user query record by user id from  user query table..
     $result = $this->db->select('uq.*,u.name,u.email,u.mobile,u.creation_time,u.dams_tokken,u.profile_picture')
     ->where('uq.user_id', $id)
     ->join('users as u', 'uq.user_id = u.id', 'LEFT')
     ->get('user_queries as uq')->result_array();

     foreach ($result as $key => $value) {
        $query_reply_sql = $this->db->select('uqar.*,bu.username')
        ->where('uqar.query_id',$value['id'])
        ->join('backend_user as bu','uqar.backend_user_id = bu.id', 'LEFT')
        ->get('user_query_admin_reply as uqar')->result_array();
        $result[$key]['query_reply'] = $query_reply_sql;
     }
     return $result;
    }
    

}