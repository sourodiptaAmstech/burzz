<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Video_control_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    
    public function get_video_list() {
      	return  $this->db->get("video_master")->result_array();
    }



    public function get_video($id){ 
		$id = $id['id'];
		$this->db->where('id',$id);
		$result = $this->db->get('video_master')->row_array();
		return $result;
	}
	public function insert_video($data){ 
		$this->db->insert('video_master',$data);
		return $this->db->insert_id();
	}
	public function update_video($data){ 
		$id = $data['id'];
		$this->db->where('id',$id);
		$result = $this->db->update('video_master',$data);
		return $result;
	}

	public function delete_filter($id){ 
		$this->db->where('id',$id);
		$result = $this->db->delete('video_filtration');
		return $result;
	}

	public function delete_video_comment($id,$video_id) {
		$this->db->where('id',$id);
		$status = $this->db->delete('video_master_comment');
		if($status){
			$count = $this->db->query('select count(id) as total from video_master_comment where video_id ='.$video_id)->row_array();
			$count = ($count && $count['total'] > 0 )?$count['total']:0;
	
			$this->db->where('id',$video_id);
			$this->db->set('comments', $count);
			$this->db->update("video_master");
			return true;
		}
		else{
			return false;
		}

	}

	public function check_featured_and_new($video_id,$subcat_id,$is_featured,$is_new){
		
			$total_count = $this->db->query("SELECT count(id) total FROM `video_master` WHERE `id` != $video_id and `sub_cat` = $subcat_id and (`featured` = $is_featured or `is_new` = $is_new) ")->row_array();
		//echo $this->db->last_query(); die;
		return	$total_count;
		
	}

	 public function get_tag_list() {
      	return  $this->db->get("video_search_tag_list")->result_array();
    }

    public function insert_video_tag($data){ 
		$this->db->insert('video_search_tag_list',$data);
		return $this->db->insert_id();
	}

    public function check_unique_tag($data){ 
		$id = $data['id'];
		$this->db->select('tag_name');
		$this->db->where('id',$id);
		$result = $this->db->get('video_search_tag_list')->row_array();
		return $result['tag_name'];
	}

	public function get_tag($id){ 
		$id = $id['id'];
		$this->db->where('id',$id);
		$result = $this->db->get('video_search_tag_list')->row_array();
		return $result;
	} 

	public function update_tag($data){ 
		$id = $data['id'];
		$this->db->where('id',$id);
		$result = $this->db->update('video_search_tag_list',$data);
		return $result;
	}

	public function delete_video_tag($id){ 
		$this->db->where('id',$id);
		$result = $this->db->delete('video_search_tag_list');
		return $result;
	}
}
