 <link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>/tags/bootstrap-tagsinput.css">
<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-12">
  <section class="panel">
      <header class="panel-heading">
         Add Product
      </header>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
       <div class="col-sm-6">
       	<div class="form-group">
                  <label for="exampleInputPassword1">Category</label>
                  <select class="form-control" name="product_category" id="product_category" onchange="getCategoeyData()" disabled="">
                  	<option value="">Select Category</option>
                  	 <?php
              
				$this->db->select('id,title');
				$category1= $this->db->get_where('master_category',array('parent_id'=>0,'status'=>2))->result_array();
              foreach($category1 as $cat){			  
			  	echo"<option value='".$cat['id']."' ".($product_detail['product_category']==$cat['id']?'selected':'').">".$cat['title']."</option>";
			  	featch($cat['title'],$cat['id'],$product_detail['product_category']);
			  			  	
			  }?>
                  </select>
                  <span style="color:red"><?php echo form_error("product_category"); ?></span>
              </div>
           
           
           <div class="form-group">
                  <label for="exampleInputPassword1">Brand</label>
                  <select class="form-control" name="product_brand">
                  	<option value="">Select Brand</option>
                  	 <?php
              
				
				$brands= $this->db->get_where('brands',array('status'=>2))->result_array();
              foreach($brands as $brand){			  
			  	echo"<option value='".$brand['id']."' ".($product_detail['product_brand']==$brand['id']?'selected':'').">".$brand['brand_name']."</option>";
			  	
			  			  	
			  }?>
                  </select>
                  <span style="color:red"><?php echo form_error("product_brand"); ?></span>
              </div>   
              
         <div class="form-group">
                  <label for="exampleInputPassword1">Product Tax</label>
                  <select class="form-control" name="product_tax">
                  	<option value="">Select Tax</option>
                  	 <?php
              
				
				$taxs= $this->db->get_where('tax',array('status'=>2))->result_array();
              foreach($taxs as $tax){			  
			  	echo"<option value='".$tax['id']."' ".($product_detail['product_tax']==$tax['id']?'selected':'')." >".$tax['tax_value']."</option>";
			  	
			  			  	
			  }?>
                  </select>
                  <span style="color:red"><?php echo form_error("product_tax"); ?></span>
              </div>        
              
              
              
       </div>
 				
 				<div class="col-sm-6">       
              <div class="form-group">
                  <label for="exampleInputPassword1">Product Name</label>
                  <input type="text" placeholder="Product Name" value="<?php echo $product_detail['product_name']?>" name="product_name" class="form-control">
                  <span style="color:red"><?php echo form_error("product_name"); ?></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Product Code</label>
                  <input type="text" placeholder="Product Code" value="<?php echo $product_detail['product_code']?>" name="product_code" class="form-control">
                  <span style="color:red"><?php echo form_error("product_code"); ?></span>
              </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Discount</label>
                  <select class="form-control product_discount" name="product_discount" onchange="refresh_stock()">
                  	<option value="">Select Discount</option>
                 	<option value="1" <?php echo ($product_detail['product_discount']==1?'selected':'');?>>Percentage</option>
                 	<option value="2" <?php echo ($product_detail['product_discount']==2?'selected':'');?>>Fixed</option>
                 
                  </select>
                  <span style="color:red"><?php echo form_error("product_discount"); ?></span>
              </div>   
               
              
           </div>  
            <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea name="product_description" class="form-control"><?php echo $product_detail['product_description']?></textarea>
                  <span style="color:red"><?php echo form_error("product_description"); ?></span>
              </div>  
              
              </hr>
              <h5 >PRODUCT PARAMETERS & STOCK</h5>
              </hr>
                            <div class="form-group col-md-12 ">
                 </div>  
               
                
                <div class="input_fields_wrap">
    <button class="add_field_button btn btn-small btn btn-success">Add More Stock</button>
    <?php 
    foreach($stock as $s){    
    ?>
    <div> 
    <input type="hidden" value="<?php echo $s['id'];?>" name="id[]">
    	<div class="form-group col-md-2">
    		<label for="exampleInputEmail1">stock </label>
    	<input name="stock[]" value="<?php echo $s['stock'];?>" type="text" placeholder="" class="form-control input-sm required">
    		
    	</div>
    <div class="form-group  col-md-2">
                    <label for="exampleInputEmail1">Price </label>
                    <input name="price[]" value="<?php echo $s['price'];?>" type="text" placeholder="" class="price form-control input-sm calculate_discount required">
                
                </div>
                <div class="form-group col-md-2">
                   
                    <label for="exampleInputEmail1">Discount</label>
                    <input type="text" name="discount[]" value="<?php echo $s['discount'];?>"  placeholder="" class="discount form-control col-md-2 input-sm for_dams_input required calculate_discount_change">
                   
                </div>
                <div class="form-group col-md-2">
                  
                    <label for="exampleInputEmail1">Pack Size</label>
                   <select  name="pack_size[]" class="form-control input-sm for_dams_input required pack_size">
                   	<option>Select Pack Size</option>
                   	<?php
                   	foreach($pack_size as $pks){
						echo "<option value='".$pks."' ".($pks==$s['pack_size']?'selected':'').">$pks</option>";
					}
                   	
                   	
                   	?>
                   </select>
                  
                </div>
                 <div class="form-group col-md-2">
                  
                    <label for="exampleInputEmail1">price(after discount)</label>
                   <input  name="price_after_discount[]" type="text" value="<?php echo $s['price_after_discount'];?>"  placeholder="" class="price_after_discount form-control input-sm for_dams_input required " >
                  
                </div>
                 <div class="form-group col-md-1">
                  
                    <label for="exampleInputEmail1">Image</label>
                    <?php if($s['image']!=''){
						echo "<img src='".$s['image']."' class='img-thumbnail'>";
						}?>
                   <input  name="image[]" type="file" value="0"  placeholder="" class="form-control input-sm for_dams_input required ">
                  
                </div>
                <div class="form-group col-md-1"><a href="#" class="remove_field btn btn-danger btn-small" style="margin-top: 20px;">X</a></div>
                
                
                </div>
                 <?php } ?>  
</div>
             
              <button class="btn btn-info col-md-2" type="submit">Submit</button>	
              
              
          </form>
      </div>
  </section>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$assets=ASSETS;
$custum_js = <<<EOD

 <script type="text/javascript" charset="utf8" src="$assets/tags/bootstrap-tagsinput.min.js"></script>
 
 
 <script>
 function getCategoeyData(){
 	
 	
 	var catId=$('#product_category').val();
 	//console.log(catId);
 	$.ajax({
        url :"$adminurl"+"products/getCategoeyData/", // json datasource
        type: "post",
        data: 'id='+catId ,
        success: function (response) {
        	$('.pack_size').html(response);
           // you will get response from your php page (what you echo or print)                 

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
 }
 
 
 
 $(document).on('keyup','.calculate_discount',function(){
 	var selector = $(this).parent().parent();
 	var price=$(this).val();
 	var discount = selector.find('div:eq(2)').find('input').val();
 	
 	var dis_type=$('.product_discount').val();
 	if(dis_type==1){
	var realprice=price-(price*discount/100);	
	}else if(dis_type==2){
		var realprice=price-discount;
	}else{
		var realprice=price;
	}
 	
 	selector.find('div:eq(4)').find('input').val(realprice);
 });
 
 $(document).on('keyup','.calculate_discount_change',function(){
 	var selector = $(this).parent().parent();
 	var price=selector.find('div:eq(1)').find('input').val();
 	var discount = $(this).val();
 	
 	var dis_type=$('.product_discount').val();
 	if(dis_type==1){
	var realprice=price-(price*discount/100);	
	}else if(dis_type==2){
		var realprice=price-discount;
	}else{
		var realprice=price;
	}
 	
 	selector.find('div:eq(4)').find('input').val(realprice);
 	
 });
 
 
 
 
 function refresh_stock(){
 	
 	$('.calculate_discount').keyup();
 }
 
 
 
 
 
 
 
 
 
 
 
 </script>
 
      
              <script>
              $(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			var catId=$('#product_category').val();
			if(catId==''){
				alert('Please Select Category First');
				return;
			}
			$.ajax({
        url :"$adminurl"+"products/getCategoeyData/", // json datasource
        type: "post",
        data: 'id='+catId ,
        success: function (response) {
        	//$('.pack_size').html(response);
           	$(wrapper).append('<div><div class="form-group col-md-2"><label for="exampleInputEmail1">stock </label><input name="stock[]" value="0" type="text" placeholder="" class="form-control input-sm required"></div><div class="form-group  col-md-2"><label for="exampleInputEmail1">Price </label><input name="price[]" value="0" type="text" placeholder="" class="price form-control input-sm required calculate_discount"></div><div class="form-group col-md-2"><label for="exampleInputEmail1">Discount</label><input type="text" name="discount[]" value="0"  placeholder="" class="discount form-control col-md-2 input-sm for_dams_input required calculate_discount_change"></div><div class="form-group col-md-2"><label for="exampleInputEmail1">Pack Size</label><select  name="pack_size[]" class="form-control input-sm for_dams_input required pack_size">'+response+'</select></div><div class="form-group col-md-2"><label for="exampleInputEmail1">price(after discount)</label><input  name="price_after_discount[]" type="text" value="0"  placeholder="" class="price_after_discount form-control input-sm for_dams_input required "> </div><div class="form-group col-md-1"><label for="exampleInputEmail1">Image</label><input  name="image[]" type="file" value="0"  placeholder="" class="form-control input-sm for_dams_input required "></div><div class="form-group col-md-1"><a href="#" class="remove_field btn btn-danger btn-small" style="margin-top: 20px;">X</a></div></div>'); //add input box
           
           
           // you will get response from your php page (what you echo or print)                 

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
			
			
			
			
		
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
	})
});
              </script>
              
          
EOD;






/*

<div><div class="form-group col-md-2"><label for="exampleInputEmail1">stock </label><input name="stock[]" value="0" type="text" placeholder="" class="form-control input-sm required"></div><div class="form-group  col-md-2"><label for="exampleInputEmail1">Price </label><input name="price[]" value="0" type="text" placeholder="" class="form-control input-sm required"></div><div class="form-group col-md-1"><label for="exampleInputEmail1">Discount</label><input type="text" name="discount[]" value="0"  placeholder="" class="form-control col-md-2 input-sm for_dams_input required"></div><div class="form-group col-md-2"><label for="exampleInputEmail1">Pack Size</label><input  name="pack_size[]" type="text" value="0"  placeholder="" class="form-control input-sm for_dams_input required "></div><div class="form-group col-md-2"><label for="exampleInputEmail1">price(after discount)</label><input  name="price_after_discount[]" type="text" value="0"  placeholder="" class="form-control input-sm for_dams_input required "> </div><div class="form-group col-md-1"><label for="exampleInputEmail1">Image</label><input  name="image[]" type="file" value="0"  placeholder="" class="form-control input-sm for_dams_input required "></div><div class="form-group col-md-1"><a href="#" class="remove_field btn btn-danger btn-small" style="margin-top: 20px;">X</a></div></div>


*/






echo modules::run('auth_panel/template/add_custum_js',$custum_js );


?>


