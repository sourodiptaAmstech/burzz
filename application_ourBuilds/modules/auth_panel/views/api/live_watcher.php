<?php //echo current_url();?>
<div id="live_events"></div>
<div class="my_api_view panel">
<div class="api_file_meta col-md-9">
<pre style="font-family: Verdana;">
<span class="btn btn-success btn-xs bold">URL: </span><p id="request_url"></p>
<span class="bold">INPUT PARAMETER -:</span>
<figure class="highlight" id="request" style="background:#929499;color: white;">
		mobile -&gt; 9876543210
	</figure>
<span class="bold">RESPONSE PARAMETER -:</span>
<figure class="highlight" id="request" style="background:#929499;color: white;">
		mobile -&gt; 9876543210
	</figure>



</pre>
</div>


</div>

<?php
$base_url = "https://demos.mydevfactory.com/debarati/burzz";//$_SERVER['SERVER_NAME'];
$custum_js = <<<EOD
<script src='//cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js'></script>


	<script>
var socket = io.connect('//$base_url:3000');
socket.on('connect', function () {
   console.log('connected');

    socket.on('request', function (data) {
        console.log(data);
		$('#request_url').text(data.url);
		delete data.url;
		$('#request').text(JSON.stringify(data));

    });

    socket.on('disconnect', function () {
        console.log('disconnected');
    });
});



</script>

EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);