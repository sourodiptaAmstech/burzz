 <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>/tags/bootstrap-tagsinput.css">
<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Edit Brand
      </header>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
 <input type="hidden" placeholder="Title" value="<?php echo $packages['id']; ?>" name="id" class="form-control">
              <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Title</label>
                  <input type="text" placeholder="Title" value="<?php echo $packages['name']; ?>" name="name" class="form-control">
                  <span style="color:red"><?php echo form_error("name"); ?></span>
              </div>
               <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Price($)</label>
                  <input type="text" placeholder="Price" value="<?php echo $packages['price']; ?>" name="price" class="form-control">
                  <span style="color:red"><?php echo form_error("price"); ?></span>
              </div>
                 <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Price(&#8358;)</label>
                  <input type="text" placeholder="Price" value="<?php echo $packages['price_2']; ?>" name="price_2" class="form-control">
                  <span style="color:red"><?php echo form_error("price_2"); ?></span>
              </div>
              <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Text</label>
                  <input type="text" placeholder="Text" value="<?php echo $packages['text']; ?>" name="text" class="form-control">
                  <span style="color:red"><?php echo form_error("text"); ?></span>
              </div>
              <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Apple Product Id</label>
                  <input type="text" placeholder="Apple Product Id" value="<?php echo $packages['apple_product_id']; ?>" name="apple_product_id" class="form-control">
                  <span style="color:red"><?php echo form_error("apple_product_id"); ?></span>
              </div>
               <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Validity</label>
                  <input type="text" placeholder="Validity" value="<?php echo $packages['validity']; ?>" name="validity" class="form-control">
                  <span style="color:red"><?php echo form_error("validity"); ?></span>
              </div>


              <button class="btn btn-info pull-right" type="submit">Submit</button>
          </form>
      </div>
  </section>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$assets = ASSETS;
$custum_js = <<<EOD

 <script type="text/javascript" charset="utf8" src="$assets/tags/bootstrap-tagsinput.min.js"></script>


EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);