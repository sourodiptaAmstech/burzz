<!-- subcategory list -->
<?php

$faqs = $this->db->get_where('faqs', array('id' => $id))->row_array();
?>
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		FAQs
		<span class="tools pull-right">
		<a href="javascript:;" class="fa fa-chevron-down"></a>

		</span>
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<div class="col-sm-12">

		<form role="form" method="post" enctype="multipart/form-data">
 <input type="hidden" placeholder="Title" value="<?php echo $faqs['id'] ?>" name="id" class="form-control">
              <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Question</label>
                  <input type="text" placeholder="Title" value="<?php echo $faqs['question'] ?>" name="question" class="form-control">
                  <span style="color:red"><?php echo form_error("question"); ?></span>
              </div>


               <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Answer</label>
                  <textarea name="description" class="form-control ckeditor"><?php echo $faqs['description'] ?></textarea>
                  <span style="color:red"><?php echo form_error("description"); ?></span>
              </div>


              <button class="btn btn-info pull-right" type="submit">Submit</button>
          </form>
          </div>



		</div>
  </div>



	</section>





</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>



EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
