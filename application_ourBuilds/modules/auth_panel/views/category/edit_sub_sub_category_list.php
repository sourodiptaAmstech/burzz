<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Edit Sub category
      </header>
      <div class="panel-body">
          <form role="form" action="" method="post">
              <div class="form-group">
                  <label for="exampleInputEmail1">Sub category Name</label>
                  <input type="text" placeholder="Enter Sub category Name" name="text" value="<?php echo $sub_category['text']; ?>" class="form-control">
                  <span style="color:red"><?php echo form_error("text"); ?></span>
              </div>
              <div class="checkbox">
                  <label>
                    <input type="checkbox" name="visible" <?php echo ($sub_category['visibilty']==0 ? 'checked' : '');?> > Visible
                  </label>
              </div>
              <button class="btn btn-info" type="submit">Submit</button>
          </form>
      </div>
  </section>
</div>
