<?php  $error = validation_errors();
$display = "display:none;";
if(!empty($error)){ $display = "";} 

?>
<div class="col-lg-6 add_file_element" style="<?php echo $display; ?>">
	  <section class="panel">
		  <header class="panel-heading">
			  Add Epub 
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 
				  <div class="form-group">
                    <label for="exampleInputEmail1">Subject</label>
                    <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">						
                    </select>
					   <span class="error bold"><?php echo form_error('subject_id');?></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Topic</label>
                    <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						 
                    </select>
					<span class="error bold"><?php echo form_error('topic_id');?></span>
                </div>
				     <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea rows="4" cols="50" class="form-control input-sm" name="description" class="form-control">						 
                    </textarea><span class="error bold"><?php echo form_error('description');?></span>
                </div>
                                 
				  <div class="form-group">
					  <label >Epub Title</label>
					  <input type="test" placeholder="Enter title" name = "title" id="title" class="form-control">
					   <span class="error bold"><?php echo form_error('title');?></span>
				  </div>

		        <div class="form-group">
					  <label for="exampleInputFile">Upload Epub</label>
					  <input type="file" accept=".epub" name = "epub_file" id="exampleInputFile">
					  <span class="error bold"><?php echo form_error('epub_file');?></span>
				  </div>  
				  <div class="form-group">
					  <label for="exampleInputFile">Epub Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php echo form_error('thumbnail');?></span>

				  </div> 
				   <div class="form-group">
                    <label for="exampleInputEmail1">Page Count</label>
                     <input type="test" placeholder="Enter page count" name = "page_count" id="page_count" class="form-control">
						 <span class="error bold"><?php echo form_error('page_count');?></span>                  
                </div>
				
				  <button class="btn btn-info"  type="submit" >Upload</button>
				   <button class="btn btn-danger btn-sm" onclick="$('.add_file_element').hide('slow');" type="button" >Cancel</button>
			  </form>

		  </div>
	  </section>
  </div>

	<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		<button onclick="$('.add_file_element').show('slow');" class="btn-success btn-xs btn pull-right"><i class="fa fa-plus"></i> Add</button>
		<?php // echo strtoupper($page); ?> Epub(s) LIST
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="all-user-grid">
  		<thead>
    		<tr>
          <th>#</th>
      	  <th>Title </th>
      	  <th>Thumbnail</th>
          <th>Subject </th>
          <th>Topic</th> 
		  <th>Page Count</th> 
		  <th>Action </th> 	 
    		</tr>
  		</thead>
      <thead>
          <tr>
              <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
              <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
			  <th><input type="text" data-column="5"  class="search-input-text form-control"></th>			            
		 	  <th></th> 
          </tr>
      </thead>
		</table>
		</div>
		</div>
	</section>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                
				 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" > 
			   
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                        });
                         $(".subject_element_select").html(html); 
                      }
                    });

                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val(); 
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>"; 
                          });
                           $(".topic_element_select").html(html); 
                        }
                      });
                    });
					
					jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                            "pageLength": 15,
                            "lengthMenu": [[15, 25, 50], [15, 25, 50]],
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"library/library/ajax_epub_file_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
                        $('.search-input-select').on( 'change', function () {   // for select box
                            var i =$(this).attr('data-column');
                            var v =$(this).val();
                            dataTable.columns(i).search(v).draw();
                        } );											
                   } );
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );