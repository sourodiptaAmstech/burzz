<?php

$page = (!isset($page)) ? "" : $page;

$dashboard = '';
$web_user = '';
$all_user = '';
$unverified_user = '';
$subscribed_user = '';
$android = '';
$ios = '';
$instructor = '';
$location = '';
$user_query = '';
$setting = '';
$backend_user = '';
$create_backend_user = '';
$backend_user_list = '';
$permission_group = '';
$category = '';
$category_list = '';
$course = '';
$fanwall = '';
$all_fanwall = '';
$fan_wall_banner = '';
$fan_wall_abuse_post = '';
$go_live = '';
$tags = '';
$tag_add = '';
$tag_group = '';
$email_template = '';
$email = '';
$sms = '';
$bulk_email = '';
$notification = '';
if ($page == 'dashboard') {$dashboard = 'active';} elseif ($page == 'all') {
	$all_user = 'active';
	$web_user = 'active';} elseif ($page == 'android') {
	$android = 'active';
	$web_user = 'active';} elseif ($page == 'ios') {
	$ios = 'active';
	$web_user = 'active';} elseif ($page == 'instructor') {
	$instructor = 'active';
	$web_user = 'active';} elseif ($page == 'location') {
	$location = 'active';
	$web_user = 'active';} elseif ($page == 'user_query') {$user_query = 'active';} elseif ($page == 'create_backend_user') {
	$create_backend_user = 'active';
	$setting = 'active';
	$backend_user = 'active';} elseif ($page == 'backend_user_list') {
	$backend_user_list = 'active';
	$setting = 'active';
	$backend_user = 'active';} elseif ($page == 'permission_group') {
	$permission_group = 'active';
	$setting = 'active';} elseif ($page == 'category_list') {
	$category_list = 'active';
	$category = 'active';} elseif ($page == 'course') {
	$course = 'active';
	$category = 'active';} elseif ($page == 'all_fanwall') {
	$all_fanwall = 'active';
	$fanwall = 'active';} elseif ($page == 'fan_wall_banner') {$fan_wall_banner = 'active';} elseif ($page == 'go_live') {$go_live = 'active';} elseif ($page == 'fan_wall_abuse_post') {
	$fan_wall_abuse_post = 'active';
	$fanwall = 'active';} elseif ($page == 'tag_add') {
	$tag_add = 'active';
	$tags = 'active';} elseif ($page == 'tag_group') {
	$tag_group = 'active';
	$tags = 'active';} elseif ($page == 'send_email') {
	$email = 'active';
	$email_template = 'active';} elseif ($page == 'send_sms') {
	$sms = 'active';
	$email_template = 'active';} elseif ($page == 'bulk_email') {
	$bulk_email = 'active';
	$email_template = 'active';} elseif ($page == 'push_notification') {$notification = 'active';} elseif ($page == 'Category') {$category = 'active';}

$sidebar_url = array('web_user/all_user_list');
$sidebar_url[] = 'admin/index';
$sidebar_url[] = 'web_user/all_user_list';
$sidebar_url[] = 'admin/create_backend_user';
$sidebar_url[] = 'admin/backend_user_list';
$sidebar_url[] = 'category/index';
$sidebar_url[] = 'course/index';
$sidebar_url[] = 'fan_wall/all_post';
$sidebar_url[] = 'add_tag/index';
$sidebar_url[] = 'fan_wall/fan_wall_abuse_post';
$sidebar_url[] = 'fan_wall/fan_wall_banner';
$sidebar_url[] = 'live_manager/live';
$sidebar_url[] = 'mailer/send_email';
$sidebar_url[] = 'bulk_message/send_bulk_message';
$sidebar_url[] = 'bulk_email/send_bulk_email';
$sidebar_url[] = 'user_query/index';
$sidebar_url[] = 'Version/versioning';

if (is_array($sidebar_url)) {
	$sidebar_url = implode("','", $sidebar_url);
	$sidebar_url = "'" . $sidebar_url . "'";
}
$session_userdata = $this->session->userdata();

$user_permsn = $this->db->query("SELECT pg.permission_fk_id FROM backend_user_role_permissions as burps left join permission_group as pg on pg.id = burps.permission_group_id where burps.user_id = '" . $session_userdata['active_backend_user_id'] . "' ")->row_array();

$user_permsn = ($user_permsn['permission_fk_id']) ? $user_permsn['permission_fk_id'] : '0';

$query = $this->db->query("SELECT * from backend_user_permission where id IN ($user_permsn)")->result_array();
$result_side_bar = $query;

//$query = $this->db->query(("SELECT bup.*, (SELECT permission_id FROM `backend_user_role_permission` WHERE user_id = '".$session_userdata['active_backend_user_id']."' AND permission_id = bup.id ) as user_permission FROM `backend_user_permission` as bup WHERE bup.permission_perm in ($sidebar_url) "));
//$result_side_bar = $query->result_array();

$temp = array();
foreach ($result_side_bar as $value) {

	$temp[$value['permission_perm']] = $value['id'];
	/*if($value['id'] == $value['user_permission']) {
		                    echo "<pre>";print_r($value);
	*/

}
//echo "<pre>";print_r($temp);die;

?>
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">


<!-- ################################ DASHBOARD MENU  #################################################-->

            <?php //if (array_key_exists("admin/index", $temp) && $temp['admin/index'] != '') {?>
            <!-- <li>
                <a class="<?php // echo $dashboard; ?>" href="<?php // echo AUTH_PANEL_URL . 'admin/index'; ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li> -->
            <?php // }?>

<!-- ################################ MOBILE USER MENU  #################################################-->

            <li class="sub-menu dcjq-parent-li">
                      <a href="javascript:;" class="<?php echo $web_user; ?>">
                          <i class="fa fa-mobile"></i>
                          <span>Mobile app user</span>
                      <span class="dcjq-icon"></span></a>

                <!-- <ul class="sub" style="display: block;">

                    <li class="sub-menu">
                        <a href="javascript:;" class="<?php echo $web_user; ?>">

                            <span>View User</span>
                            <span class="dcjq-icon"></span>
                        </a> -->
                        <ul class="sub" style="display: block;">
                          <?php
                            if ((array_key_exists("web_user/all_user_list", $temp) && $temp['web_user/all_user_list'] != '')) {
                            	?>
                             <li class="<?php echo $all_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/all_user_list'; ?>"><i class="fa fa-users"></i>All</a></li>
                             <li class="<?php echo $unverified_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/verified_user_list'; ?>"><i class="fa fa-users"></i>Verified</a></li>
                             <li class="<?php echo $unverified_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/unverified_user_list'; ?>"><i class="fa fa-users"></i>Unverified</a></li>
                             <li class="<?php echo $subscribed_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/fake_user_list'; ?>"><i class="fa fa-users"></i>Fake</a></li>
                          <li class="<?php echo $subscribed_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'web_user/subscribed_user_list'; ?>"><i class="fa fa-users"></i>Subscriptions</a></li>
                           <?php }?>

                        </ul>
                    <!-- </li>
                </ul> -->
            </li>


<!-- ################################ FEEDS MENU  ####################################################-->



<!-- ################################ CATEGORIES MENU  ################################################-->

            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="javascript:;"> <i class="fa fa-list-ol "></i> <span>Masters </span> </a>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/questions'; ?>">Questions</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/industry'; ?>">Industry</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/education_level'; ?>">Education</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/releationship'; ?>">Relationship</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/kids'; ?>">Kids</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/looking_for'; ?>">Looking For Biz</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/looking_for_d'; ?>">Looking For BD</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/looking_for_f'; ?>">Looking For BFF</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/new_to_area'; ?>">New To Area</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/pets'; ?>">Pets</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/drinking'; ?>">Drinking</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/smoking'; ?>">Smoking</a></li>
                </ul>
                   <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/religion'; ?>">Religion</a></li>
                </ul>
                   <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/exercise'; ?>">Exercise</a></li>
                </ul>
                   <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/zodiac'; ?>">Zodiac</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'masters/politics'; ?>">Politics</a></li>
                </ul>

            </li>

             <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'packages/package_list'; ?>"> <i class="fa fa-list-ol "></i> <span>Packages </span> </a>
            </li>

            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'follow/follow_list'; ?>"> <i class="fa fa-list-ol "></i> <span>Follow</span> </a>
            </li>

            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'follow/unfollow_list'; ?>"> <i class="fa fa-list-ol "></i> <span>Un Follow </span> </a>
            </li>
            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'follow/matches_list'; ?>"> <i class="fa fa-list-ol "></i> <span>Matches </span> </a>
            </li>
            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'follow/blocked_list'; ?>"> <i class="fa fa-list-ol "></i> <span>Blocked </span> </a>
            </li>
             <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="<?php echo AUTH_PANEL_URL . 'Static_pages/helpdesk'; ?>"> <i class="fa fa-list-ol "></i> <span>Help Desk </span> </a>
            </li>

<!--            <li><a href="<?php echo AUTH_PANEL_URL . 'reports/transactions'; ?>">All transactions</a></li>-->

            <li class="sub-menu">
                <a class="<?php echo $category; ?>" href="javascript:;"> <i class="fa fa-list-ol "></i> <span>Static Pages </span> </a>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'Static_pages/about'; ?>">Terms & Conditions</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'Static_pages/p_policy'; ?>">Privacy Policy</a></li>
                </ul>
                <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'Static_pages/u_guide'; ?>">User Guide</a></li>
                </ul>
                 <ul class="sub">
                    <li class="<?php echo $category_list; ?>" ><a href="<?php echo AUTH_PANEL_URL . 'Static_pages/faqs'; ?>">FAQs</a></li>
                </ul>


            </li>


 <!-- ########################## Course managemnet ################################################-->



      <!--   <li class="sub-menu dcjq-parent-li">
            <a href="#" class="dcjq-parent"><i class="fa fa-file"></i> Reports <span class="dcjq-icon"></span></a>
            <ul class="sub" style="display: none;">
                <li class=" sub-menu dcjq-parent-li">
                    <a href="#" class="dcjq-parent"> Account Section <span class="dcjq-icon"></span></a>
                    <ul class="sub" style="display: none;">
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions'; ?>">All transactions</a></li>
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=pending'; ?>">Pending</a></li>
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=complete'; ?>">Complete</a></li>
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=cancel'; ?>">Cancel</a></li>
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=refund_req'; ?>">Refund Req.</a></li>
                      <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_transactions?status=refunded'; ?>">Refunded</a></li>
                    </ul>
                </li>

                  <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/instructor_transactions/index'; ?>">Instructor transaction</a></li>
                  <li><a href="<?php echo AUTH_PANEL_URL . 'course_product/course_wise_transactions'; ?>">Course transaction</a></li>

                  <li><a href="<?php echo AUTH_PANEL_URL . "course_product/course/all_courses_reviews_list"; ?>">Course Reviews Reports</a></li>
                  <li><a title="Instructor reviews Report" href="<?php echo AUTH_PANEL_URL . "course_product/course/all_instructor_reviews_list"; ?>">Inst. Reviews Reports</a></li>
                  <li><a title="Instructor reviews Report" href="<?php echo AUTH_PANEL_URL . "course_product/course/all_test_given_report"; ?>">Test Taken Reports</a></li>
                  <li><a title="Points earn Report" href="<?php echo AUTH_PANEL_URL . "points_earns/report"; ?>">User points report</a></li>
            </ul>
        </li> -->









            <?php
if ((array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '')
	|| (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '')
) {
	?>

            <li class="sub-menu dcjq-parent-li">
                      <a href="javascript:;" class="dcjq-parent <?php echo $setting; ?>">
                          <i class="fa fa-cogs"></i>
                          <span>Settings</span>
                      <span class="dcjq-icon"></span></a>

                <ul class="sub" style="display: block;">
                    <?php
if ((array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '')
		|| (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '')
	) {
		?>
                    <li class="sub-menu dcjq-parent-li">
                        <a href="javascript:;" class="dcjq-parent <?php echo $backend_user; ?>">
                            <i class="fa fa-users"></i>
                            <span>Backend Users</span>
                            <span class="dcjq-icon"></span>
                        </a>
                        <ul class="sub" style="display: block;">
                            <?php if (array_key_exists("admin/create_backend_user", $temp) && $temp['admin/create_backend_user'] != '') {?>
                            <li class="<?php echo $create_backend_user; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/create_backend_user'; ?>">Add New</a></li>
                             <?php }if (array_key_exists("admin/backend_user_list", $temp) && $temp['admin/backend_user_list'] != '') {?>
                            <li class="<?php echo $backend_user_list; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/backend_user_list'; ?>">View List</a></li>
                            <?php }?>
                        </ul>
                    </li>
                    <?php }?>

                      <li class="<?php echo $permission_group; ?>"><a href="<?php echo AUTH_PANEL_URL . 'admin/make_permission_group'; ?>">Role management</a></li>
                      <li class="<?php echo $permission_group; ?>"><a href="<?php echo AUTH_PANEL_URL . 'refer_code/refer_code'; ?>">Points</a></li>
                      <li class="<?php echo $permission_group; ?>"><a href="<?php echo AUTH_PANEL_URL . 'meta/App_setting/'; ?>">App Setting</a></li>
                </ul>
            </li>

            <?php }?>





 </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
