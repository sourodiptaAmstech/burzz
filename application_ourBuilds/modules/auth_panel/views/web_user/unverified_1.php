<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    <?php echo strtoupper($page); ?> USER(s) LIST
    </header>
    <div class="clearfix"></div>
    <div class="panel-body">
            <div class="adv-table ">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                    <table  class="table display table-bordered table-striped" id="all-user-grid">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Mobile </th>
                                <th>Status </th>
                                <th>On </th>
                                <th title="EMIAL SUBSCRIPTION"  >@</th>
                                <!--<th title="POINTS"  >&nbsp;&nbsp;&nbsp;<i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;</th>-->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="2"  class="search-input-text form-control"></th>
                                <th><input type="text" data-column="3"  class="search-input-text form-control"></th>
                               <th></th>
                                <th></th>
                                <th></th>
                              <!--  <th></th>-->
                                <th></th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
    </div>
  </section>
</div>

<?php
$query_string = "";
$adminurl = AUTH_PANEL_URL;
if($page == 'android'){
  $device_type = 1;
}elseif($page == 'ios'){
  $device_type = 2;
}elseif($page == 'all'){
  $device_type = '0';
}elseif($page == 'unverified'){
  $device_type = '0';
}elseif($page == 'instructor') {
  $device_type = '0';
  $query_string = 'instructor';
}elseif($page == 'expert') {
  $device_type = '0';
  $query_string = 'expert';
}

$custum_js = <<<EOD
              <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
               <script type="text/javascript" language="javascript" >
                   var all_user_all = "$adminurl"+"web_user/ajax_unverified_user_list/"+"$device_type"+"?user=$query_string"; 
                   jQuery(document).ready(function() {
                       var table = 'all-user-grid';
                       var dataTable_user = jQuery("#"+table).DataTable( {
                            "processing": true,
                            "language": {
                                "processing": "<i class='fa fa-spin  fa-spinner fa-4x' style='z-index:999'><i>" //add a loading image,simply putting <img src="loader.gif" /> tag.
                            },
                            // "autoWidth": false,
                            // "responsive": true,
                            // "scrollX": true,
                            "pageLength": 100,
                            "lengthMenu": [[15, 25, 50 ,100], [15, 25, 50,100]],
                            "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url : all_user_all, // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable_user.columns(i).search(v).draw();
                       } );
                   } );

              
     </script>

EOD;

  echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>
