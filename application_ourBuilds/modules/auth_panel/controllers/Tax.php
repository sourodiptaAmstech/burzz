<?php
use Aws\S3\S3Client;
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');		
		$this->load->helper('image_resizer');

	}
	
	public function index() {
		$data['page_title'] = "Tax list";
		$view_data['page']  = 'tax_list';
		if($this->input->post()) { 
			$this->form_validation->set_rules('tax_value', 'Tax Value', 'required');		

			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert_data['tax_value']=$this->input->post('tax_value');
				$this->db->insert('tax',$insert_data);
            	$data['page_toast'] = 'Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		
		
		
		$data['page_data'] = $this->load->view('tax/tax_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_tax_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'tax_value'
		);

		$query = "SELECT count(id) as total FROM tax ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * FROM tax where 1=1";
		if (!empty($requestData['columns'][0]['search']['value'])) {  
			$sql.=" AND tax_value LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); 
		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->tax_value;
			$nestedData[] = ($r->status == 1 )?'<button class="btn btn-danger change_status" id="'.$r->id .'" status="2">Unpublished</button>':'<button class="btn btn-success change_status" id="'.$r->id .'" status="1">Published</button>';
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "tax/edit_tax/" . $r->id . "'>Edit</a>";
			
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData), 
			"recordsFiltered" => intval($totalFiltered), 
			"data" => $data  
		);

		echo json_encode($json_data);
	}
		public function update_status($id){
		$this->db->where('id',$id);
		$update=$this->db->update('tax',array('status'=>$this->input->post('status')));
		if($update){
			echo json_encode(array('status'=>true,'message'=>'Successfully Updated'));
			die;
		}
		echo json_encode(array('status'=>true,'message'=>'Something Went Wrong'));
		die;
	}

	public function edit_tax($id) {
		if($this->input->post()) { 
			$this->form_validation->set_rules('tax_value', 'Tax Value', 'required');
			if ($this->form_validation->run() == FALSE) {
               
            } else {
            	$insert_data['tax_value']=$this->input->post('tax_value'); 
            	
            	$this->db->where('id',$id);
            	$this->db->update('tax',$insert_data);
            	 
				$data['page_toast'] = 'Information Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
            }


		}
		$view_data['page']  = 'edit_tax';
		$data['page_title'] = "edit_tax";
		$view_data['tax'] = $this->db->get_where('tax',array('id'=>$id))->row_array();
		$view_data['tax_id'] = $id;
		$data['page_data'] = $this->load->view('tax/edit_tax', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}






	

}