<?php
use Aws\S3\S3Client;

defined('BASEPATH') OR exit('No direct script access allowed');

class Masters extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Category_list_model");
		$this->load->helper('image_resizer');
		$this->load->library('grocery_CRUD');

	}

	public function _example_output($output = null) {
		$this->load->view(AUTH_TEMPLATE . 'grocery_crud_template', (array) $output);
	}

	public function questions() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Questions');
		//$crud->set_relation('course_id','course_master','title');
		//$crud->set_relation('coupon_id','course_coupon_master','coupon_tilte ');
		//$crud->display_as('course_id','Course')->display_as('coupon_id','Coupon');
		$crud->set_table('master_relation');
		$crud->field_type('master_table', 'hidden');
		$crud->field_type('icon', 'hidden');
		$crud->columns('title', 'question', 'type');

		$interest_type = $this->db->get('interest_type')->result_array();
		$interest = [];
		$i = 1;
		foreach ($interest_type as $it) {
			$interest[$i] = $it['title'];
			$i++;
		}

		$crud->field_type('type', 'multiselect', $interest);
		$crud->unset_add();
		$crud->unset_delete();
		$crud->required_fields(array('title', 'question', 'type[]'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function industry() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Industry');
		//$crud->set_table('coupon_course_relation_master');
		//$crud->set_relation('course_id','course_master','title');
		//$crud->set_relation('coupon_id','course_coupon_master','coupon_tilte ');
		//$crud->display_as('course_id','Course')->display_as('coupon_id','Coupon');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function education_level() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Education');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function releationship() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Releationship');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function politics() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Politics');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function kids() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Kids');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function looking_for() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Looking For');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function looking_for_d() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Looking For');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function looking_for_f() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Looking For');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function new_to_area() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('New To Area');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function pets() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Pets');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function drinking() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Drinking');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function smoking() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Smoking');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function religion() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Religion');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function exercise() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Exercise');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}
	public function zodiac() {
		$crud = new grocery_CRUD();
		$crud->unset_export();
		$crud->unset_print();
		$crud->set_subject('Zodiac');
		$crud->required_fields(array('title', 'status'));
		$crud->field_type('status', 'dropdown', array('1' => 'Disable', '2' => 'Enable'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function amazon_s3_upload($name, $aws_path, $name_option = "") {
		$_FILES['file'] = $name;
		require_once FCPATH . 'aws/aws-autoloader.php';

		$s3Client = new S3Client([
			'version' => 'latest',
			'region' => 'ap-southeast-2',
			'credentials' => [
				'key' => AMS_S3_KEY,
				'secret' => AMS_SECRET,
			],
		]);
		$result = $s3Client->putObject(array(
			'Bucket' => AMS_BUCKET_NAME,
			'Key' => $aws_path . '/' . (($name_option == "") ? rand(0, 7896756) . $_FILES["file"]["name"] : $name_option),
			'SourceFile' => $_FILES["file"]["tmp_name"],
			'ContentType' => 'image',
			'ACL' => 'public-read',
			'StorageClass' => 'REDUCED_REDUNDANCY',
			'Metadata' => array('param1' => 'value 1', 'param2' => 'value 2'),
		));
		$data = $result->toArray();
		return $data['ObjectURL'];

	}

	public function ajax_category_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'title',
		);

		$query = "SELECT count(id) as total
								FROM master_category
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT mclo.* FROM master_category as  mclo left
								join master_category as mc
								on mc.id = mclo.parent_id
								where 1=1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			//name
			$sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {
			// preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = ($r->status == 1) ? '<button class="btn btn-danger change_status" id="' . $r->id . '" status="2">Unpublished</button>' : '<button class="btn btn-success change_status" id="' . $r->id . '" status="1">Published</button>';
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "category/edit_sub_category/" . $r->id . "'>Edit</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
		);

		echo json_encode($json_data); // send data as json format
	}
	public function update_status($id) {
		$this->db->where('id', $id);
		$update = $this->db->update('master_category', array('status' => $this->input->post('status')));
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Updated'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function edit_sub_category($id) {
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'Title', 'required');
			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['parent_id'] = $this->input->post('parent_id');
				$insert_data['description'] = $this->input->post('description');
				$insert_data['title'] = $this->input->post('title');
				$insert_data['pack_size'] = $this->input->post('pack_size');
				if ($_FILES && $_FILES['image']['name']) {
					$insert_data['image'] = $this->amazon_s3_upload($_FILES['image'], "products");
				}
				if ($_FILES && $_FILES['banner_image']['name']) {
					$insert_data['banner_image'] = $this->amazon_s3_upload($_FILES['banner_image'], "products");
				}
				$this->db->where('id', $id);
				$this->db->update('master_category', $insert_data);

				$data['page_toast'] = 'Information Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$view_data['page'] = '';
		$data['page_title'] = "category list";
		$view_data['sub_category_id'] = $id;
		$view_data['sub_category_list'] = $this->Category_list_model->get_sub_category_list();
		$view_data['category'] = $this->Category_list_model->get_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/edit_category', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function add_category() {
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'Title', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['parent_id'] = $this->input->post('parent_id');
				$insert_data['description'] = $this->input->post('description');
				$insert_data['title'] = $this->input->post('title');
				$insert_data['pack_size'] = $this->input->post('pack_size');
				$insert_data['image'] = $this->amazon_s3_upload($_FILES['image'], "products");
				$insert_data['banner_image'] = $this->amazon_s3_upload($_FILES['banner_image'], "products");
				$this->db->insert('master_category', $insert_data);
				$data['page_toast'] = 'Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$view_data['page'] = '';
		$data['page_title'] = "category list";
		$view_data['sub_category'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('category/add_category', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_subcategory_list_level_two($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'sub_text',
			2 => 'text',
		);

		$query = "SELECT count(id) as total
								FROM master_category_level_two
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT mclt.id as id , mclt.text as sub_text , mclo.text as text
								FROM master_category_level_two as  mclt
								join master_category_level_one as mclo
								on mclo.id = mclt.parent_id
								where mclt.parent_id = $id
								And 1=1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			//name
			$sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND text LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND sub_text LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {
			// preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->text;
			$nestedData[] = $r->sub_text;
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "category/edit_sub_sub_category/" . $r->id . "'>Edit</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
		);

		echo json_encode($json_data); // send data as json format
	}

	public function edit_sub_sub_category($id) {
		if ($this->input->post()) {
			$this->form_validation->set_rules('text', 'Sub Category', 'required');
			if ($this->form_validation->run() == FALSE) {

			} else {
				$visible = $this->input->post('visible');
				if (isset($visible)) {$visible = 0;} else { $visible = 1;}
				$update = array('text' => $this->input->post('text'), 'visibilty' => $visible);
				$this->db->where('id', $id);
				$this->db->update('master_category_level_two', $update);
				//$this->

				$data['page_toast'] = 'Information Updated  successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}
		}
		$view_data['page'] = '';
		$data['page_title'] = "Sub category list";
		$view_data['sub_category'] = $this->Category_list_model->get_sub_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/edit_sub_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function add_sub_sub_category($id) {
		if ($this->input->post()) {

			$this->form_validation->set_rules('text', 'Sub Category', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data = array('parent_id' => $id, 'text' => $this->input->post('text'));

				$this->db->insert('master_category_level_two', $insert_data);

				$data['page_toast'] = 'Information Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$data['page_title'] = "Add Sub category list";
		$view_data['page'] = '';
		$view_data['sub_category'] = $this->Category_list_model->get_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/add_sub_sub_category_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

}