<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		$this->load->library('form_validation');

		modules::run('auth_panel/auth_panel_ini/auth_ini');

	}

	public function index() {

		$data['page_title'] = "API";
		$data['page_data'] = $this->load->view('api/apis', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function live_watcher() {

		$data['page_title'] = "live_watcher";
		$data['page_data'] = $this->load->view('api/live_watcher', array(), TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

}