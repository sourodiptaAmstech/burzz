<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Version extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation');
	}

	public function versioning(){
		if($_POST){
			$array = array(
					"android"=>$this->input->post('android'),
					"android_code"=>$this->input->post('android_code'),
					"ios"=>$this->input->post('ios'),
				);
			$this->db->where('id',1);
			$this->db->update('version_control',$array);
			backend_log_genration($this->session->userdata('active_backend_user_id'),'Updated version.','Version');
		}

		$data['page_data'] = $this->load->view('version/version_view', array() , TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);	
	}
}	