<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Web_user extends MX_Controller {
	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->model('web_user_model');
		$this->load->library('form_validation', 'uploads');
	}

	public function all_user_list() {
		if ($this->input->get('user') != '') {

			($this->input->get('user') == 'android') ? $view_data['page'] = 'android' : $view_data['page'] = 'ios';
			($this->input->get('user') == 'instructor') ? $view_data['page'] = 'instructor' : '';
			($this->input->get('user') == 'expert') ? $view_data['page'] = 'expert' : '';
		} else {
			$view_data['page'] = 'all';
		}
		$data['page_data'] = $this->load->view('web_user/all_user1', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_user_list($device_type) {
		$output_csv = $output_pdf = false;
		if ($device_type != 0) {$where = "AND device_type = $device_type";} else { $where = '';}
		//storing  request (ie, get/post) global array to a variable

		$requestData = $_REQUEST;
		if (isset($_POST['input_json'])) {
			$requestData = json_decode($_POST['input_json'], true);
			if (ISSET($_POST['download_pdf'])) {
				$output_pdf = true;
			} else {
				$output_csv = true;
			}
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'mobile',
			4 => 'status',
			5 => 'creation_time',
		);

		/*------------------------------------------*/
		if ($_GET['period'] != "") {
			$period = $_GET['period'];

			$today = date('d-m-Y');
			$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
			$currentMonth = date('m');
			$currentYear = date('Y');
			$current_millisecond = strtotime(date('01-m-Y 00:00:00')) * 1000;
			if ($period == "today") {
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(u.creation_time,1,10)), '%d-%m-%Y')='$today' ";
			} elseif ($period == "yesterday") {
				$yesterday = date('d-m-Y', strtotime($today . ' - 1 days'));
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(u.creation_time,1,10)), '%d-%m-%Y')='$yesterday' ";
			} elseif ($period == "7days") {
				//$yesterday = date('d-m-Y', strtotime($today. ' - 7 days'));
				$yesterday = strtotime("-1 week") . "000";
				$where .= " AND u.creation_time >= '$yesterday'  ";
			} elseif ($period == "current_month") {
				$current_month = date('m-Y');
				$where .= " AND DATE_FORMAT(FROM_UNIXTIME(SUBSTR(u.creation_time,1,10)), '%m-%Y') = '$current_month'  ";
			} elseif ($period == "all") {
				$where .= "";
			}
		}

		$query = "SELECT count(id) as total FROM users as u where status != 2 $where
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;
		$sql = "SELECT id,name,email,mobile,DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y') as creation_time,status  , email_subscription , reward_points FROM  users as u   where status != 2 $where";
		//getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {

			//name
			$sql .= " AND id LIKE '%" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND name LIKE '%" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (isset($requestData['columns'][7]['search']['value']) && $requestData['columns'][7]['search']['value'] != "") {
			//salary
			$sql .= " AND status = " . $requestData['columns'][7]['search']['value'];
		}
		if (!empty($requestData['columns'][8]['search']['value'])) {
			//salary
			$date = explode(',', $requestData['columns'][8]['search']['value']);
			$start = strtotime($date[0]) * 1000;
			$end = (strtotime($date[1]) * 1000) + 86400000;
			$sql .= "  AND  creation_time >= '$start' and creation_time <= '$end'";
		}
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();
                //print_r($query); die;
		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		// if download for csv do not add csv

//		if ($output_csv == false && $output_pdf == false) {
//			$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length
//		} else {
//			$sql .= " ORDER BY u.creation_time desc ";
//		}
                $sql .= " ORDER BY u.creation_time desc ";
		$result = $this->db->query($sql)->result();
		$data = array();
		if ($output_csv == true || $output_pdf == true) {
			// for csv loop
			$head = array('s.no', 'name', 'email', 'mobile', 'status', 'creation time');
			$start = 0;
			foreach ($result as $r) {
				$nestedData = array();
				$nestedData[] = ++$start; //$r->id;
				$nestedData[] = $r->name;
				$nestedData[] = $r->email;
				$nestedData[] = $r->mobile;
				$nestedData[] = ($r->status == 0) ? 'Active' : 'Disabled';
				$nestedData[] = $r->creation_time;
				$data[] = $nestedData;
			}
			if ($output_csv == true) {
				$this->all_user_to_csv_download($data, $filename = "export.csv", $delimiter = ";", $head);
				die;
			}
			if ($output_pdf == true) {
				$this->all_user_to_pdf_download($data);
				die;
			}

		}

		foreach ($result as $r) {
			//preparing an array
			$nestedData = array();
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = '<span title="' . $r->name . '">' . substr($r->name, 0, 15) . (strlen($r->name) > 15 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->email . '">' . substr($r->email, 0, 20) . (strlen($r->email) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = $r->mobile;
			$nestedData[] = ($r->status == 0) ? '<span class="btn btn-xs bold btn-success">Active</span>' : '<span class="btn btn-sm btn-danger">Disabled</span>';
			$nestedData[] = $r->creation_time;
			$nestedData[] = ($r->email_subscription == 0) ? '&nbsp;&nbsp;&nbsp;<i class="fa fa-check">&nbsp;&nbsp;&nbsp;</i>' : '&nbsp;&nbsp;&nbsp;<i class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>';
			//$nestedData[] = $r->reward_points;
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='" . AUTH_PANEL_URL . "web_user/user_profile/" . $r->id . "'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
			"posted_data" => $_POST,
		);

		echo json_encode($json_data); // send data as json format
	}

	/*
		* this function is to generated csv w.r.t. to user inout in ajax datatable
		* why i use it ?
		* it is realted with authetication machenism
		* Warning please check ajax data table view and other related function before changing it
		* happy coding :)
	*/
	public function get_request_for_csv_download($device_type) {
		$this->ajax_all_user_list($device_type);
	}

	public function unverified_user_list() {
		if ($this->input->get('user') != '') {

			($this->input->get('user') == 'android') ? $view_data['page'] = 'android' : $view_data['page'] = 'ios';
			($this->input->get('user') == 'instructor') ? $view_data['page'] = 'instructor' : '';
			($this->input->get('user') == 'expert') ? $view_data['page'] = 'expert' : '';
		} else {
			$view_data['page'] = 'unverified';
		}
		$data['page_data'] = $this->load->view('web_user/unverified_users1', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
        
        public function verified_user_list() {
		if ($this->input->get('user') != '') {

			($this->input->get('user') == 'android') ? $view_data['page'] = 'android' : $view_data['page'] = 'ios';
			($this->input->get('user') == 'instructor') ? $view_data['page'] = 'instructor' : '';
			($this->input->get('user') == 'expert') ? $view_data['page'] = 'expert' : '';
		} else {
			$view_data['page'] = 'verified';
		}
		$data['page_data'] = $this->load->view('web_user/verified_users', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
        
        public function ajax_verified_user_list($device_type) {
		$output_csv = $output_pdf = false;
		if ($device_type != 0) {$where = "AND device_type = $device_type AND is_verified = 1";} else { $where = 'AND is_verified = 1';}
		//storing  request (ie, get/post) global array to a variable

		$requestData = $_REQUEST;
		if (isset($_POST['input_json'])) {
			$requestData = json_decode($_POST['input_json'], true);
			if (ISSET($_POST['download_pdf'])) {
				$output_pdf = true;
			} else {
				$output_csv = true;
			}
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'mobile',
			4 => 'status',
			5 => 'creation_time',
		);

		/*------------------------------------------*/

		$query = "SELECT count(id) as total
									FROM users as u where status != 2
									$where
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;
		$sql = "SELECT id,name,email,mobile,DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y') as creation_time,DATE_FORMAT(FROM_UNIXTIME(modified_time/1000), '%d-%m-%Y') as modified_time, email_subscription , reward_points,is_verified as status FROM  users as u   where status != 2 $where";
		//getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {

			//name
			$sql .= " AND id LIKE '%" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND name LIKE '%" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		// if download for csv do not add csv
//		if ($output_csv == false && $output_pdf == false) {
//			$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length
//		} else {
//			$sql .= " ORDER BY u.modified_time desc ";
//		}
                $sql .= " ORDER BY u.creation_time desc ";
		$result = $this->db->query($sql)->result();
		$data = array();
		if ($output_csv == true || $output_pdf == true) {
			// for csv loop
			$head = array('s.no', 'name', 'email', 'mobile', 'status', 'creation time');
			$start = 0;
			foreach ($result as $r) {
				$nestedData = array();
				$nestedData[] = ++$start; //$r->id;
				$nestedData[] = $r->name;
				$nestedData[] = $r->email;
				$nestedData[] = $r->mobile;
				$nestedData[] = ($r->status == 0) ? 'Unverified' : 'Verified';
				$nestedData[] = $r->creation_time;
				$data[] = $nestedData;
			}
			if ($output_csv == true) {
				$this->all_user_to_csv_download($data, $filename = "export.csv", $delimiter = ";", $head);
				die;
			}
			if ($output_pdf == true) {
				$this->all_user_to_pdf_download($data);
				die;
			}

		}

		foreach ($result as $r) {
			//preparing an array
			$nestedData = array();
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = '<span title="' . $r->name . '">' . substr($r->name, 0, 15) . (strlen($r->name) > 15 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->email . '">' . substr($r->email, 0, 20) . (strlen($r->email) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = $r->mobile;
			$nestedData[] = ($r->status == 0) ? '<span class="btn btn-xs bold btn-danger">Unverified</span>' : '<span class="btn btn-sm btn-success">Verified</span>';
			$nestedData[] = $r->creation_time;
			$nestedData[] = ($r->email_subscription == 0) ? '&nbsp;&nbsp;&nbsp;<i class="fa fa-check">&nbsp;&nbsp;&nbsp;</i>' : '&nbsp;&nbsp;&nbsp;<i class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>';
			//$nestedData[] = $r->reward_points;
                        
			$nestedData[] = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "web_user/user_profile/" . $r->id . "'>View</a><a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "web_user/delete_unverified_user/delete/" . $r->id . "' onclick='return confirm("."\"Are you sure to delete this user\"".");'>Delete User</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
			"posted_data" => $_POST,
		);

		echo json_encode($json_data); // send data as json format
	}

	public function ajax_unverified_user_list($device_type) {
		$output_csv = $output_pdf = false;
		if ($device_type != 0) {$where = "AND device_type = $device_type AND is_verified = 0";} else { $where = 'AND is_verified = 0';}
		//storing  request (ie, get/post) global array to a variable

		$requestData = $_REQUEST;
		if (isset($_POST['input_json'])) {
			$requestData = json_decode($_POST['input_json'], true);
			if (ISSET($_POST['download_pdf'])) {
				$output_pdf = true;
			} else {
				$output_csv = true;
			}
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'mobile',
			4 => 'status',
			5 => 'creation_time',
		);

		/*------------------------------------------*/

		$query = "SELECT count(id) as total
									FROM users as u where status != 2
									$where
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;
		$sql = "SELECT id,name,email,mobile,DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y') as creation_time, email_subscription , reward_points,is_verified as status FROM  users as u   where status != 2 $where";
		//getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {

			//name
			$sql .= " AND id LIKE '%" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND name LIKE '%" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		// if download for csv do not add csv

//		if ($output_csv == false && $output_pdf == false) {
//			$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length
//		} else {
//			$sql .= " ORDER BY u.creation_time desc ";
//		}
                $sql .= " ORDER BY u.modified_time desc ";
		$result = $this->db->query($sql)->result();
		$data = array();
		if ($output_csv == true || $output_pdf == true) {
			// for csv loop
			$head = array('s.no', 'name', 'email', 'mobile', 'status', 'creation time');
			$start = 0;
			foreach ($result as $r) {
				$nestedData = array();
				$nestedData[] = ++$start; //$r->id;
				$nestedData[] = $r->name;
				$nestedData[] = $r->email;
				$nestedData[] = $r->mobile;
				$nestedData[] = ($r->status == 0) ? 'Unverified' : 'Verified';
				$nestedData[] = $r->creation_time;
				$data[] = $nestedData;
			}
			if ($output_csv == true) {
				$this->all_user_to_csv_download($data, $filename = "export.csv", $delimiter = ";", $head);
				die;
			}
			if ($output_pdf == true) {
				$this->all_user_to_pdf_download($data);
				die;
			}

		}

		foreach ($result as $r) {
			//preparing an array
			$nestedData = array();
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = '<span title="' . $r->name . '">' . substr($r->name, 0, 15) . (strlen($r->name) > 15 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->email . '">' . substr($r->email, 0, 20) . (strlen($r->email) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = $r->mobile;
			$nestedData[] = ($r->status == 0) ? '<span class="btn btn-xs bold btn-danger">Unverified</span>' : '<span class="btn btn-sm btn-success">Verified</span>';
			$nestedData[] = $r->creation_time;
			$nestedData[] = ($r->email_subscription == 0) ? '&nbsp;&nbsp;&nbsp;<i class="fa fa-check">&nbsp;&nbsp;&nbsp;</i>' : '&nbsp;&nbsp;&nbsp;<i class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>';
			//$nestedData[] = $r->reward_points;
			$nestedData[] = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "web_user/user_profile/" . $r->id . "'>View</a><a class='btn-xs bold btn btn-info' onclick='return confirm("."\"Are you sure to verify this user\"".");' href='" . AUTH_PANEL_URL . "web_user/verify_user_from_list/" . $r->id . "'>Verify</a><a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "web_user/delete_unverified_user/delete/" . $r->id . "' onclick='return confirm("."\"Are you sure to delete this user\"".");'>Delete User</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
			"posted_data" => $_POST,
		);

		echo json_encode($json_data); // send data as json format
	}

	public function subscribed_user_list() {
		if ($this->input->get('user') != '') {

			($this->input->get('user') == 'android') ? $view_data['page'] = 'android' : $view_data['page'] = 'ios';
			($this->input->get('user') == 'instructor') ? $view_data['page'] = 'instructor' : '';
			($this->input->get('user') == 'expert') ? $view_data['page'] = 'expert' : '';
		} else {
			$view_data['page'] = 'subscribed';
		}
		$data['page_data'] = $this->load->view('web_user/subscribed_users', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_subscribed_user_list($device_type) {
		$output_csv = $output_pdf = false;
		if ($device_type != 0) {$where = "AND device_type = $device_type";} else { $where = '';}
		//storing  request (ie, get/post) global array to a variable

		$requestData = $_REQUEST;
		if (isset($_POST['input_json'])) {
			$requestData = json_decode($_POST['input_json'], true);
			if (ISSET($_POST['download_pdf'])) {
				$output_pdf = true;
			} else {
				$output_csv = true;
			}
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'mobile',
			4 => 'pay_via',
			5 => 'package_price',
			6 => 'package_name',
			7 => 'creation_time'
		);

		/*------------------------------------------*/

		$query = "SELECT count(u.id) as total
									FROM users as u join transaction_record as t on t.user_id = u.id join packages as p on p.id = t.package_id where u.status != 2 and t.transaction_status = 1 and t.validity > ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000) and t.is_deleted != 1
									$where
									";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;
		$sql = "SELECT u.id as id, u.name as name, u.email as email,"
                        . " u.mobile as mobile, t.id as subscription_id, t.pay_via as pay_via,"
                        . " t.package_price as package_price, t.currency as currency,"
                        . " t.package_id as package_id, DATE_FORMAT(FROM_UNIXTIME(t.creation_time/1000),"
                        . " '%d-%m-%Y') as creation_time, p.name as package_name,p.validity as package_validity "
                        . "FROM  users as u join transaction_record as t on t.user_id = u.id join packages "
                        . "as p on p.id = t.package_id where u.status != 2 and t.transaction_status = 1 "
                        . " and t.is_deleted != 1 "
                        . "$where";
		//getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {

			//name
			$sql .= " AND u.id LIKE '%" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND u.name LIKE '%" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND u.email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND u.mobile LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		
		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		// if download for csv do not add csv

		if ($output_csv == false && $output_pdf == false) {
			$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length
		} else {
			$sql .= " ORDER BY u.creation_time desc ";
		}
		$result = $this->db->query($sql)->result();
               
		$data = array();
		if ($output_csv == true || $output_pdf == true) {
			// for csv loop
			$head = array('s.no', 'name', 'email', 'mobile', 'payment method', 'package price', 'package name', 'package_validity','purchase time');
			$start = 0;
			foreach ($result as $r) {
				$nestedData = array();
				$nestedData[] = ++$start; //$r->id;
				$nestedData[] = $r->name;
				$nestedData[] = $r->email;
				$nestedData[] = $r->mobile;
				if($r->pay_via == 'PAY_PAL'){
					$payment = 'Paypal';
				}else if($r->pay_via == 'OTHERS'){
					$payment = 'Paystack';
				}else{
					$payment = 'Apple Pay';
				}
				$nestedData[] = $payment;
				$nestedData[] = $r->currency ." ". $r->package_price;
                                $nestedData[] = $r->package_name;
				$nestedData[] = $r->package_validity;
				$nestedData[] = $r->creation_time;
				
				$data[] = $nestedData;
			}
			if ($output_csv == true) {
				$this->all_user_to_csv_download($data, $filename = "export.csv", $delimiter = ";", $head);
				die;
			}
			if ($output_pdf == true) {
				$this->all_user_to_pdf_download($data);
				die;
			}

		}

		foreach ($result as $r) {
			if($r->pay_via == 'PAY_PAL'){
				$payment = 'Paypal';
			}else if($r->pay_via == 'OTHERS'){
				$payment = 'Paystack';
			}else{
				$payment = 'Apple Pay';
			}
			//preparing an array
			$nestedData = array();
			$nestedData[] = ++$requestData['start'];
			$nestedData[] = '<span title="' . $r->name . '">' . substr($r->name, 0, 15) . (strlen($r->name) > 15 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->email . '">' . substr($r->email, 0, 20) . (strlen($r->email) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = $r->mobile;
			$nestedData[] = '<span title="' . $payment . '">' . substr($payment, 0, 20) . (strlen($payment) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->currency ." ". $r->package_price . '">' . substr($r->currency ." ". $r->package_price, 0, 20) . (strlen($r->currency ." ". $r->package_price) > 20 ? ' ...' : '') . '</span>';
			$nestedData[] = '<span title="' . $r->package_name . '">' . substr($r->package_name, 0, 20) . (strlen($r->package_name) > 20 ? ' ...' : '') . '</span>';
                        $nestedData[] = $r->package_validity;
			$nestedData[] = $r->creation_time;
			$nestedData[] = "<a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "web_user/remove_subscription/remove/" . $r->subscription_id . "' onclick='return confirm(\"Are you sure to remove this subscription from panel?\");'>Remove</a>";
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
			"posted_data" => $_POST,
		);

		echo json_encode($json_data); // send data as json format
	}

	public function remove_subscription($status, $id) {

		$this->db->where('id', $id);
		$status = $this->db->update('transaction_record',array('is_deleted' => 1));
		if ($status == 'TRUE') {
			redirect('auth_panel/web_user/subscribed_user_list');
		}
	}

	public function all_user_to_csv_download($array, $filename = "export.csv", $delimiter = ";", $header) {
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '";');
		$f = fopen('php://output', 'w');
		fputcsv($f, $header);
		foreach ($array as $line) {
			fputcsv($f, $line, $delimiter);
		}
	}

	public function all_user_to_pdf_download($data) {
		$this->load->library('fpdf_users');
		$this->fpdf_users->pdf_out($data);
	}

	public function user_profile($id) {
		if ($this->input->post()) {
			//$this->form_validation->set_rules('user_id', '', 'required');
			$this->form_validation->set_rules('u_name', 'Beneficiary name', 'required');
			$this->form_validation->set_rules('b_account', 'Beneficiary account', 'required');
			$this->form_validation->set_rules('b_ifsc', 'Bank IFSC', 'required');
			$this->form_validation->set_rules('b_name', 'Bank name', 'required');
			$this->form_validation->set_rules('b_address', 'Bank address', 'required');
			$this->form_validation->set_rules('r_sharing', 'Resource sharing', 'required');
			if ($this->form_validation->run() == FALSE) {
			}
			$update = array('u_name' => $this->input->post('u_name'), 'b_account' => $this->input->post('b_account'), 'b_ifsc' => $this->input->post('b_ifsc'), 'b_name' => $this->input->post('b_name'), 'b_address' => $this->input->post('b_address'), 'r_sharing' => $this->input->post('r_sharing'));
			$this->db->where('user_id', $id);
			$this->db->update('course_instructor_information', $update);
			$data['page_toast'] = 'File added successfully.';
			$data['page_toast_type'] = 'success';
			$data['page_toast_title'] = 'Action performed.';
		}

		$view_data['user_data'] = $this->web_user_model->get_user_profile($id);
		$view_data['user_profile_detail'] = $this->web_user_model->get_user_profile_details($id);
		//$view_data['user_register_record'] = $this->web_user_model->get_user_registered_details($id);

		$view_data['page'] = 'all_user';
		$data['page_data'] = $this->load->view('web_user/user_profile', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function delete_unverified_user($status, $id) {

		$status = $this->web_user_model->update_user_status($status, $id);
		if ($status == 'TRUE') {
			redirect('auth_panel/web_user/unverified_user_list');
		}
	}

	public function delete_user($status, $id) { 
        //echo $status.' -  '.$id; die('amit');
		$status = $this->web_user_model->update_user_status($status, $id);
		if ($status == 'TRUE') {
			redirect('auth_panel/web_user/all_user_list');
		}
	}

	public function disable_user($status, $id) {
		$status = $this->web_user_model->update_user_status($status, $id);

		if ($status) {
			redirect('auth_panel/web_user/user_profile/' . $id);
		}
	}

	public function enable_user($status, $id) {
		$status = $this->web_user_model->update_user_status($status, $id);

		if ($status) {
			redirect('auth_panel/web_user/user_profile/' . $id);
		}
	}

	public function verify_user($id) {
		$status = $this->web_user_model->update_verify_status($id);

		if ($status) {
			redirect('auth_panel/web_user/user_profile/' . $id);
		}
	}

	public function verify_user_from_list($id) {
		$status = $this->web_user_model->update_verify_status($id);

		if ($status) {
			redirect('auth_panel/web_user/unverified_user_list');
		}
	}

	public function is_moderator($id) {
		if ($id > 0) {
			$this->db->where('id', $id);
			$this->db->update('users', array('is_moderate' => $_POST['modrator']));
			//creating backend log
			$user_details = $this->web_user_model->get_user_basic_detail($id);
			if ($_POST['modrator'] == 1) {
				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Assigned Modrator role to ' . $user_details['name'] . '.',
					'WEB USER');
			} else {
				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Removed Modrator role from ' . $user_details['name'] . '.',
					'WEB USER');
			}

		}

		redirect('auth_panel/web_user/user_profile/' . $id);
	}

	public function is_course_validator($id) {
		if ($id > 0) {
			$this->db->where('id', $id);
			$this->db->update('users', array('is_course_validator' => $_POST['is_course_validator']));
			//creating backend log
			$user_details = $this->web_user_model->get_user_basic_detail($id);
			if ($_POST['is_course_validator'] == 1) {
				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Assigned course validator role to ' . $user_details['name'] . '.',
					'WEB USER');
			} else {
				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Removed course validator role from ' . $user_details['name'] . '.',
					'WEB USER');
			}

		}

		redirect('auth_panel/web_user/user_profile/' . $id);
	}

	public function is_instructor($id) {
		if ($id > 0) {
			//print_r($_POST); die;
			$this->db->where('id', $id);
			$update = $this->db->update('users', array('is_instructor' => $_POST['instructor']));

			$is_instructor = $_POST['instructor'];

			$this->db->select("name,email,password");
			$this->db->where('id', $id);
			$user_details = $this->db->get('users')->row_array();

			$new_password = rand(1000, 999999);
			$add_instructor = array('username' => $user_details['name'], 'email' => $user_details['email'], 'password' => md5($new_password), 'creation_time' => time(), '	instructor_id' => $id, 'status' => 0);

			if ($_POST['instructor'] == 1) {
				$status = $this->web_user_model->add_instructor_id($id);

				$this->db->where('instructor_id', $id);
				$instructor_details_backend_user = $this->db->get('backend_user')->row_array();
				if (empty($instructor_details_backend_user)) {
					$add_instructor_to_backend_user = $this->db->insert("backend_user", $add_instructor);
					$input = array(
						"SENDER" => "donotreply@emedicoz.com",
						"RECIPIENT" => $user_details['email'],
						"SUBJECT" => "Welcome to Emedicoz Instructor",
						"HTMLBODY" => " Dear " . $user_details['name'] . ", Your password for instructor login is = $new_password with email = " . $user_details['email'],
						"TEXTBODY" => "Password For Instructor Login",
					);
					$this->aws_emailer->send_aws_email($input);
				} elseif ($instructor_details_backend_user['status'] == 1) {
					$this->db->where('instructor_id', $id);
					$update = $this->db->update('backend_user', array('status' => 0));
				}
			} elseif ($_POST['instructor'] == 0) {
				$this->db->where('instructor_id', $id);
				$update = $this->db->update('backend_user', array('status' => 1));

				$this->db->where('id', $id);
				$update = $this->db->update('users', array('is_instructor' => $_POST['instructor']));
			}
		}

		redirect('auth_panel/web_user/user_profile/' . $id);
	}

	public function is_expert($id) {
		if ($id > 0) {
			$this->db->where('id', $id);
			$this->db->set('is_expert', $_POST['expert'], FALSE);
			$this->db->set('expert_tag_id', 'CONCAT(expert_tag_id,\',\',\'' . $_POST['expert_tag_id'] . '\')', FALSE);
			$update = $this->db->update('users');
			//echo $this->db->last_query(); die;
			$user_details = $this->web_user_model->get_user_basic_detail($id);

			if ($_POST['expert'] == 1) {
				$this->refresh_tag_ids($id, false);
				$status = $this->web_user_model->add_instructor_id($id);

				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Assigned Expert role to ' . $user_details['name'] . '.',
					'WEB USER');
			} else {
				$this->refresh_tag_ids($id, true);
				backend_log_genration($this->session->userdata('active_backend_user_id'),
					'Removed Expert role from ' . $user_details['name'] . '.',
					'WEB USER');
			}
		}

		redirect('auth_panel/web_user/user_profile/' . $id);
	}

	private function refresh_tag_ids($user_id, $reset = false) {
		if ($reset == true) {
			$this->db->where('id', $user_id);
			$this->db->set('expert_tag_id', '', FALSE);
			$update = $this->db->update('users');
		} else {

			$this->db->where('id', $user_id);
			$t_id = $this->db->select('expert_tag_id')->get('users')->row()->expert_tag_id;
			$t_id = explode(',', $t_id);
			$t_id = array_unique($t_id);
			$t_id = implode(',', $t_id);

			$this->db->where('id', $user_id);
			$this->db->set('expert_tag_id', $t_id);
			$update = $this->db->update('users');
		}
	}

	public function delete_expert_ids($user_id, $tag_id) {
		$this->db->where('id', $user_id);
		$t_id = $this->db->select('expert_tag_id')->get('users')->row()->expert_tag_id;
		$t_id = explode(',', $t_id);
		$t_id = array_unique($t_id);
		if (count($t_id)) {
			$t_id = array_diff($t_id, array($tag_id));

			$t_id = implode(',', $t_id);
			$this->db->where('id', $user_id);
			$this->db->set('expert_tag_id', $t_id);
			$update = $this->db->update('users');

		}

		redirect('auth_panel/web_user/user_profile/' . $user_id);
	}

	public function all_user_location() {

		$view_data['page'] = 'location';
		$data['page_data'] = $this->load->view('web_user/all_user_location', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_all_user_location() {

		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'email',
			3 => 'country',
			4 => 'state',
			5 => 'city',
		);

		$query = "SELECT count(id) as total
									FROM user_registerd_location
									where 1 = 1"
		;
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT url.*,u.name,u.email,u.creation_time FROM  user_registerd_location as url
				JOIN users as u ON url.user_id = u.id
				where 1 = 1
				";
		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			//name
			$sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND email LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND country LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {
			//salary
			$sql .= " AND state LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {
			//salary
			$sql .= " AND city LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}

		//echo $requestData['columns'][5]['search']['value'];
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length

		$result = $this->db->query($sql)->result();

		$data = array();

		foreach ($result as $r) {
			// preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $r->email;
			$nestedData[] = $r->country;
			$nestedData[] = $r->state;
			$nestedData[] = $r->city;
			$nestedData[] = $r->latitude;
			$nestedData[] = $r->longitude;
			$nestedData[] = $r->ip_address;
			$nestedData[] = date("d-m-Y", $r->creation_time / 1000);
			$nestedData[] = "<a class='btn-xs bold btn btn-info' href='#'>View</a>";
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
		);

		echo json_encode($json_data); // send data as json format
	}
	/*	 * *******************End User************************* */

	public function ajax_instructor_ratings_list($id) {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'name',
			2 => 'rating',
			3 => 'text',
			4 => 'creation_time',

		);

		$query = "SELECT count(id) as total FROM course_instructor_rating";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT cir.*,u.name,DATE_FORMAT(FROM_UNIXTIME(cir.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time
								FROM course_instructor_rating as  cir
								 join users as u
								on cir.user_id = u.id where instructor_id = $id";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			//name
			$sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//salary
			$sql .= " AND name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			//salary
			$sql .= " AND rating LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][3]['search']['value'])) {
			//salary
			$sql .= " AND text LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {
			//salary
			$sql .= " AND creation_time LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length

		$result = $this->db->query($sql)->result();

		$data = array();
		foreach ($result as $r) {
			// preparing an array
			$nestedData = array();

			$star = "";
			if ($r->rating > 0) {
				for ($i = 0; $i < $r->rating; $i++) {
					$star .= '<i class =  "fa fa-star text-danger"></i>';
				}
				if ($i < 5) {$j = 5 - $i;for ($i = 0; $i < $j; $i++) {$star .= '<i class =  "fa fa-star-o text-danger"></i>';}}
			}

			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $star;
			$nestedData[] = substr($r->text, 0, 30);
			$nestedData[] = $r->creation_time;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "web_user/edit_instructor_rating/" . $r->id . "'>Edit</a>";
			$action .= "<a class='btn-sm btn btn-danger btn-xs bold' href='" . AUTH_PANEL_URL . "web_user/delete_review/" . $r->id . "?instructor_id=" . $r->instructor_id . "'>delete</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
		);

		echo json_encode($json_data); // send data as json format
	}

	public function edit_instructor_rating($id) {
		$view_data['page'] = 'edit_instructor_rating';
		$data['page_title'] = "Edit Instructor Rating";
		if ($this->input->post('update_instructor_review')) {
			/* handle submission */
			$this->update_instructor_review();
		}
		$view_data['instructor_rating_detail'] = $this->web_user_model->get_instructor_rating_details_by_id($id);
		$data['page_data'] = $this->load->view('web_user/edit_instructor_details', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	private function update_instructor_review() {
		if ($this->input->post()) {
			$this->form_validation->set_rules('rating', 'Rating', 'required');
			$this->form_validation->set_rules('text', 'Review', 'required');
			$review_id = $this->input->post('id');
			$user_id = $this->input->post('instructor_id');
			if ($this->form_validation->run() == FALSE) {
				$error = validation_errors();
			} else {
				$update = array(
					'rating' => $this->input->post('rating'),
					'text' => $this->input->post('text'),

				);
				$this->db->where('id', $review_id);
				$this->db->update('course_instructor_rating', $update);
				page_alert_box('success', 'Action performed', 'Review updated successfully');
			}
		}

	}

	public function delete_review($id) {
		$user_id = $_GET['instructor_id'];
		$status = $this->web_user_model->delete_review($id);
		page_alert_box('success', 'Action performed', 'Review deleted successfully');
		if ($status) {
			redirect('auth_panel/web_user/user_profile/' . $user_id);
		}
		$update = array(
			'device_tokken' => "",
		);
		$this->db->where('id', $user_id);
		$this->db->update('users', $update);
		page_alert_box('success', 'Action performed', 'User Session destroyed successfully');
		redirect('auth_panel/web_user/user_profile/' . $user_id);
	}}

	
	

	

}
