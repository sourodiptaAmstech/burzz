
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Static_pages extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->helper('image_resizer');

	}

	public function about() {
		$data['page_title'] = "static list";
		$view_data['page'] = 'static';
		if ($this->input->post()) {
			$this->form_validation->set_rules('description', 'description', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['description'] = $this->input->post('description');
				$this->db->where('id', 1);
				$this->db->update('static_pages', $insert_data);
				$data['page_toast'] = 'Updated successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}

		$data['page_data'] = $this->load->view('static/about', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function u_guide() {
		$data['page_title'] = "static list";
		$view_data['page'] = 'static';
		if ($this->input->post()) {
			$this->form_validation->set_rules('description', 'description', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['description'] = $this->input->post('description');
				$this->db->where('id', 3);
				$this->db->update('static_pages', $insert_data);
				$data['page_toast'] = 'Updated successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}

		$data['page_data'] = $this->load->view('static/u_guide', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function p_policy() {
		$data['page_title'] = "static list";
		$view_data['page'] = 'static';
		if ($this->input->post()) {
			$this->form_validation->set_rules('description', 'description', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['description'] = $this->input->post('description');
				$this->db->where('id', 2);
				$this->db->update('static_pages', $insert_data);
				$data['page_toast'] = 'Updated successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}

		$data['page_data'] = $this->load->view('static/p_policy', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_faqs_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => 'question',
			2 => 'description',
		);

		$query = "SELECT count(id) as total
								FROM faqs
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * FROM faqs where 1=1";
		if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql .= " AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][01]['search']['value'])) {
			$sql .= " AND question LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {
			$sql .= " AND description LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->question;
			$nestedData[] = $r->description;

			$action = "<button class='btn btn-danger change_status' id='" . $r->id . "'>Delete</button>
			<a class='btn-xs bold btn btn-success' id='edit_" . $r->id . "' href='" . AUTH_PANEL_URL . "static_pages/edit_faqs/" . $r->id . "'>Edit</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}
	public function delete_faqs($id) {
		$this->db->where('id', $id);
		$update = $this->db->delete('faqs');
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Updated'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function faqs() {
		if ($this->input->post()) {
			$this->form_validation->set_rules('question', 'question', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['question'] = $this->input->post('question');
				$insert_data['description'] = $this->input->post('description');

				$this->db->insert('faqs', $insert_data);
				// $this->db->where('id', $id);
				// $this->db->update('brands', $insert_data);

				$data['page_toast'] = 'Information Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$view_data['page'] = 'add_faqs';
		$data['page_title'] = "add_faqs";
		$data['page_data'] = $this->load->view('static/faqs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function edit_faqs($id) {
		if ($this->input->post()) {
			$this->form_validation->set_rules('question', 'question', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
			$this->form_validation->set_rules('id', 'id', 'required');
			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['question'] = $this->input->post('question');
				$insert_data['description'] = $this->input->post('description');
				$insert_data['id'] = $this->input->post('id');
				$this->db->where('id', $insert_data['id']);
				$this->db->update('faqs', $insert_data);

				$data['page_toast'] = 'Information Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
				redirect(AUTH_PANEL_URL . "static_pages/faqs");
			}

		}
		$view_data['page'] = 'edit_brand';
		$data['page_title'] = "edit_brand";
		$view_data['id'] = $id;
		$data['page_data'] = $this->load->view('static/edit_faqs', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function helpdesk() {
		$data['page_title'] = "helpdesk list";
		$view_data['page'] = 'helpdesk';

		$data['page_data'] = $this->load->view('static/helpdesk', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_feedback_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',

		);

		$query = "SELECT count(id) as total FROM feedback";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT feedback.*,name,mobile,email FROM feedback join users on users.id=feedback.user_id where 1=1";

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
		$data = array();
		$i = 1;

		foreach ($result as $r) {
			$nestedData = array();
			$nestedData[] = $i++;
			$nestedData[] = $r->name;
			$nestedData[] = $r->mobile;
                        $nestedData[] = $r->email;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->message;

			$action = "<button class='btn btn-danger change_status' id='" . $r->id . "'>Delete</button>
			<a class='btn-xs bold btn btn-success' id='edit_" . $r->id . "' href='" . AUTH_PANEL_URL . "static_pages/edit_faqs/" . $r->id . "'>Edit</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}

}