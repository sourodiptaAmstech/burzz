<?php
require 'vendor/yabacon/paystack-php/src/autoload.php';

$reference = isset($_GET['reference']) ? $_GET['reference'] : '';
if (!$reference) {
	die('No reference supplied');
}
$paystack = new Yabacon\Paystack('sk_test_fbc719564aa816cdb60d1469aaee8e7d6c6ff685');
//$paystack = new Yabacon\Paystack('sk_test_2010160a569fb1d684091d77521eb7000aaac8c6');//live
try
{
	$tranx = $paystack->transaction->verify([
		'reference' => $reference, // unique to transactions
	]);
} catch (\Yabacon\Paystack\Exception\ApiException $e) {
	die($e->getMessage());
}
print_r($tranx);
if ('success' === $tranx->data->status) {
	echo json_encode($tranx);
//	echo json_encode(array("status" => true, 'data'=$tranx->data));
} else {
	echo json_encode(array("status" => false));
}

?>
