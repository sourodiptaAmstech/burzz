<?php

require 'vendor/yabacon/paystack-php/src/autoload.php';
$paystack = new Yabacon\Paystack('sk_test_fbc719564aa816cdb60d1469aaee8e7d6c6ff685');

echo $reference = "ref".rand(1000,9999);


$url        =  "https://api.paystack.co/transaction/verify";
$header     = ['Authorization: Bearer sk_test_fbc719564aa816cdb60d1469aaee8e7d6c6ff685'];

call_api($url,$header= getallheaders(),$input);

function call_api($url,$header,$document){
    $document   = json_encode($input,true);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $document);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $msg = curl_error($ch);
    }
    curl_close($ch);
    $data = $result;
    if (isset($msg)) {
        $return = array("StatusCode" => "fail", "StatusDesc" => $msg, "Output" => "");
        echo json_encode($return);
        die;
    }
}


try {
    $tranx = $paystack->transaction->verify([
        'reference' => $reference, // unique to transactions
    ]);
} catch (\Yabacon\Paystack\Exception\ApiException $e) {
    die($e->getMessage());
}

if ('success' === $tranx->data->status) {
    echo json_encode($tranx);
//	echo json_encode(array("status" => true, 'data'=$tranx->data));
} else {
    echo json_encode(array("status" => false));
}


?>
