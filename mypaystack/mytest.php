
<?php

$input = $_POST;
$url = $input['url'];
unset($input['url']);
call_api($url,$header= getallheaders(),$input);

function call_api($url,$header,$document){
    $document   = json_encode($input,true);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $document);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $msg = curl_error($ch);
    }
    curl_close($ch);
    $data = $result;
    if (isset($msg)) {
        $return = array("StatusCode" => "fail", "StatusDesc" => $msg, "Output" => "");
        echo json_encode($return);
        die;
    }
}

?>