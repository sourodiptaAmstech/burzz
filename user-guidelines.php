<?php
// Here is the data we will be sending to the service

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "http://3.215.188.72/index.php/data_model/Static_pages/get_policy");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    "id=3");

// In real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// Receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_close($ch);

$info = json_decode($server_output)->data;
foreach ($info as $value) {
    $data = $value;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title></title>
    <style>
        h3{
            font-size: 29px;
            font-weight: 600;
        }
        h2{
            font-size: 34px;
            font-weight: 700;
            text-align: center;
        }
    </style>
</head>
<body>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php echo $data->description; ?>
                </div>
            </div>
        </div>
    </section>
</body>
</html>