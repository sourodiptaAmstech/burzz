<?php
/***
 *      _____            _  _
 *     |  __ \          | |(_)
 *     | |__) | ___   __| | _  ___
 *     |  _  / / _ \ / _` || |/ __|
 *     | | \ \|  __/| (_| || |\__ \
 *     |_|  \_\\___| \__,_||_||___/
 *
 *    A libarary having normal functions of php and redis
 *    While written I used redis 4.* version
 *    fx -: means function in this file
 *    follow link https://www.hugeserver.com/kb/install-redis-centos/ for installation
 *    http://webd.is/ for many things 
 */

 Class Redis_magic{
   public $redis = 0 ;

   public function __construct(){
     $this->redis = new Redis();
     $this->redis->connect('127.0.0.1', 6379);
   }
   /* fx to set a value  for respcetive key */
   public function SET($key,$value){
     return $this->redis->SET($key,$value);
   }

   /* fx to get a value from key */
   public function GET($key){
     return $this->redis->GET($key);
   }

   /*set key with expiry in seconds */
   public function SETEX($key,$seconds,$value){
     return $this->redis->SETEX($key,$seconds,$value);
   }
   /* check variables living time*/
   public function TTL($key){
     return $this->redis->TTL($key);
   }
   /* Redis sets
   * https://redis.io/commands/hexists
   */
   /* set an array */
   public function HMSET($table,$u_id,$data){
     $this->redis->HMSET( $table.':'.$u_id, $data);
     $this->redis->SADD($table.':Ids', $u_id);
   }

  public function HGETALL($table,$u_id){
    return $this->redis->HGETALL($table.':'.$u_id);
  }

  /* Publish a message */
  public function PUBLISH($channel_name , $message){
    return $this->redis->PUBLISH($channel_name , $message);
  }

 }
