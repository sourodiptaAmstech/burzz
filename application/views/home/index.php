<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Comming Soon</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	
	<!-- Font -->
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
	
	
	<link href="<?php echo WEB_ASSETS;?>common-css/ionicons.css" rel="stylesheet">
	
	
	<link rel="stylesheet" href="<?php echo WEB_ASSETS;?>common-css/jquery.classycountdown.css" />
		
	<link href="<?php echo WEB_ASSETS;?>03-comming-soon/css/styles.css" rel="stylesheet">
	
	<link href="<?php echo WEB_ASSETS;?>03-comming-soon/css/responsive.css" rel="stylesheet">
	
</head>
<body>
	
	<div class="main-area center-text" style="background-image:url(<?php echo WEB_ASSETS?>images/countdown-3-1600x900.jpg);">
		
		<div class="display-table">
			<div class="display-table-cell">
				
				<h1 class="title font-white"><b>Comming Soon</b></h1>
				<p class="desc font-white">Our website is currently undergoing scheduled construction.
					We Should be back shortly. Thank you for your patience.</p>
				
				<!-- <a class="notify-btn" href="#"><b>NOTIFY US</b></a> -->
				
				<!-- <ul class="social-btn font-white">
					<li><a href="#">facebook</a></li>
					<li><a href="#">twitter</a></li>
					<li><a href="#">google</a></li>
					<li><a href="#">instagram</a></li>
				</ul> --><!-- social-btn -->
				
			</div><!-- display-table -->
		</div><!-- display-table-cell -->
	</div><!-- main-area -->
	
</body>
</html>