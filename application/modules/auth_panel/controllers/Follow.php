<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Follow extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->helper('image_resizer');

	}

	public function follow_list() {
		$data['page_title'] = "follow list";
		$view_data['page'] = 'follow_list';
		$data['page_data'] = $this->load->view('follow/follow_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function unfollow_list() {
		$data['page_title'] = "unfollow list";
		$view_data['page'] = 'unfollow_list';
		$data['page_data'] = $this->load->view('follow/unfollow_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function matches_list() {
		$data['page_title'] = "matches list";
		$view_data['page'] = 'matches_list';
		$data['page_data'] = $this->load->view('follow/matches_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function blocked_list() {
		$data['page_title'] = "blocked list";
		$view_data['page'] = 'blocked_list';
		$data['page_data'] = $this->load->view('follow/blocked_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_follow_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'follow_id',
                        1 => 'user_id',
                        2 => 'creation_date',
		);

		$query = "SELECT count(id) as total FROM followed ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT followed.*,DATE_FORMAT(FROM_UNIXTIME(creation_date/1000), '%d-%m-%Y') as creation_date,f1.name as followed_by, f2.name as followed_to FROM followed join users as f1 on followed.follow_id=f1.id join users as f2 on f2.id=followed.user_id where 1=1";

		if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql .= " AND f1.name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
                if (!empty($requestData['columns'][1]['search']['value'])) {
			$sql .= " AND f2.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
//                if (!empty($requestData['columns'][2]['search']['value'])) {
//			$sql .= " AND creation_date LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
//		}
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
               // print_r($result);die;
		$data = array();
		$i = 0;
		foreach ($result as $r) {
			$nestedData = array();

			$nestedData[] = ++$i;
			$nestedData[] = $r->followed_by;
			$nestedData[] = $r->followed_to;
			$nestedData[] = $r->creation_date;
			$action = "<button class='btn-xs btn-danger change_status' id='" . $r->id . "'>Remove</button>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}

	public function ajax_unfollow_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
                        1 => 'follow_id',
                        2 => 'user_id',
		);

		$query = "SELECT count(id) as total FROM never_followed ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT never_followed.*,f1.name as not_followed_by, f2.name as not_followed_to FROM never_followed join users as f1 on never_followed.follow_id=f1.id join users as f2 on f2.id=never_followed.user_id where 1=1";

		if (!empty($requestData['columns'][1]['search']['value'])) {
			$sql .= " AND f1.name LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
                if (!empty($requestData['columns'][2]['search']['value'])) {
			$sql .= " AND f1.name LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
		$data = array();
		$i = 0;
		foreach ($result as $r) {
			$nestedData = array();

			$nestedData[] = ++$i;
			$nestedData[] = $r->not_followed_by;
			$nestedData[] = $r->not_followed_to;
			$nestedData[] = date('Y/m/d h:m:sa ',  $r->creation_date);
			$action = "<button class='btn-xs btn-danger change_status' id='" . $r->id . "'>Remove</button>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}

	public function ajax_blocked_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
		);

		$query = "SELECT count(id) as total FROM blocked ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT blocked.*,f1.name as blocked_by, f2.name as blocked_to FROM blocked join users as f1 on blocked.user_id=f1.id join users as f2 on f2.id=blocked.blocked_id where 1=1";

		/*if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql .= " AND product_name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}*/

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
		$data = array();
		$i = 0;
		foreach ($result as $r) {
			$nestedData = array();

			$nestedData[] = ++$i;
			$nestedData[] = $r->blocked_by;
			$nestedData[] = $r->blocked_to;
			$nestedData[] = $r->creation_time;
			$action = "<button class='btn-xs btn-danger change_status' id='" . $r->id . "'>Remove</button>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}

	public function ajax_matches_list() {
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
		);

		$query = "SELECT count(id) as total FROM followed ";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT matches.*,f1.name as user_one, f2.name as user_two FROM matches join users as f1 on matches.user_id=f1.id join users as f2 on f2.id=matches.match_id where 1=1";
		/*if (!empty($requestData['columns'][0]['search']['value'])) {
			$sql .= " AND product_name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}*/

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query);
		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";

		$result = $this->db->query($sql)->result();
		$data = array();
		$i = 0;
		foreach ($result as $r) {
			$nestedData = array();

			$nestedData[] = ++$i;
			$nestedData[] = $r->user_one;
			$nestedData[] = $r->user_two;
			$nestedData[] = date('Y/m/d h:m:sa ', $r->creation_time);
			//$action = "<a class='btn-xs bold btn btn-danger' href='" . AUTH_PANEL_URL . "follow/remove_follow/" . $r->id . "'>Remove</a>";

			//$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']),
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data,
		);

		echo json_encode($json_data);
	}

	public function remove_follow($id) {
		$this->db->where('id', $id);
		$update = $this->db->delete('followed');
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Removed'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function remove_unfollow($id) {
		$this->db->where('id', $id);
		$update = $this->db->delete('never_followed');
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Removed'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function remove_blocked($id) {
		$this->db->where('id', $id);
		$update = $this->db->delete('blocked');
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Removed'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function update_status($id) {
		$this->db->where('id', $id);
		$update = $this->db->update('stock', array('status' => $this->input->post('status')));
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Updated'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;

	}
}