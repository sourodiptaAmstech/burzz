<?php
use Aws\S3\S3Client;

defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
			 *  admin panel initialization
			 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		*/
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Category_list_model");
		$this->load->helper('image_resizer');
		$this->load->library('grocery_CRUD');

	}

	public function _example_output($output = null) {
		$this->load->view(AUTH_TEMPLATE . 'grocery_crud_template', (array) $output);
	}

	public function amazon_s3_upload($name, $aws_path, $name_option = "") {
		$_FILES['file'] = $name;
		require_once FCPATH . 'aws/aws-autoloader.php';

		$s3Client = new S3Client([
			'version' => 'latest',
			'region' => 'ap-southeast-2',
			'credentials' => [
				'key' => AMS_S3_KEY,
				'secret' => AMS_SECRET,
			],
		]);
		$result = $s3Client->putObject(array(
			'Bucket' => AMS_BUCKET_NAME,
			'Key' => $aws_path . '/' . (($name_option == "") ? rand(0, 7896756) . $_FILES["file"]["name"] : $name_option),
			'SourceFile' => $_FILES["file"]["tmp_name"],
			'ContentType' => 'image',
			'ACL' => 'public-read',
			'StorageClass' => 'REDUCED_REDUNDANCY',
			'Metadata' => array('param1' => 'value 1', 'param2' => 'value 2'),
		));
		$data = $result->toArray();
		return $data['ObjectURL'];

	}

	public function edit_sub_category($id) {
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'Title', 'required');
			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['parent_id'] = $this->input->post('parent_id');
				$insert_data['description'] = $this->input->post('description');
				$insert_data['title'] = $this->input->post('title');
				$insert_data['pack_size'] = $this->input->post('pack_size');
				if ($_FILES && $_FILES['image']['name']) {
					$insert_data['image'] = $this->amazon_s3_upload($_FILES['image'], "products");
				}
				if ($_FILES && $_FILES['banner_image']['name']) {
					$insert_data['banner_image'] = $this->amazon_s3_upload($_FILES['banner_image'], "products");
				}
				$this->db->where('id', $id);
				$this->db->update('master_category', $insert_data);

				$data['page_toast'] = 'Information Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$view_data['page'] = '';
		$data['page_title'] = "category list";
		$view_data['sub_category_id'] = $id;
		$view_data['sub_category_list'] = $this->Category_list_model->get_sub_category_list();
		$view_data['category'] = $this->Category_list_model->get_sub_category_list_by_id($id);
		$data['page_data'] = $this->load->view('category/edit_category', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function add_category() {
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'Title', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$insert_data['parent_id'] = $this->input->post('parent_id');
				$insert_data['description'] = $this->input->post('description');
				$insert_data['title'] = $this->input->post('title');
				$insert_data['pack_size'] = $this->input->post('pack_size');
				$insert_data['image'] = $this->amazon_s3_upload($_FILES['image'], "products");
				$insert_data['banner_image'] = $this->amazon_s3_upload($_FILES['banner_image'], "products");
				$this->db->insert('master_category', $insert_data);
				$data['page_toast'] = 'Inserted successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$view_data['page'] = '';
		$data['page_title'] = "category list";
		$view_data['sub_category'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('category/add_category', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function package_list() {

		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Title', 'required');
			$this->form_validation->set_rules('validity', 'Validity', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('price_2', 'price_2', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$this->db->insert('packages', $this->input->post());

				$data['page_toast'] = 'Package Added Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$data['page_title'] = "package_list";
		$view_data['page'] = '';

		$view_data['packages'] = array();
		$data['page_data'] = $this->load->view('packages/package_list', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function update_package($id) {

		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Title', 'required');
			$this->form_validation->set_rules('validity', 'Validity', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('price_2', 'price_2', 'required');

			if ($this->form_validation->run() == FALSE) {

			} else {
				$this->db->where('id', $this->input->post('id'));
				$this->db->update('packages', $this->input->post());

				$data['page_toast'] = 'Package Updated Successfully.';
				$data['page_toast_type'] = 'success';
				$data['page_toast_title'] = 'Action performed.';
			}

		}
		$data['page_title'] = "Edit Package";
		$view_data['page'] = '';

		$view_data['packages'] = $this->db->get_where('packages', array('id' => $id))->row_array();
		$data['page_data'] = $this->load->view('packages/edit_package', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);

	}

	public function update_status($id) {
		$this->db->where('id', $id);
		$update = $this->db->update('packages', array('status' => $this->input->post('status')));
		if ($update) {
			echo json_encode(array('status' => true, 'message' => 'Successfully Updated'));
			die;
		}
		echo json_encode(array('status' => true, 'message' => 'Something Went Wrong'));
		die;
	}

	public function ajax_packages_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'name',
			1 => 'status',
		);

		$query = "SELECT count(id) as total	FROM packages";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT * FROM packages 	where 1=1";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {
			//name
			$sql .= " AND name LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {
			//name
			$sql .= " AND status LIKE '" . $requestData['columns'][1]['search']['value'] . "' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {
			// preparing an array
			$nestedData = array();
			$nestedData[] = $r->id;
			$nestedData[] = $r->name;
			$nestedData[] = $r->price;
			$nestedData[] = $r->price_2;
			$nestedData[] = $r->text;
			$nestedData[] = $r->validity;
			$nestedData[] = ($r->status == 1) ? '<button class="btn-xs btn-danger change_status" id="' . $r->id . '" status="2">Unpublished</button>' : '<button class="btn-xs btn-success change_status" id="' . $r->id . '" status="1">Published</button>';
			$action = "<a class='btn-xs bold btn btn-success' href='" . AUTH_PANEL_URL . "packages/update_package/" . $r->id . "'>Edit</a>";

			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data, // total data array
		);

		echo json_encode($json_data); // send data as json format
	}

}