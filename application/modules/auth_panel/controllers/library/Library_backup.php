<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends MX_Controller {

	function __construct() {
		parent::__construct();
		/* !!!!!! Warning !!!!!!!11
		 *  admin panel initialization
		 *  do not over-right or remove auth_panel/auth_panel_ini/auth_ini
		 */
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Library_model");
		$this->load->library('upload');

	}
	
	
	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');
				
						$s3Client = new S3Client([  
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [        
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,  
						],
						]);
						$result = $s3Client->putObject(array(   
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).$_FILES["file"]["name"], 
							'SourceFile' => $_FILES["file"]["tmp_name"], 
							'ContentType' => 'image', 
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY', 
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )        
						));
				$data=$result->toArray();
				return $data['ObjectURL'];
	
	}
	
	public function index() { 
		if($this->input->post()) { 

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['pdf_file']['name']))
			{ 
				$this->form_validation->set_rules('pdf_file', 'File', 'required');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 		  					
				if(!empty($_FILES['pdf_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['pdf_file'],"course_file_meta");
					}else{ 
						$file = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 1,
					'page_count' => $this->input->post('page_count')
				);


				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');			
            }

		}

		$view_data['page']  = 'add_pdf';
		$data['page_title'] = "Add Pdf";		
		$data['page_data'] = $this->load->view('library/add_pdf', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_pdf_file_list() { 
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',
			
		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type =1 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm 
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm 
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 1
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		
		
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->URL;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";
						
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	
	
	public function add_ppt() { 
		if($this->input->post()) { 

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['ppt_file']['name']))
			{ 
				$this->form_validation->set_rules('ppt_file', 'File', 'required');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 		  					
				if(!empty($_FILES['ppt_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['ppt_file'],"course_file_meta");
					}else{ 
						$file = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 2,
					'page_count' => $this->input->post('page_count')
				);			

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');				
            }

		}

		$view_data['page']  = 'add_ppt';
		$data['page_title'] = "Add Ppt";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('library/add_ppt', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	
	public function ajax_ppt_file_list() { 
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',
			
		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type =2 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm 
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm 
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 2
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		
		
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->URL;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";				

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	public function add_video() { 
		if($this->input->post()) { 

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			
			if (empty($_FILES['video_file']['name']))
			{ 
				$this->form_validation->set_rules('video_file', 'File', 'required');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 		  					
				if(!empty($_FILES['video_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['video_file'],"course_file_meta");
					}else{ 
						$file = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 3
				);				
				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');	
            }

		}

		$view_data['page']  = 'add_video';
		$data['page_title'] = "Add Video";
		$data['page_data'] = $this->load->view('library/add_video', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	public function ajax_video_file_list() { 
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',
			
		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type =3 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm 
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm 
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 3
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		
		
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->URL;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";
						
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	
	public function add_epub() { 
		if($this->input->post()) { 

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['epub_file']['name']))
			{ 
				$this->form_validation->set_rules('epub_file', 'File', 'required');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 		  					
				if(!empty($_FILES['epub_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['epub_file'],"course_file_meta");
					}else{ 
						$file = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 4,
					'page_count' => $this->input->post('page_count')
				);				

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');	
            }

		}

		$view_data['page']  = 'add_epub';
		$data['page_title'] = "Add Epub";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('library/add_epub', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	
	public function ajax_epub_file_list() { 
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',
			
		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type = 4 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm 
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm 
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 4
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		
		
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->URL;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";
					

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}
	
	public function add_doc() { 
		if($this->input->post()) { 

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['doc_file']['name']))
			{ 
				$this->form_validation->set_rules('doc_file', 'File', 'required');
			}

			if ($this->form_validation->run() == FALSE) { 
             
            } 
			else { 		  					
				if(!empty($_FILES['doc_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['doc_file'],"course_file_meta");
					}else{ 
						$file = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 5,
					'page_count' => $this->input->post('page_count')
				);		

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');	
            }

		}

		$view_data['page']  = 'add_doc';
		$data['page_title'] = "Add Doc";		
		$data['page_data'] = $this->load->view('library/add_doc', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	
	public function ajax_doc_file_list() { 
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',
			
		);

		$query = "SELECT count(id) as total
								FROM course_topic_file_meta_master where file_type = 5 
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm 
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm 
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 5
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}
		
		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}
		
		
			
		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = $r->title;
			$nestedData[] = $r->URL;
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			//$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "course/edit_sub_course/" . $r->id . "'>Edit</a>";
			
			//$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}



}