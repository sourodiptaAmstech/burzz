<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends MX_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->helper('aul');
		modules::run('auth_panel/auth_panel_ini/auth_ini');
		$this->load->library('form_validation', 'uploads');
		$this->load->model("Library_model");
		$this->load->library('upload');

	}

	public function download(){
		$file = urldecode($_GET['file']);	
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    readfile($file);
	    exit;
	}

	public function amazon_s3_upload($name,$aws_path) {
		$_FILES['file'] = $name;
		require_once(FCPATH.'aws/aws-autoloader.php');

						$s3Client = new S3Client([
						'version'     => 'latest',
						'region'      => 'ap-south-1',
						'credentials' => [
						'key'    => AMS_S3_KEY,
						'secret' => AMS_SECRET,
						],
						]);
						$result = $s3Client->putObject(array(
							'Bucket' => AMS_BUCKET_NAME,
							'Key' => $aws_path.'/'.rand(0,7896756).str_replace([':', ' ', '/', '*','#','@','%'],"_",$_FILES["file"]["name"]), 
							'SourceFile' => $_FILES["file"]["tmp_name"],
							'ContentType' => 'image',
							'ACL' => 'public-read',
							'StorageClass' => 'REDUCED_REDUNDANCY',
							'Metadata' => array(        'param1' => 'value 1',        'param2' => 'value 2' )
						));
				$data=$result->toArray();
				return $data['ObjectURL'];

	}
	public function amazon_s3_upload_from_url($file_path,$aws_path) {
		
		$filePath = $file_path;

	require_once(FCPATH.'aws/aws-autoloader.php');
	
	$bucketName = AMS_BUCKET_NAME;
	$keyName = $aws_path.'/'.basename($filePath);
	$IAM_KEY = AMS_S3_KEY;
	$IAM_SECRET = AMS_SECRET;
	
	// Set Amazon S3 Credentials
	$s3 = S3Client::factory(
		array(
			'credentials' => array(
				'key' => $IAM_KEY,
				'secret' => $IAM_SECRET
			),
			'version' => 'latest',
			'region'  => 'ap-south-1'
		)
	);
  
	try {
		// Create temp file
	 	$tempFilePath = $_SERVER["DOCUMENT_ROOT"].'/dams/videothumbnail/tmp' . basename($filePath);
		$tempFile = fopen($tempFilePath, "w") or die("Error: Unable to open file.");
	    $fileContents = file_get_contents($filePath);
		$tempFile = file_put_contents($tempFilePath, $fileContents);
		//fclose($tempFile);
		
		// Put on S3
		$result =$s3->putObject(
			array(
				'Bucket'=>$bucketName,
				'Key' =>  $keyName,
				'SourceFile' => $tempFilePath,
				'StorageClass' => 'REDUCED_REDUNDANCY'
			)
		);
	} catch (S3Exception $e) {
		echo $e->getMessage();
	} catch (Exception $e) {
		echo $e->getMessage();
	}
		$data=$result->toArray();

		return $data['ObjectURL'];
	}
	public function index()
	{
		if($this->input->post()) {
			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['pdf_file']['name']))
			{
				$this->form_validation->set_rules('pdf_file', 'File', 'required');
			}
			if (empty($_FILES['thumbnail']['name']))
			{
				$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
			}

			if ($this->form_validation->run() == FALSE) {

            }
			else {
				if(!empty($_FILES['pdf_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['pdf_file'],"course_file_meta");
					}else{
						$file = '';
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					}else{
						$thumbnail = '';
					}

				$insert_data = array(					
					'main_id'=>$this->input->post('main_id'),
					'sub_id'=>$this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'thumbnail_url' => $thumbnail,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 1,
					'page_count' => $this->input->post('page_count'),
					'backend_user_id'=>$backend_user_id
				);


				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_pdf';
		$data['page_title'] = "Add Pdf";
		$data['page_data'] = $this->load->view('library/add_pdf', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_pdf_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}
		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',

		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type =1 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.thumbnail_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic, ctfmm.file_url 
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 1 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;			
			$nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/edit_pdf_library/" . $r->id . "'>Edit</a>
			<a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}



	public function add_ppt()
	{
		if($this->input->post()) {			
			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['ppt_file']['name']))
			{
				$this->form_validation->set_rules('ppt_file', 'File', 'required');
			}
			if (empty($_FILES['thumbnail']['name']))
			{
				$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
			}

			if ($this->form_validation->run() == FALSE) {

            }
			else {
				if(!empty($_FILES['ppt_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['ppt_file'],"course_file_meta");
					}else{
						$file = '';
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					}else{
						$thumbnail = '';
					}

				$insert_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'thumbnail_url' => $thumbnail,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 2,
					'page_count' => $this->input->post('page_count'),
					'backend_user_id'=>$backend_user_id
				);

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_ppt';
		$data['page_title'] = "Add Ppt";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('library/add_ppt', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_ppt_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',

		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type =2 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.thumbnail_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic, ctfmm.file_url
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 2 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}

		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length
		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "library/library/edit_ppt_library/" . $r->id . "'>Edit</a>
			<a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function add_video() {
		$this->cleardir();
		if($this->input->post()) {	

			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');

			if (empty($_FILES['video_file']['name']))
			{
				$this->form_validation->set_rules('video_file', 'File', 'required');
			}
			/*if (empty($_FILES['thumbnail']['name']))
			{
				$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
			}*/

			if ($this->form_validation->run() == FALSE)
			{

            }
			else {
				if(!empty($_FILES['video_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['video_file'],"course_file_meta");
					}else{
						$file = '';
					}

			



				if(!empty($_FILES['thumbnail']['name'])){
					 $thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					}else{
						// creating thumbnail from video
						require 'FFMpeg/FFMpeg.php';

						$video = $file;

						$image1 = time().'thumbnail1.jpg';
						$thumb = 'videothumbnail/'.$image1;
						
						shell_exec("ffmpeg -i $video -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumb 2>&1");
						 $file_path= base_url().$thumb;

						$path = $this->amazon_s3_upload_from_url($file_path,"course_file_meta");
						$path1 = $_SERVER["DOCUMENT_ROOT"]."/dams/videothumbnail/".$image1;
						unlink($path1);
						$path3 = $_SERVER["DOCUMENT_ROOT"]."/dams/videothumbnail/tmp".$image1;
						unlink($path3);
						$thumbnail = $path;
					}

				$insert_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'thumbnail_url' => $thumbnail,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 3,
					'backend_user_id'=>$backend_user_id
				);
				
				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_video';
		$data['page_title'] = "Add Video";
		$data['page_data'] = $this->load->view('library/add_video', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_video_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',

		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type =3 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.thumbnail_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic, ctfmm.file_url
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 3 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			//$nestedData[] = $r->title;
			$nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "library/library/edit_video_library/" . $r->id . "'>Edit</a>
					  <a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>
			";

			$nestedData[] = $action;
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}


	public function add_epub() {
		if($this->input->post()) {
			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['epub_file']['name']))
			{
				$this->form_validation->set_rules('epub_file', 'File', 'required');
			}
			if (empty($_FILES['thumbnail']['name']))
			{
				$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
			}

			if ($this->form_validation->run() == FALSE) {

            }
			else {
				if(!empty($_FILES['epub_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['epub_file'],"course_file_meta");
					}else{
						$file = '';
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					}else{
						$thumbnail = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'thumbnail_url' => $thumbnail,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 4,
					'page_count' => $this->input->post('page_count'),
					'backend_user_id'=>$backend_user_id
				);

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_epub';
		$data['page_title'] = "Add Epub";
		//$view_data['categories'] = $this->Category_list_model->get_category_list();
		$data['page_data'] = $this->load->view('library/add_epub', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


	public function ajax_epub_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',

		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type = 4 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();

		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.thumbnail_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic, ctfmm.file_url
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 4 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			 $nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "library/library/edit_epub_library/" . $r->id . "'>Edit</a>
			<a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>";
			$nestedData[] = $action;


			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function add_doc() {
		if($this->input->post()) {
			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
			if (empty($_FILES['doc_file']['name']))
			{
				$this->form_validation->set_rules('doc_file', 'File', 'required');
			}
			if (empty($_FILES['thumbnail']['name']))
			{
				$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
			}

			if ($this->form_validation->run() == FALSE) {

            }
			else {
				if(!empty($_FILES['doc_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['doc_file'],"course_file_meta");
					}else{
						$file = '';
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					}else{
						$thumbnail = '';
					}

				$insert_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'thumbnail_url' => $thumbnail,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 5,
					'page_count' => $this->input->post('page_count'),
					'backend_user_id'=>$backend_user_id
				);

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_doc';
		$data['page_title'] = "Add Doc";
		$data['page_data'] = $this->load->view('library/add_doc', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_doc_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',
			4 => 'page_count',

		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type = 5 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.thumbnail_url as URL,ctfmm.page_count as page_count, csm.name as subject,cstm.topic as topic, ctfmm.file_url
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 5 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][5]['search']['value'])) {  //salary
			$sql.=" having page_count LIKE '" . $requestData['columns'][5]['search']['value'] . "%' ";
		}



		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;
			$nestedData[] = $r->page_count;
			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "library/library/edit_doc_library/" . $r->id . "'>Edit</a>
			 <a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>";
			$nestedData[] = $action;

			//$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);
		echo json_encode($json_data);  // send data as json format
	}


	public function edit_pdf_library($id){
		if($this->input->post()) 
		{
			// echo('<pre>');
			// print_r($this->input->post());die();
			//$this->form_validation->set_rules('main', 'Main', 'required');	
			//$this->form_validation->set_rules('sub', 'Sub', 'required');
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');

		/*	if (!empty($_FILES['pdf_file']['name']))
			{
				$this->form_validation->set_rules('pdf_file', 'File', 'required');
			}
		*/
			if ($this->form_validation->run() == FALSE) {
				
            }
			else {				
				$update_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 1,
					'page_count' => $this->input->post('page_count')
				);
				 			
				if(!empty($_FILES['pdf_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['pdf_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					$update_data['thumbnail_url'] = $thumbnail;
					}
				$file_id = $this->input->post('id');
				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				//$this->db->last_query();
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_pdf';
		$data['page_title'] = "Edit Pdf ";
		$view_data['pdf_detail'] = $this->Library_model->get_library_file_by_id($id);		
		$data['page_data'] = $this->load->view('library/edit_pdf', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function edit_ppt_library($id){
		if($this->input->post()) {
			//print_r($this->input->post());die();
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
	  /*		if (empty($_FILES['ppt_file']['name']))
			{
				$this->form_validation->set_rules('ppt_file', 'File', 'required');
			}
	  */
			if ($this->form_validation->run() == FALSE) {

            }
			else {
					$update_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 2,
					'page_count' => $this->input->post('page_count')
				);
				if(!empty($_FILES['ppt_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['ppt_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					$update_data['thumbnail_url'] = $thumbnail;
					}
				$file_id = $this->input->post('id');

				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_ppt';
		$data['page_title'] = "Edit Ppt ";
		$view_data['ppt_detail'] = $this->Library_model->get_library_file_by_id($id);
		$data['page_data'] = $this->load->view('library/edit_ppt', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function edit_video_library($id){

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');

	/*		if (empty($_FILES['video_file']['name']))
			{
				$this->form_validation->set_rules('video_file', 'File', 'required');
			}
    */
			if ($this->form_validation->run() == FALSE) {

            }
			else {
				$update_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 3
				);
				if(!empty($_FILES['video_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['video_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					$update_data['thumbnail_url'] = $thumbnail;
					}
				$file_id = $this->input->post('id');					
				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_video';
		$data['page_title'] = "Edit Video ";
		$view_data['video_detail'] = $this->Library_model->get_library_file_by_id($id);

		$data['page_data'] = $this->load->view('library/edit_video', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function edit_epub_library($id){
		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
	/*		if (empty($_FILES['epub_file']['name']))
			{
				$this->form_validation->set_rules('epub_file', 'File', 'required');
			}
    */
			if ($this->form_validation->run() == FALSE) {

            }
			else {
				$update_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 4,
					'page_count' => $this->input->post('page_count')
				);
				if(!empty($_FILES['epub_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['epub_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					$update_data['thumbnail_url'] = $thumbnail;
					}
				$file_id = $this->input->post('id');


				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_epub';
		$data['page_title'] = "Edit epub ";
		$view_data['epub_detail'] = $this->Library_model->get_library_file_by_id($id);
		$data['page_data'] = $this->load->view('library/edit_epub', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	public function edit_doc_library($id){
		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');
			$this->form_validation->set_rules('page_count', 'Page Count', 'required');
	/*		if (empty($_FILES['doc_file']['name']))
			{
				$this->form_validation->set_rules('doc_file', 'File', 'required');
			}
    */
			if ($this->form_validation->run() == FALSE) {

            }
			else {
				$update_data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 5,
					'page_count' => $this->input->post('page_count')
				);
				if(!empty($_FILES['doc_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['doc_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				if(!empty($_FILES['thumbnail']['name'])){
					$thumbnail  = $this->amazon_s3_upload($_FILES['thumbnail'],"course_file_meta");
					$update_data['thumbnail_url'] = $thumbnail;
					}
				$file_id = $this->input->post('id');


				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_doc';

		$data['page_title'] = "Edit Doc ";
		$view_data['doc_detail'] = $this->Library_model->get_library_file_by_id($id);
		$data['page_data'] = $this->load->view('library/edit_doc', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}


		public function add_image()
		{
		if($this->input->post()) {
			$user_data = $this->session->userdata('active_user_data');
			$backend_user_id = $user_data->id;

			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');

			if (empty($_FILES['image_file']['name']))
			{
				$this->form_validation->set_rules('image_file', 'Image File', 'required');
			}

			if ($this->form_validation->run() == FALSE) {

            }
			else {
				if(!empty($_FILES['image_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['image_file'],"course_file_meta");

					}else{
						$file = '';
					}

				$insert_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'file_url' => $file,
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 6,
					'backend_user_id' => $backend_user_id
				);

				$this->db->insert('course_topic_file_meta_master',$insert_data);
				page_alert_box('success','Action performed','File added successfully');
            }

		}

		$view_data['page']  = 'add_image';
		$data['page_title'] = "Add Image";
		$data['page_data'] = $this->load->view('library/add_image', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}

	public function ajax_image_file_list() {
		// storing  request (ie, get/post) global array to a variable
		$requestData = $_REQUEST;
		$user_data = $this->session->userdata('active_user_data');
		$instructor_id = $user_data->instructor_id;
		$backend_user_id = $user_data->id;
		$where = "";
		if($instructor_id != 0){
			$where = "AND ctfmm.backend_user_id = $backend_user_id";
		}

		$columns = array(
			// datatable column index  => database column name
			0 => 'id',
			1 => 'title',
			2 => 'URL',
			3 => 'subject',
			4 => 'topic',


		);

		$query = "SELECT count(ctfmm.id) as total
								FROM course_topic_file_meta_master ctfmm where ctfmm.file_type =6 $where
								";
		$query = $this->db->query($query);
		$query = $query->row_array();
		$totalData = (count($query) > 0) ? $query['total'] : 0;
		$totalFiltered = $totalData;

		$sql = "SELECT ctfmm.id as id,ctfmm.title as title ,ctfmm.file_url as URL, csm.name as subject,cstm.topic as topic, ctfmm.file_url
								FROM course_topic_file_meta_master as  ctfmm
								join course_subject_master as csm
								on ctfmm.subject_id = csm.id
								join course_subject_topic_master as cstm
								on ctfmm.topic_id = cstm.id
								where  ctfmm.file_type = 6 $where
								";

		// getting records as per search parameters
		if (!empty($requestData['columns'][0]['search']['value'])) {   //name
			$sql.=" AND id LIKE '" . $requestData['columns'][0]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][1]['search']['value'])) {  //salary
			$sql.=" having title LIKE '" . $requestData['columns'][1]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][2]['search']['value'])) {  //salary
			$sql.=" having URL LIKE '" . $requestData['columns'][2]['search']['value'] . "%' ";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {  //salary
			$sql.=" having subject LIKE '" . $requestData['columns'][3]['search']['value'] . "%' ";
		}
		if (!empty($requestData['columns'][4]['search']['value'])) {  //salary
			$sql.=" having topic LIKE '" . $requestData['columns'][4]['search']['value'] . "%' ";
		}




		$query = $this->db->query($sql)->result();

		$totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

		$sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

		$result = $this->db->query($sql)->result();
		$data = array();
		foreach ($result as $r) {  // preparing an array
			$nestedData = array();

			$nestedData[] = $r->id;
			$nestedData[] = substr($r->title,0,13).(strlen($r->title)>13?'...':'');
			$nestedData[] = "<img  height = '60' width ='60' src= ".$r->URL.">";
			$nestedData[] = $r->subject;
			$nestedData[] = $r->topic;

			$action = "<a class='btn-sm btn btn-success btn-xs bold' href='" . AUTH_PANEL_URL . "library/library/edit_image_library/" . $r->id . "'>Edit</a>";
			$action .= "<a style='margin-left:10px' class='btn-sm btn btn-success btn-xs bold copy_url' data-url='".$r->URL."' >Copy Url </a>
			 <a style='margin-left:10px' target='_blank' class='btn-sm btn btn-success btn-xs bold' href='". AUTH_PANEL_URL ."library/library/download?file=".urlencode($r->file_url)."'>Download</a>";
			$nestedData[] = $action;

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal" => intval($totalData), // total number of records
			"recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
		);

		echo json_encode($json_data);  // send data as json format
	}

	public function edit_image_library($id){
		if($this->input->post()) 
		{
			
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('title', 'File Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('subject_id', 'Subject', 'required');
			$this->form_validation->set_rules('topic_id', 'Topic', 'required');

	/*		if (empty($_FILES['image_file']['name']))
			{
				$this->form_validation->set_rules('image_file', 'File', 'required');
			}
    */
			if ($this->form_validation->run() == FALSE) {

            }
			else {
				$update_data = array(
					'main_id' => $this->input->post('main_id'),
					'sub_id' => $this->input->post('sub_id'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'subject_id' => $this->input->post('subject_id'),
					'topic_id' => $this->input->post('topic_id'),
					'file_type' => 6
				);
				if(!empty($_FILES['image_file']['name'])){
					$file  = $this->amazon_s3_upload($_FILES['image_file'],"course_file_meta");
					$update_data['file_url'] = $file;
					}
				$file_id = $this->input->post('id');
				$this->db->where('id',$file_id);
				$this->db->update('course_topic_file_meta_master',$update_data);
				page_alert_box('success','Action performed','File updated successfully');
            }

		}
		$view_data['page']  = 'edit_image';
		$data['page_title'] = "Edit Image ";
		$view_data['video_detail'] = $this->Library_model->get_library_file_by_id($id);
		$data['page_data'] = $this->load->view('library/edit_image', $view_data, TRUE);
		echo modules::run(AUTH_DEFAULT_TEMPLATE, $data);
	}
	private function cleardir(){
	
	$folder_path = "videothumbnail"; 

// List of name of files inside 
// specified folder 
$files = glob($folder_path.'/*');  

// Deleting all the files in the list 
foreach($files as $file) { 

if(is_file($file))  

    // Delete the given file 
    unlink($file);  
} 
}


}
