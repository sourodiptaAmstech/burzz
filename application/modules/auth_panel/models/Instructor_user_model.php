<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Instructor_user_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }


    public function get_user_profile($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('backend_user')->row_array();
		  
	/*		if($query['is_course_register'] == 1) {
			$sql = $this->db->query('SELECT mc.text as Stream,mclo.text as Course,urd.interested_course from `user_registration_data` as urd INNER JOIN `master_category` as mc ON urd.master_id = mc.id INNER JOIN master_category_level_one mclo ON urd.master_id_level_one = mclo.id where user_id="'.$query['id'].'" ')->row_array();
			$query['stream'] = $sql['Stream'];
			$query['course'] = $sql['Course'];
			$query['interested_course'] = $sql['interested_course'];

			$interested_course = explode(',',$query['interested_course']);
			$query['interested_course']= array();
			foreach($interested_course as $key=>$value) {
				$this->db->select('text');
				$this->db->where('id',$value);
				$course = $this->db->get('course_intersted_in_list')->row_array();
				$query['interested_course'][$key] = $course['text'];
			}
                           
			}       */
			
			//echo "<pre>";print_r($query);die;  
			return $query;  
    }
	
	
	public function get_user_instructor_details($id) {
				
		//$this->db->join('course_instructor_information', 'users.id = request_helping_hand.request_id');
		$this->db->where('user_id',$id);
		$query = $this->db->get('course_instructor_information')->row_array();
		  
			
		if($query){
		//echo "<pre>";print_r($query);die;  
		return $query; 
		}
		else{
			return false;
		}
    }
	
	public function get_instructor_rating_details_by_id($id) {
		$this->db->select("course_instructor_rating.*,users.name,DATE_FORMAT(FROM_UNIXTIME(course_instructor_rating.creation_time/1000), '%d-%m-%Y %h:%i:%s') as creation_time");
		$this->db->join('users','users.id=course_instructor_rating.user_id');
    	$this->db->where('course_instructor_rating.id',$id);
    	return $this->db->get('course_instructor_rating')->row_array();

    }



	
	public function delete_review($id) {
		
    	$this->db->where('id',$id);
		$this->db->delete('course_instructor_rating');
    	return true;

    }


}