<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Course_transactions_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_course_transactions_details($id) {
    	$this->db->select("course_transaction_record.*,user.name as user_name,user.profile_picture as user_profile_picture,user.email as user_email,user.mobile as user_mobile,ins_user.username as instructor_name,ins_user.profile_picture as instructor_profile_picture,ins_user.email as instructor_email,ins_user.mobile as instructor_mobile,cm.title as course_name,cm.description as course_description,cm.cover_image as course_image");
		$this->db->join('users as user','course_transaction_record.user_id = user.id','left');
		$this->db->join('backend_user as ins_user','course_transaction_record.instructor_id = ins_user.id','left');
		$this->db->join('course_master as cm','course_transaction_record.course_id = cm.id','left');
		$this->db->where('course_transaction_record.id',$id);
    	return $this->db->get('course_transaction_record')->row_array();
    }

}