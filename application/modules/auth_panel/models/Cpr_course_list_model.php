<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Cpr_course_list_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_course_image_exist_by_id($id) {
        $this->db->select('course_master.cover_image as image');
        $this->db->where('id',$id);
        return $this->db->get('cpr_course_master')->row_array();

    }

    public function delete_filter($id,$filter_name){
		$this->db->where('course_id',$id);
        $this->db->where('f_name',$filter_name);
		$result = $this->db->delete('cpr_course_filtration');
		return $result;
    }

  	public function delete_course($id){
		$data['state'] = 1;
		$this->db->where('id',$id);
      	$result = $this->db->update("cpr_course_master",$data);
      	return true;

	}

}
