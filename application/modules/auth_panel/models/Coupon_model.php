<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Coupon_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }


    public function get_coupon_by_id($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('course_coupon_master')->row_array();
 
		return $query;  
    }


    public function update_coupon($id,$insert_data) {    	    		
		$this->db->where('id',$id);
		return $this->db->update('course_coupon_master',$insert_data);

	}
	
	public function delete_coupon($id){
      $data = array('state' =>1);
      $this->db->where('id',$id);
      $result = $this->db->update("course_coupon_master",$data);
    }
    


}