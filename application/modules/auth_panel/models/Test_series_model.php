<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Test_series_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    public function get_test_series_by_id($id) {
    	$this->db->where('id',$id);
    	return $this->db->get('course_test_series_master')->row_array();

    }

    public function get_questions_exist_in_test_series($id) {
    	$sql="select count(id) as total from course_testseries_question_relation where test_series_id=$id";
    	return $result=$this->db->query($sql)->row()->total;
    }
	
	public function test_series_result_status_count(){   
		
		$today = date('d-m-Y');
		$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
		$currentMonth = date('m');
		$currentYear = date('Y');
		$current_millisecond = strtotime(date('01-m-Y 00:00:00'))*1000;
		
		
		$result = array();
		$this->db->select("count(id) total,sum(case when result = 1  then  1 when result IS NULL then 0  else 0 end) passed,sum(case when result = 0 then  1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where("DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y')='$today'");
		$daily = $this->db->get('course_test_series_report')->row_array();		
		$result['daily'] = $daily;
		
		$this->db->select("count(id) total,sum(case when result = 1 then 1 when result IS NULL then 0 else 0 end) passed,sum(case when result = 0 then 1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where("DATE_FORMAT(FROM_UNIXTIME(creation_time/1000), '%d-%m-%Y') BETWEEN '$currentWeekDate' AND '$today'");
		$weekly = $this->db->get('course_test_series_report')->row_array();
		$result['weekly'] = $weekly;
		
		$this->db->select("count(id) total,sum(case when result = 1 then 1 when result IS NULL then 0 else 0 end) passed,sum(case when result = 0 then 1 when result IS NULL then 0 else 0 end) failed");
		$this->db->where(" creation_time >'$current_millisecond'");
		$monthly = $this->db->get('course_test_series_report')->row_array();		
		$result['monthly'] = $monthly;
		
		return $result;
	}
	
	public function delete_question_from_testseries($id,$test_series_id){
      $data = array('state' =>1);
      $this->db->where('test_series_id',$test_series_id);
	  $this->db->where('question_id',$id);
      $result = $this->db->delete("course_testseries_question_relation");
    }
	
	public function add_question_to_testseries($test_series_id,$question_id) {
    	$this->db->where('test_series_id',$test_series_id);
		$this->db->where('question_id',$question_id);
    	$check = $this->db->get('course_testseries_question_relation')->row_array();
		if($check){
			return false;
		}else{
			$data = array('test_series_id'=>$test_series_id,
						   'question_id'=>$question_id);			
			$result = $this->db->insert("course_testseries_question_relation",$data);
			if($result){
				return true;
			}
			
		}

    }
	

}