<!-- subcategory list -->
<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading">
		FAQs
		<span class="tools pull-right">
		<a href="javascript:;" class="fa fa-chevron-down"></a>

		</span>
		</header>
		<div class="panel-body">
		<div class="adv-table">
		<div class="col-sm-12">

		<form role="form" method="post" enctype="multipart/form-data">

              <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Question</label>
                  <input type="text" placeholder="Title" value="" name="question" class="form-control">
                  <span style="color:red"><?php echo form_error("question"); ?></span>
              </div>


               <div class="form-group col-sm-6">
                  <label for="exampleInputPassword1">Answer</label>
                  <textarea name="description" class="form-control ckeditor"></textarea>
                  <span style="color:red"><?php echo form_error("description"); ?></span>
              </div>


              <button class="btn btn-info pull-right" type="submit">Submit</button>
          </form>
          </div>

<hr>

    <table  class="display table table-bordered table-striped" id="all-subcategory-grid" style="margin-top: 100px">
      <thead>
        <tr>
        <th>#</th>
        <th>Ques</th>
        <th>Answer</th>
        <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
        <th></th>
        <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
        <th></th>
        <th></th>
          </tr>
      </thead>

    </table>

		</div>
  </div>



	</section>





</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD


               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                        $('#category').click(function() {
                          //alert($('.categories_element').val());
                        })

                   } );
               </script>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-subcategory-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"static_pages/ajax_faqs_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
             $('.search-input-select').on( 'change', function () {   // for select box
                  var i =$(this).attr('data-column');
                  var v =$(this).val();
                  dataTable.columns(i).search(v).draw();
              } );
                   } );



    $(document).on('click','.change_status',function(){
  var status=$(this).attr("status");
  var id=$(this).attr("id");
    $('#'+id).parent().parent().hide();
  $.ajax({
        url :"$adminurl"+"static_pages/delete_faqs/"+id , // json datasource
        type: "post",
        success: function (response) {
          response=JSON.parse(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });


  //selector.find('div:eq(4)').find('input').val(realprice);
 });








               </script>


EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
