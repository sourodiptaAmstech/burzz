<!-- subcategory list -->
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            Help Desk
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>

            </span>
        </header>
        <div class="panel-body">
            <div class="adv-table">



                <table  class="display table table-bordered table-striped" id="all-subcategory-grid">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>

                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD

               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                   	    $('#category').click(function() {
                   	    	//alert($('.categories_element').val());
                   	    })

                   } );
               </script>

               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {

                       var table = 'all-subcategory-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :"$adminurl"+"static_pages/ajax_feedback_list/", // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
						 $('.search-input-select').on( 'change', function () {   // for select box
							    var i =$(this).attr('data-column');
							    var v =$(this).val();
							    dataTable.columns(i).search(v).draw();
							} );
                   } );







                  $(document).on('click','.change_status',function(){
 	var status=$(this).attr("status");
 	var id=$(this).attr("id");
 	$.ajax({
        url :"$adminurl"+"products/update_status/"+id , // json datasource
        type: "post",
        data: 'status='+status ,
        success: function (response) {
        	response=JSON.parse(response);
	       	if(response.status==true){
	       		//console.log(response.message);
	       		console.log(this);
	       		if(status==1){
					$('#'+id).html('Unpublished');
					$('#'+id).removeClass('btn-success').addClass('btn-danger');
					$('#'+id).attr("status",2);
				}else{
					$('#'+id).html('Published');
					$('#'+id).removeClass('btn-danger').addClass('btn-success');
					$('#'+id).attr("status",1);
				}
			}
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });


 	//selector.find('div:eq(4)').find('input').val(realprice);
 });







               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
