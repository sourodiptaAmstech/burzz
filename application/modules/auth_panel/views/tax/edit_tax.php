 <link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>/tags/bootstrap-tagsinput.css">
<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Edit Tax
      </header>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
              
              <div class="form-group">
                  <label for="exampleInputPassword1">Title</label>
                  <input type="text" placeholder="Title" value="<?php echo $tax['tax_value']; ?>" name="tax_value" class="form-control">
                  <span style="color:red"><?php echo form_error("tax_value"); ?></span>
              </div>
               
               
              
              
              <button class="btn btn-info" type="submit">Submit</button>
          </form>
      </div>
  </section>
</div>
<?php
$adminurl = AUTH_PANEL_URL;
$assets=ASSETS;
$custum_js = <<<EOD

 <script type="text/javascript" charset="utf8" src="$assets/tags/bootstrap-tagsinput.min.js"></script>
 
          
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );