	<div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Edit Email Template 
          </header>
          <div class="panel-body">
              <form role="form" method="post" action="<?php echo AUTH_PANEL_URL.'mailer/update_edited_template'; ?>">
          		  <div class="form-group">
                      <label for="exampleInputPassword1">Template Name</label>
                      <input type="text" value="<?php echo $template['template_name']; ?>" placeholder="Enter template name" id="template_name"  name="template_name" class="form-control" readonly="readonly">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Template Variable</label>
                      <input type="text" value="<?php echo $template['template_variable']; ?>" placeholder="Enter template name" id="template_variable"  name="template_variable" class="form-control" >
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Template</label>
                      <textarea name="template_html" class="form-control ckeditor">
                      <?php echo $template['template_html']; ?>
                      </textarea>
                  </div>
                  <input type="hidden" name="id" value="<?php echo $template['id']; ?>">
                  <button class="btn btn-info" type="submit">Submit</button>
              </form>
          </div>
      </section>
    </div>
<?php
$assetsurl = AUTH_ASSETS.'assets/ckeditor/ckeditor.js';
$custum_js = <<<EOD
              
				<script type="text/javascript" src="$assetsurl"></script>     
					<script>
						CKEDITOR.replace('html_template');
					</script>

EOD;

	echo modules::run('auth_panel/template/add_custum_js',$custum_js );
?>

