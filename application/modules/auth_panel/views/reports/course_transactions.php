<?php
$period = $this->input->get('period');

$today = date('d-m-Y');
$currentWeekDate = date('d-m-Y', strtotime('-7 days', strtotime($today)));
$currentMonth = date('m');
$currentYear = date('Y');
$current_millisecond = strtotime(date('01-m-Y 00:00:00')) * 1000;
if ($period == "today") {
	$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y')= '$today' ";

	$total_revenue = 0;

	$sql = "SELECT sum(package_price) as total FROM `transaction_record` where DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y') = '$today'  and transaction_status = 1";
	$total_sale = 0; // $this->db->query($sql)->row()->total;

	$sql = "SELECT count(id) as total FROM `transaction_record` WHERE  transaction_status =1 and package_price > 0 and   DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y') = '$today'";
	$total_transactions = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where";
	$total_review = 0; // $this->db->query($sql)->row()->total;

} elseif ($period == "yesterday") {
	$yesterday = date('d-m-Y', strtotime($today . ' - 1 days'));
	$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%d-%m-%Y')= '$yesterday' ";

	$total_revenue = 0;

	$sql = "SELECT sum(package_price) as total FROM `transaction_record` $where and transaction_status = 1";
	$total_sale = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(id) as total FROM `transaction_record` $where AND  transaction_status =1 and package_price > 0";
	$total_transactions = 0; // $this->db->query($sql)->row()->total;

	$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
	$total_review = 0; //$this->db->query($sql)->row()->total;
} elseif ($period == "7days") {
	$week = strtotime("-1 week") . "000";
	$where = " WHERE creation_time >=  $week ";

	$total_revenue = 0;

	$sql = "SELECT sum(package_price) as total FROM `transaction_record` $where AND  transaction_status =1";
	$total_sale = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(id) as total FROM `transaction_record` $where and package_price > 0";
	$total_transactions = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
	$total_review = 0; //$this->db->query($sql)->row()->total;
} elseif ($period == "current_month") {
	$current_month = date('m-Y');
	$where = " WHERE DATE_FORMAT(FROM_UNIXTIME(SUBSTR(creation_time,1,10)), '%m-%Y') = '$current_month' ";

	$total_revenue = 0;

	$sql = "SELECT sum(package_price) as total FROM `transaction_record` $where  and transaction_status = 1  ";
	$total_sale = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(id) as total FROM `transaction_record` $where AND  transaction_status =1 and package_price > 0";
	$total_transactions = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(`id`) as total FROM `course_user_rating` $where ";
	$total_review = $this->db->query($sql)->row()->total;
} elseif ($period == "all" || $period == "" || $period == "custom") {

	$total_revenue = 0;

	$sql = "SELECT sum(package_price) as total FROM `transaction_record` where transaction_status = 1";
	$total_sale = 0; //$this->db->query($sql)->row()->total;

	$sql = "SELECT count(id) as total FROM `transaction_record` WHERE transaction_status =1 and package_price > 0 ";
	$total_transactions = 0; //$this->db->query($sql)->row()->total;

}

?>
<?php if ($period != "custom") {?>
<div class=" state-overview">
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <a href="<?php echo AUTH_PANEL_URL . "course_product/course/all_courses_reviews_list?period=$period"; ?>" >
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
                              <h1 class="count"><?php //echo //$total_review; ?></h1>
                              <p>Reviews</p>
                          </div>
                          </a>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol red">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count2"><?php echo $total_transactions; ?></h1>
                              <p>Paid Transactions</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol yellow">
                              <i class="fa fa-shopping-cart"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count3"><?php echo $total_sale; ?></h1>
                              <p>Sale amount</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="panel">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
                              <h1 class=" count4"><?php echo $total_revenue; ?></h1>
                              <p>Revenue</p>
                          </div>
                      </section>
                  </div>
              </div>

<?php }?>
<div class="col-sm-12">
  <section class="panel">
    <header class="panel-heading">
    All Transactions
    <span class="tools pull-right">

    </span>
    <!-- download csv  -->
    <span class="tools pull-right">
      <form id="download_content_csv" method="post" action=""  >

        <button class="btn btn-sm btn-danger margin-right bold">
          <i class="fa fa-file" aria-hidden="true"></i>
          Download csv
        </button>
        <input name="download_pdf" class="btn btn-info btn-sm  margin-right bold" value="Download PDF" type="submit">
        <textarea style="display:none;" name="input_json"></textarea>
      </form>
    </span>

    </header>
    <div class="clearfix"></div>
    <div class="panel-body">
    <div class="adv-table">
		<!--<?php if ($period == "custom") {?>
  	<div class="col-md-6 pull-right custom_search" >
          <div data-date-format="dd-mm-yyyy" data-date="13/07/2013" class="input-group ">
              <div  class="input-group-addon">From</div>
              <input type="text" id="min-date-course-transaction" class="form-control date-range-filter input-sm course_start_date"  placeholder="">
              <div class="input-group-addon">to</div>
              <input type="text" id="max-date-course-transaction" class="form-control date-range-filter input-sm course_end_date"  placeholder="">
              <div class="input-group-addon btn date-range-filter-clear">Clear</div>
          </div>
      </div>
		<?php }?>-->

    <div class="col-md-12 custom_filter_label  margin-top  margin-bottom" >

    </div>

    <table  class="display table table-bordered table-striped " id="all-Course-transactions-grid">
      <thead>

        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Course </th>
          <th>Pay Via</th>
  		    <th>Coupon </th>
          <th>Ins. Name</th>
          <th>Status</th>
          <th>On</th>
          <th>Type</th>
          <th>Points</th>
          <th>Price</th>
          <th>Gst</th>
          <th>Net Amt.</th>
          <th>Ins. Share</th>
          <th>Profit</th>
          <th>Action</th>
        </tr>
      </thead>
      <thead>
          <tr>
            <th><input type="text" data-column="0"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="1"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="2"  value="<?php echo urldecode($this->input->get('course_name')); ?>" class="search-input-text form-control course_name_search"></th>
            <th></th>
            <th><input type="text" data-column="4"  class="search-input-text form-control"></th>
            <th><input type="text" data-column="5" value="<?php echo urldecode($this->input->get('instructor_name')); ?>" class="search-input-text form-control"></th>
            <th>
              <select data-column="6"  class="form-control search-input-select">
              <option value="" >(All)</option>
              <option value="0" <?php echo ($this->input->get('status') == 'pending') ? 'selected' : ''; ?> >Pending</option>
              <option value="1" <?php echo ($this->input->get('status') == 'complete') ? 'selected' : ''; ?> >Complete</option>
              <option value="2" <?php echo ($this->input->get('status') == 'cancel') ? 'selected' : ''; ?> >Cancel</option>
              <option value="3" <?php echo ($this->input->get('status') == 'refund_req') ? 'selected' : ''; ?> >Refund Req.</option>
              <option value="4" <?php echo ($this->input->get('status') == 'refunded') ? 'selected' : ''; ?> >Refunded</option>
              </select>
            </th>
            <th><input type="text" data-column="7"    class="search-input-text instructor_name_search form-control"></th>
            <th>
              <select data-column="8"  class="form-control search-input-select">
                <option value="">(All)</option>
                <option selected="selected" value="1">Paid</option>
                <option value="2">Free</option>
              </select>
            <th></th>
            <th></th>
            <th> </th>
            <th><input type="text" data-column="12"  class="search-input-text form-control"></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
      </thead>
    </table>
    </div>
    </div>
  </section>
</div>



<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
 <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
                        $('#category').click(function() {
                          //alert($('.categories_element').val());
                        })

                   } );
               </script>

               <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" >

                   jQuery(document).ready(function() {
var get_all_record = "$adminurl"+"reports/transactions/get_ajax_course_transactions_list/";
                       var table = 'all-Course-transactions-grid';
                       var dataTable = jQuery("#"+table).DataTable( {
                           "processing": true,
                           "serverSide": true,
                           "order": [[ 0, "desc" ]],
                           "ajax":{
                               url :  get_all_record, // json datasource
                               type: "post",  // method  , by default get
                               error: function(){  // error handling
                                   jQuery("."+table+"-error").html("");
                                   jQuery("#"+table+"_processing").css("display","none");
                               }
                           }
                       } );
                       jQuery("#"+table+"_filter").css("display","none");
                       $('.search-input-text').on( 'keyup click', function () {   // for text boxes
                           var i =$(this).attr('data-column');  // getting column index
                           var v =$(this).val();  // getting search input value
                           dataTable.columns(i).search(v).draw();
                       } );
             $('.search-input-select').on( 'change', function () {   // for select box
                  var i =$(this).attr('data-column');
                  var v =$(this).val();
                  dataTable.columns(i).search(v).draw();
              } );
                   } );











               </script>

EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
