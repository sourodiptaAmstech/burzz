<?php
/*******  Instructor User Login Details (In Session) *******/
$user_data = $this->session->userdata('active_user_data');
$instructor_id = $user_data->id;

/**********************************************************/
 ?>
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

	<!-- ########################### Instructor Profile Details Begins #####################################-->

	 <li class="sub-menu">
                <a class="" href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span> Profile </span>
                </a>
                <ul class="sub">
                    <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'course_product/Instructor_transactions_details?transaction_id='.$instructor_id;  ?>">Profile Details</a></li>
                    <li class="" ><a href="<?php echo AUTH_PANEL_URL . 'instructor_user/instructor_account_details/'.$instructor_id; ?>">Bank Account Details</a></li>
                </ul>
            </li>
<!-- ########################### Instructor Rewiews List Ends #####################################-->

<!-- ########################### Instructor Reviews Details Begins #####################################-->
                <li>
                 <a class="" href="<?php echo AUTH_PANEL_URL . 'instructor_user/instructor_reviews_list/'.$instructor_id; ?>"><i class="fa fa-star-half-o" ></i><span>Reviews List</span></a>
                </li>
<!-- ########################### Instructor Reviews Details Ends #####################################-->

                <li>
                 <a class="" href="<?php echo AUTH_PANEL_URL . 'course_product/Link_page/links'; ?>"><i class="fa fa-info" ></i><span>Course Management</span></a>
                </li>
				 <li>
                 <a class="" href="<?php echo AUTH_PANEL_URL . 'instructor_user/instructor_transaction_list/'.$instructor_id; ?>"><i class="fa fa-inr" ></i><span>Accounts</span></a>
                </li>
 </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
