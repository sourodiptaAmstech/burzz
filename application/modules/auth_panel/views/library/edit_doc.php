<div class="col-lg-6">
	  <section class="panel">
		  <header class="panel-heading">
			  Edit DOC 
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 <input type="hidden"  name = "id" id="id" value="<?php echo $doc_detail['id']; ?>" class="form-control input-sm">
				  <div class="form-group">
                    <label for="exampleInputEmail1">Subject</label>
                    <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">					 
                    </select>
					  <span class="error bold"><?php echo form_error('subject_id');?></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Topic</label>
                    <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						
                    </select>
					 <span class="error bold"><?php echo form_error('topic_id');?></span>
                </div>
				   <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                   <textarea rows="4" cols="50" class="form-control input-sm" name="description"  class="form-control"><?php echo $doc_detail['description'] ?></textarea><span class="error bold"><?php echo form_error('description');?></span>
                </div>
                                 
				  <div class="form-group">
					  <label >Doc Title</label>
					  <input type="test" placeholder="Enter title" name = "title" id="title" value = "<?php echo $doc_detail['title'] ?>" class="form-control">
					   <span class="error bold"><?php echo form_error('title');?></span>
				  </div>

		        <div class="form-group">
					  <label for="exampleInputFile">Upload Doc</label>
					  <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" name = "doc_file" id="exampleInputFile">
					  <span class="error bold"><?php echo form_error('doc_file');?></span>
				  </div> 
				  <div class="form-group">
					  <label for="exampleInputFile">Doc Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php echo form_error('thumbnail');?></span>
				  </div> 
				   <div class="form-group">
                    <label for="exampleInputEmail1">Page Count</label>
                     <input type="test" placeholder="Enter page count" name = "page_count" id="page_count" value = "<?php echo $doc_detail['page_count'] ?>" class="form-control">
						 <span class="error bold"><?php echo form_error('page_count');?></span>                  
                </div>
				
				  <button class="btn btn-info"  type="submit" >Update</button>
			  </form>

		  </div>
	  </section>
  </div>

<?php
$subject_id = $doc_detail['subject_id'];
$topic_id = $doc_detail['topic_id'];
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
                
				 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" > 
			   
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                        });
                         $(".subject_element_select").html(html).val('$subject_id').change();
                      }
                    });

                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val(); 
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>"; 
                          });
                           $(".topic_element_select").html(html).val('$topic_id'); 
                        }
                      });
                    });
					
				
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );