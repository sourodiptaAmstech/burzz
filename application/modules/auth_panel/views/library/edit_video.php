<div class="col-lg-6">
	  <section class="panel">
		  <header class="panel-heading">
			  Edit Video 
		  </header>
		  <div class="panel-body">
			  <form role="form" method="post" enctype="multipart/form-data">
				 <input type="hidden"  name = "id" id="id" value="<?php echo $video_detail['id']; ?>" class="form-control input-sm">
				 <?php
					 		$all_option = $this->db->where('status',0)->get('course_stream_name_master')->result(); 
					 		$main_option = $sub_option = "";
					 		foreach($all_option as $ao){
					 			if($ao->parent_id == 0 ){
					 				$main_option .= "<option value='".$ao->id."'>".$ao->name."</option>";
					 			}else{
					 				$sub_option .= "<option style='display: none;' class='substream sub".$ao->parent_id."' value='".$ao->id."'>".$ao->name."</option>";
					 			}
					 		}
					 	?>
						<div class="form-group col-md-12">
							<label for="exampleInputEmail1">Main stream</label>
							<select class="form-control input-xs stream_element_select" name="main_id">
							<option value=''>--select Stream--</option>
							<?php echo $main_option;?>
							</select>							
						</div>
						<div class="form-group col-md-12 ">
							<label for="exampleInputEmail1">Sub stream</label>
							<select class="form-control input-xs sub_element_select" name="sub_id">
							<option value=''>--select Sub Stream--</option>
							<?php echo $sub_option;?>
							</select>							
						</div>	
				  <div class="form-group">
                    <label for="exampleInputEmail1">Subject</label>
                    <select class="form-control input-sm subject_element_select" name="subject_id" class="form-control">						 
                    </select>
					  <span class="error bold"><?php echo form_error('subject_id');?></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Topic</label>
                    <select class="form-control input-sm topic_element_select" name="topic_id" class="form-control">						 
                    </select>
					<span class="error bold"><?php echo form_error('topic_id');?></span>
                </div>
				     <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                   <textarea rows="4" cols="50" class="form-control input-sm" name="description"  class="form-control"><?php echo $video_detail['description'] ?></textarea><span class="error bold"><?php echo form_error('description');?></span>
                </div>
                                 
				  <div class="form-group">
					  <label >Video Title</label>
					  <input type="test" placeholder="Enter title" name = "title" id="title" value = "<?php echo $video_detail['title'] ?>" class="form-control">
					   <span class="error bold"><?php echo form_error('title');?></span>
				  </div>

		        <div class="form-group">
					  <label for="exampleInputFile">Upload Video</label>
					  <input type="file" accept="video/mp4" name = "video_file" id="exampleInputFile">
					  <span class="error bold"><?php echo form_error('video_file');?></span>
				  </div>

				  <div>
						<video controls class="video" height="200" width="300" 
						 <source src="<?php if(isset($video_detail)){echo $video_detail['file_url'];} ?>">	
						</video>
				  </div>

				  <div class="form-group">
					  <label for="exampleInputFile">Video Thumbnail</label>
					  <input type="file" accept="image/*" name = "thumbnail" id="">
					  <span class="error bold"><?php echo form_error('thumbnail');?></span>
				  </div> 
		
				
				  <button class="btn btn-info"  type="submit" >Update</button>
			  </form>

		  </div>
	  </section>
  </div>

<?php
$adminurl = AUTH_PANEL_URL;
$subject_id = $video_detail['subject_id'];
$topic_id = $video_detail['topic_id'];

$stream_id 			=  $video_detail['main_id'];
$sub_stream_id	 	=  $video_detail['sub_id'];
$custum_js = <<<EOD
             
             <script>   
					$ ("#exampleInputFile").change(function () {
					var fileInput = document.getElementById('exampleInputFile');
					var fileUrl = window.URL.createObjectURL(fileInput.files[0]);
					$(".video").attr("src", fileUrl);
					});
             </script>


			<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
              <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
               <script type="text/javascript" language="javascript" > 
			   
                  jQuery.ajax({
                      url: "$adminurl"+"course_product/subject_topics/get_all_subject?return=json",
                      method: 'Get',
                      dataType: 'json',
                      success: function (data) {

                        var html = "<option value=''>--select--</option>";
                        $.each( data , function( key , value ) {
                          html += "<option value='"+value.id+"'>"+value.name+"</option>"; 
                        });
                         $(".subject_element_select").html(html).val('$subject_id').change();
                      }
                    });
					$('.stream_element_select').val('$stream_id').change();
					$('.sub_element_select').val('$sub_stream_id');
                    $( ".subject_element_select" ).change(function() {
                      id = $(this).val(); 
                       jQuery.ajax({
                        url: "$adminurl"+"course_product/subject_topics/get_topic_from_subject/"+id+"?return=json",
                        method: 'Get',
                        dataType: 'json',
                        success: function (data) {

                          var html = "<option value=''>--select--</option>";
                          $.each( data , function( key , value ) {
                            html += "<option value='"+value.id+"'>"+value.topic+"</option>"; 
                          });
                           $(".topic_element_select").html(html).val('$topic_id'); 
                        }
                      });
                    });
				$( ".stream_element_select" ).change(function() {
                      val = $(this).val();					 
                      $('.sub_element_select').val('');
                      $('.substream').hide();
                      $('.sub'+val).show();
                    });		
				
               </script>              

EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );