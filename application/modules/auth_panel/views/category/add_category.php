 <link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>/tags/bootstrap-tagsinput.css">
<div class="col-lg-12">
    <div class="col-lg-6">
      <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back </button></a>
    </div>
    <div class="col-lg-6"></div>
</div><br><br>

<div class="col-lg-6">
  <section class="panel">
      <header class="panel-heading">
          Add Category
      </header>
      <div class="panel-body">
          <form role="form" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="exampleInputEmail1">Parent</label>
                  <select name="parent_id" class="form-control search-input-select">
                  <option value="0" selected="selected">Select Parent</option>
                  <?php
              
				$this->db->select('id,title');
				$category1= $this->db->get_where('master_category',array('parent_id'=>0))->result_array();
              foreach($category1 as $cat){			  
			  	echo"<option value='".$cat['id']."'>".$cat['title']."</option>";
			  	featch($cat['title'],$cat['id'],'');
			  			  	
			  }?>
                  </select>
                  <span style="color:red"><?php echo form_error("parent_id"); ?></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Title</label>
                  <input type="text" placeholder="Title" value="<?php echo set_value('title'); ?>" name="title" class="form-control">
                  <span style="color:red"><?php echo form_error("title"); ?></span>
              </div>
               <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea placeholder="Description" name="description" class="form-control"><?php echo set_value('description'); ?></textarea>
                  <span style="color:red"><?php echo form_error("description"); ?></span>
              </div>
               <div class="form-group">
                  <label for="exampleInputPassword1">Image</label>
                  <input type="file" placeholder="Image"  name="image" class="form-control">
                  <span style="color:red"><?php echo form_error("image"); ?></span>
              </div>
               <div class="form-group">
                  <label for="exampleInputPassword1">Banner Image</label>
                  <input type="file" placeholder="Banner_image"  name="banner_image" class="form-control">
                  <span style="color:red"><?php echo form_error("banner_image"); ?></span>
              </div>
              
               <div class="form-group">
                  <label for="exampleInputPassword1">Pack Size</label>
                  <input class="form-control" type="text" data-role="tagsinput" placeholder="Pack Size" value="<?php echo set_value('pack_size'); ?>" name="pack_size" >
                  <span style="color:red"><?php echo form_error("pack_size"); ?></span>
              </div>
              <button class="btn btn-info" type="submit">Submit</button>
          </form>

      </div>
  </section>
</div>

<?php
$adminurl = AUTH_PANEL_URL;
$assets=ASSETS;
$custum_js = <<<EOD

 <script type="text/javascript" charset="utf8" src="$assets/tags/bootstrap-tagsinput.min.js"></script>
 
          
EOD;
echo modules::run('auth_panel/template/add_custum_js',$custum_js );




