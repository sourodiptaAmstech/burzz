<aside class="profile-nav col-lg-3">
      <section class="panel">
          <div class="user-heading round" style="padding: 20px">
              <?php
$img = ($user_data['profile_picture'] == '' ? base_url('auth_panel_assets/img/user.png') : $user_data['profile_picture']);

?>            
              <a href="<?php echo $img; ?>" class="image-link">
                  <img alt="" src="<?php echo $img; ?>">
              </a>

          </div>

          <ul class="nav nav-pills nav-stacked">
    <!--          <li><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post?search_user_post_id=' . $user_data['id']; ?>"> <i class="fa fa-thumbs-up"></i> Interested<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
              <li><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post?search_user_post_id=' . $user_data['id']; ?>"> <i class="fa fa-thumbs-down"></i> Not Interested<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
              <li><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post?search_user_post_id=' . $user_data['id']; ?>"> <i class="fa fa-users"></i> Matched<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
              <li><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post?search_user_post_id=' . $user_data['id']; ?>"> <i class="fa fa-ban"></i> Blocked<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
              <li><a href="<?php echo AUTH_PANEL_URL . 'fan_wall/all_post?search_user_post_id=' . $user_data['id']; ?>"> <i class="fa fa-comments"></i> Chat<span class="label label-danger pull-right"><?php echo $user_data['post_count']; ?></span></a></li>
!-->

              <li><a>Interested<span class="label label-danger pull-right"><?php echo $this->db->query("select count(id) as total from followed where user_id=" . $user_data['id'])->row()->total; ?></span></a></li>
              <li><a>Not Interested<span class="label label-danger pull-right"><?php echo $this->db->query("select count(id) as total from never_followed where user_id=" . $user_data['id'])->row()->total; ?></span></a></li>
              <li><a>Matched<span class="label label-danger pull-right"><?php echo $this->db->query("select count(id) as total from matches where user_id=" . $user_data['id'])->row()->total; ?></span></a></li>
              <li><a>Blocked<span class="label label-danger pull-right"><?php echo $this->db->query("select count(id) as total from blocked where user_id=" . $user_data['id'])->row()->total; ?></span></a></li>
              <li><a>Chat<span class="label label-danger pull-right"><?php echo 0 ?></span></a></li>


          </ul>

      </section>



  </aside>
  <aside class="profile-info col-lg-9">

      <section class="panel card">
        <div class="bio-graph-heading"><?php echo $user_profile_detail['headline']; ?></div>
          <div class="panel-body bio-graph-info ">
            <!--  <h1>Bio Graph <a href="javascript:history.go(-1)"><button class="pull-right btn btn-info btn-xs bold">Back to user list</button></a> </h1>-->
             <div class="col-md-12 pull-right">

              <?php if ($user_data['status'] != '2') {?>
                <div class="col-md-2 pull-right">
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/delete_user/delete/' . $user_data['id']; ?>" onclick="return confirm('Are you sure to delete this user');"><button class="pull-right btn btn-danger btn-xs bold">Delete User</button></a>
                </div>
                <div class="col-md-2 pull-right">
                <?php if ($user_data['status'] == '1') {?>
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/enable_user/enable/' . $user_data['id']; ?>"><button class="pull-right btn btn-warning btn-xs bold">Enable login</button></a>
                <?php } else {?>
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/disable_user/disable/' . $user_data['id']; ?>" onclick="return confirm('Are you sure to disable this user');"><button class="pull-right btn btn-warning btn-xs bold">Disable login</button></a>
                <?php }?>
                </div>
                <?php } else {?>
                <div class="col-md-2 pull-right">
                    <button class="pull-right btn btn-info btn-xs bold" disabled="disabled">User Deleted</button>
                </div>
              <?php }?>
<?php if ($user_data['status'] == 0) {?>
               <!-- <div class="col-md-2 pull-right">
                  <a href="<?php echo AUTH_PANEL_URL . 'bulk_messenger/bulk_message/send_bulk_message?M=' . base64_encode($user_data['mobile']); ?>"><button class="pull-right btn btn-success btn-xs bold">Send Sms</button></a>
              </div>-->

              <div class="col-md-2 pull-right">
                  <!-- <a href="<?php echo AUTH_PANEL_URL . 'bulk_messenger/push_notification/send_push_notification?q=' . base64_encode(json_encode(array('id' => $user_data['id'], 'name' => $user_data['name'], 'device_type' => $user_data['device_type'], 'device_token' => $user_data['device_token']))); ?>">
                    <button class="pull-right btn btn-info btn-xs bold">Push Notification</button>
                  </a> -->
                   <button class="pull-right btn btn-info btn-xs bold">Push Notification</button>
              </div>

              <div class="col-md-2 pull-right">
                  <a onclick="return confirm('You are going to refresh session of logged in user. User session will be destroyed.')" href="<?php echo AUTH_PANEL_URL . 'web_user/reset_session/' . $user_data['id']; ?>"><button class="pull-right btn btn-danger btn-xs bold">Refresh session</button></a>
              </div>

              <!--<div class="col-md-2 pull-right">
                  <a onclick="return confirm('Do you really want to send email to user.')" href="<?php echo AUTH_PANEL_URL . 'bulk_messenger/bulk_email/send_bulk_email?email=' . urlencode($user_data['email']); ?>"><button class="pull-right btn btn-info btn-xs bold">Send Email</button></a>
              </div>-->
<?php }?>
            <div class="col-md-2 pull-right">
                <?php if ($user_data['is_verified'] == '0') {?>
                    <a href="<?php echo AUTH_PANEL_URL . 'web_user/verify_user/' . $user_data['id']; ?>"><button class="pull-right btn btn-warning btn-xs bold">Verify User</button></a>
                <?php } else {?>
                <div class="col-md-2 pull-right">
                    <button class="pull-right btn btn-info btn-xs bold" disabled="disabled">User Verified</button>
                </div>
              <?php }?>
            </div>
          </div>
              <div class="row">
                 <div class="bio-row">
                    <p ><span>Name </span>: <?php echo $user_data['name']; ?> <!-- <span id='lblName' class="editable"></span>&nbsp -->
                     <!-- <a href="javascript:void(0)"><i id="" data-edit_id="#lblName" class=" editable_input fa btn btn-xs btn-success">edit</i></a></span>-->
                    </p>
                  </div>
                  <div class="bio-row">
                      <p><span>Sr.No </span>: <?php echo $user_data['id']; ?></p>
                  </div>

                  <div class="bio-row">
                      <p><span>Email </span>: <?php echo $user_data['email']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Recovery Email </span>: <?php echo $user_data['recovery_email']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Mobile</span>: <?php echo $user_data['c_code'] . $user_data['mobile']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Location</span>: <?php echo $user_data['location']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Date Of Birth </span>: <?php echo $user_data['dob']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Height </span>: <?php echo $user_profile_detail['height']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Religion </span>: <?php echo $user_profile_detail['religion_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Job </span>: <?php echo $user_profile_detail['job']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Education </span>: <?php echo $user_profile_detail['education']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Company </span>: <?php echo $user_profile_detail['company']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Interest </span>: <?php echo $user_profile_detail['interest']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Looking for </span>: <?php echo $user_profile_detail['looking_for_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Industry </span>: <?php echo $user_profile_detail['industry_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Education level </span>: <?php echo $user_profile_detail['education_level_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Experience </span>: <?php echo $user_profile_detail['experience']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Drinking </span>: <?php echo $user_profile_detail['drinking_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Smoking </span>: <?php echo $user_profile_detail['smoking_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Pets </span>: <?php echo $user_profile_detail['pets_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Kids </span>: <?php echo $user_profile_detail['kids_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>New to area </span>: <?php echo $user_profile_detail['new_to_area_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Relationship </span>: <?php echo $user_profile_detail['releationships_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Interest in </span>: <?php echo $user_profile_detail['interest_in']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Likes </span>: <?php echo $user_profile_detail['likes']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Dislikes </span>: <?php echo $user_profile_detail['dislikes']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Exercise </span>: <?php echo $user_profile_detail['exercise_text']; ?></p>
                  </div>
                   <div class="bio-row">
                      <p><span>Zodiac </span>: <?php echo $user_profile_detail['zodiac_text']; ?></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Date/Time of Registration </span>: <?php echo ($user_data['creation_time'] == "" ? '-' : date("d-m-Y H:i:s", $user_data['creation_time'] / 1000)); ?></p>
                  </div>
              </div>
          </div>
          <div class="bio-graph-heading"><?php $user_profile_detail['about_me'];?></div>

      </section>

      <section>
  <div class="row product-list">

    <?php
$images = $this->db->get_where('user_images', array('user_id' => $user_data['id']))->result_array();

foreach ($images as $image) {
	?>
                          <div class="col-md-3">
                              <section class="card">
                                  <div class="pro-img-box">
                                  	<div class="img">
                                      <a href="<?php echo $image['image_url']; ?>" class="image-link"><img class="img img-thumbnail" height="200" src="<?php echo $image['image_url']; ?>" style="height: 200px; width: auto;"></a>
                                       </div>
                                  </div>


                              </section>
                          </div>

          <?php }?>

                      </div>
                      
</section>


</aside>

<?php
$adminurl = AUTH_PANEL_URL;
$custum_js = <<<EOD
<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script>
$(document).ready(function() {
  $('.image-link').magnificPopup({type:'image'});
});
</script>
EOD;
echo modules::run('auth_panel/template/add_custum_js', $custum_js);
