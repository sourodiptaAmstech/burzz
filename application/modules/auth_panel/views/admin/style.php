
 <link href="<?=base_url()?>newtheme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>newtheme/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=base_url()?>newtheme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=base_url()?>newtheme/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?=base_url()?>newtheme/css/owl.carousel.css" type="text/css">

    <!--right slidebar-->
    <link href="<?=base_url()?>newtheme/css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="<?=base_url()?>newtheme/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>newtheme/css/style-responsive.css" rel="stylesheet" />
<link href="<?=base_url()?>newtheme/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />


    <script src="<?=base_url()?>newtheme/js/jquery.js"></script>
    <script src="<?=base_url()?>newtheme/js/bootstrap.bundle.min.js"></script>
    <script class="include" type="text/javascript" src="<?=base_url()?>newtheme/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=base_url()?>newtheme/js/jquery.scrollTo.min.js"></script>
    <script src="<?=base_url()?>newtheme/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?=base_url()?>newtheme/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?=base_url()?>newtheme/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?=base_url()?>newtheme/js/owl.carousel.js" ></script>
    <script src="<?=base_url()?>newtheme/js/jquery.customSelect.min.js" ></script>
    <script src="<?=base_url()?>newtheme/js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="<?=base_url()?>newtheme/js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="<?=base_url()?>newtheme/js/common-scripts5e1f.js?v=2"></script>

    <!--script for this page-->
    <script src="<?=base_url()?>newtheme/js/sparkline-chart.js"></script>
    <script src="<?=base_url()?>newtheme/js/easy-pie-chart.js"></script>
    <script src="<?=base_url()?>newtheme/js/count.js"></script>
    <script src="<?=base_url()?>newtheme/js/morris-script.js"></script>
        <script src="<?=base_url()?>newtheme/assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>newtheme/assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      $(window).on("resize",function(){
          var owl = $("#owl-demo").data("owlCarousel");
          owl.reinit();
      });

  </script>
