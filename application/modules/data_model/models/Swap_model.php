<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Swap_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/***
		 *       _____                      _    _
		 *      / ____|                    | |  | |
		 *     | (___    __ _ __   __ ___  | |  | | ___   ___  _ __
		 *      \___ \  / _` |\ \ / // _ \ | |  | |/ __| / _ \| '__|
		 *      ____) || (_| | \ V /|  __/ | |__| |\__ \|  __/| |
		 *     |_____/  \__,_|  \_/  \___|  \____/ |___/ \___||_|
		 *
		 *
	*/

	public function save_otp($data) {
		// echo '<pre>'; print_r($data); die;
		$data['creation_time'] = $data['modified_time'] = milliseconds();
		$this->db->insert('swapLimit', $data);
		$user_id = $this->db->insert_id();
		return $user_id;
	}

	public function update_user($data) {
		//$data['creation_time'] = milliseconds();
                $data['modified_time'] = milliseconds();
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->update('swapLimit', $data);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	/***
		 *       _____        _     _    _
		 *      / ____|      | |   | |  | |
		 *     | |  __   ___ | |_  | |  | | ___   ___  _ __
		 *     | | |_ | / _ \| __| | |  | |/ __| / _ \| '__|
		 *     | |__| ||  __/| |_  | |__| |\__ \|  __/| |
		 *      \_____| \___| \__|  \____/ |___/ \___||_|
		 *
		 *
	*/
	public function get_user($id) {
		$this->db->where("user_id", $id);
		return $this->db->get('swapLimit')->row_array();
	}

	public function check_otp_exist($data) {

        $email = $data['email'];
        $otp = $data['otp'];
		if ($email != '' && $otp != '') {
			$query = $this->db->query("SELECT *
									   FROM `otp`
                                       WHERE  `email`='$email' AND `otp` = '$otp' AND `status`=1");
                                       echo $query;
			return $query->num_rows();
		} else {
			return 0;
		}
	}

	public function check_mobile_exist($data) {
		$mobile = $data['mobile'];
		$query = $this->db->query("SELECT `id`,`mobile` FROM `users` WHERE `mobile`='$mobile'");
		return $query->num_rows();
	}

	public function check_social_exist($data) {
		$social_tokken = $data['social_tokken'];
		$this->db->where("social_tokken", $data['social_tokken']);
		return $this->db->get('users')->row_array();
	}

/***
 *       _____        _      _____             _                      _    _
 *      / ____|      | |    / ____|           | |                    | |  | |
 *     | |  __   ___ | |_  | |     _   _  ___ | |_  ___   _ __ ___   | |  | | ___   ___  _ __
 *     | | |_ | / _ \| __| | |    | | | |/ __|| __|/ _ \ | '_ ` _ \  | |  | |/ __| / _ \| '__|
 *     | |__| ||  __/| |_  | |____| |_| |\__ \| |_| (_) || | | | | | | |__| |\__ \|  __/| |
 *      \_____| \___| \__|  \_____|\__,_||___/ \__|\___/ |_| |_| |_|  \____/ |___/ \___||_|
 *
 *
 */
	public function get_custum_user($info) {
		$this->db->where("mobile", $info['mobile']);
		return $this->db->get('users')->row_array();
	}
	public function get_custum_username($info) {
		$this->db->where("username", $info['username']);
		return $this->db->get('users')->row_array();
	}

	public function get_user_from_email($info) {
		$this->db->where("email", $info['email']);
		return $this->db->get('users')->row_array();
	}

	public function get_master($type, $user_id) {

		$this->db->where('user_id', $user_id);
		$update_pf = $this->db->update('user_profile_detail', array('interest' => $type));

		$masters = $this->db->query("SELECT master_table,title,question,icon FROM `master_relation` WHERE find_in_set($type ,type)")->result_array();
		$i = 0;
		foreach ($masters as $mas) {
			$table_name = $mas['master_table'];
			$data[$i]['title'] = $mas['title'];
			$data[$i]['icon'] = ($mas['icon'] != '' ? base_url('icon/') . $mas['icon'] : '');
			$data[$i]['field_name'] = $table_name;
			$data[$i]['question'] = $mas['question'];
			$data[$i]['options'] = $this->db->get_where("$table_name", array('status' => 2))->result_array();
			$table_name = ($table_name == 'interest_type' ? 'interest' : $table_name);
			$data[$i]['pref'] = $this->db->get_where("user_profile_detail", array('user_id' => $user_id))->row()->$table_name;
			$i++;
		}
		/*	$data['drinking'] = $this->db->get_where('drinking', array('status' => 2))->result_array();
			$data['education_level'] = $this->db->get_where('education_level', array('status' => 2))->result_array();
			$data['industry'] = $this->db->get_where('industry', array('status' => 2))->result_array();
			$data['interest_type'] = $this->db->get_where('interest_type', array('status' => 2))->result_array();
			$data['kids'] = $this->db->get_where('kids', array('status' => 2))->result_array();
			$data['looking_for'] = $this->db->get_where('looking_for', array('status' => 2))->result_array();
			$data['new_to_area'] = $this->db->get_where('new_to_area', array('status' => 2))->result_array();
			$data['pets'] = $this->db->get_where('pets', array('status' => 2))->result_array();
			$data['releationship'] = $this->db->get_where('releationship', array('status' => 2))->result_array();
		*/

		return $data;
	}

	public function save_images_ios($data) {
		error_reporting(0);
		$images = explode(',', $data['images']);
		$id = $data['id'];
		$this->db->where('id', $id);
		$update_pf = $this->db->update('users', array('profile_picture' => str_replace("\\", '', $images[0])));
		if ($update_pf) {
			$this->db->where('user_id', $id);
			$delimages = $this->db->delete('user_images');
			if ($delimages) {
				if (count($images) > 1) {
					unset($images[0]);

					foreach ($images as $img) {
						$oth_img[] = array('image_url' => str_replace('\\', '', $img), 'user_id' => $id);
					}

					$this->db->insert_batch('user_images', $oth_img);
				}
			}
			$this->db->where('id', $id);
			$this->db->update('users', array('is_verified' => 0,'modified_time'=>milliseconds())); //set user as unverified
		}

	}

	public function save_insta_images($data) {
		error_reporting(0);
		$images = explode(',', $data['insta_images']);
		$id = $data['id'];

		$this->db->where('user_id', $id);
		$delimages = $this->db->delete('insta_images');
		if (count($images) > 0) {
			foreach ($images as $img) {
				$oth_img[] = array('insta_img_url' => str_replace('\\', '', $img), 'user_id' => $id);
			}

			$this->db->insert_batch('insta_images', $oth_img);
		}
		$this->db->where('id', $id);
		$this->db->update('users', array('is_verified' => 0,'modified_time'=>milliseconds())); //set user as unverified
	}

	public function save_images($data) {
		$images = json_decode($data['images'], true);
		//print_r($images);
		$images = array_column($images, 'image');

		$id = $data['id'];
		$this->db->where('id', $id);
		$update_pf = $this->db->update('users', array('profile_picture' => $images[0]));
		if ($update_pf) {
			$this->db->where('user_id', $id);
			$delimages = $this->db->delete('user_images');
			if ($delimages) {
				if (count($images) > 1) {
					unset($images[0]);

					foreach ($images as $img) {
						$oth_img[] = array('image_url' => $img, 'user_id' => $id);
					}

					$this->db->insert_batch('user_images', $oth_img);
				}
			}
			$this->db->where('id', $id);
			$this->db->update('users', array('is_verified' => 0,'modified_time'=>milliseconds())); //set user as unverified
		}

	}

	public function get_user_field($field,$id){
		$this->db->where("id", $id);
		return $this->db->get('users')->row()->$field;
	}

}
