<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Bhajan_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_bhajan_list($data) {
		$user_id = $data['user_id'];
		$cats = array(array("category_name" => "Trending"), array("category_name" => "Artist"), array("category_name" => "Recent View"));
		$final = [];
		$f_v = [];
		if ($cats) {
			foreach ($cats as $cat) {
				$f_v = [];
				if ($cat['category_name'] == "Trending") {
					$bhajans = $this->db->query('select bhajan.*,1 as direct_play,artist.artist_image from bhajan join artist on artist.id=bhajan.artists_id where bhajan.status=0 order by id desc limit 0,10')->result_array();
				}
				if ($cat['category_name'] == "Artist") {
					$bhajans = $this->db->query('select *,0 as direct_play,artist.artist_image from bhajan join artist on artist.id=bhajan.artists_id where bhajan.status=0 group by bhajan.artist_name limit 0,10')->result_array();
				}
				if ($cat['category_name'] == "Recent View") {
					$bhajans = $this->db->query('select bhajan.*,1 as direct_play,artist.artist_image from bhajan join artist on artist.id=bhajan.artists_id join user_meta on bhajan.id=user_meta.media_id where user_meta.type=1 and user_meta.user_id=' . $user_id . ' and bhajan.status=0 order by user_meta.creation_time desc limit 0,5')->result_array();
				}
				if ($bhajans) {
					foreach ($bhajans as $bhajan) {
						$liked = $this->is_already_like_bhajan(["user_id" => $user_id, "bhajan_id" => $bhajan['id']]);
						$bhajan['is_like'] = '0';
						if ($liked) {
							$bhajan['is_like'] = '1';
						}
						$f_v[] = $bhajan;
					}
				}
				$cat['bhajan'] = $f_v;
				$final[] = $cat;
			}
		}
		// $sql = 'select bhajan.* from bhajan join user_meta on bhajan.id=user_meta.media_id where user_meta.type=1 and user_meta.user_id='.$user_id.' order by user_meta.id desc limit 0,5';
		// $recent_view = $this->db->query($sql)->result_array();
		// if($recent_view){
		// 	$final['recent_views']=$recent_view;
		// }
		return $final;
	}

	// public function get_bhajan_list($data){
	// 	$user_id=$data['user_id'];
	// 	$cats = $this->db->get('bhajan_category')->result_array();
	// 	$final=[];
	//   		if($cats){
	//   			foreach ($cats as $cat) {
	// 			$bhajan=$this->db->where('category',$cat['id'])->get('bhajan')->result_array();
	// 			$cat['bhajan']=$bhajan;
	// 			$final[]=$cat;
	// 		}
	//   		}
	//   		// $sql = 'select bhajan.* from bhajan join user_meta on bhajan.id=user_meta.media_id where user_meta.type=1 and user_meta.user_id='.$user_id.' order by user_meta.id desc limit 0,5';
	//   		// $recent_view = $this->db->query($sql)->result_array();
	//   		// if($recent_view){
	//    	// 	$final['recent_views']=$recent_view;
	//   		// }
	// 	return $final;
	// }

	public function get_bhajan_list_by_category($data) {
		$this->db->where('category', $data['category']);
		$result = $this->db->get('bhajan')->result_array();
		return $result;
	}

	public function get_bhajan_of_artist($data) {
		$user_id = $data['user_id'];
		$artist_name = $data['artist_name'];
		$f_v = [];
		$bhajans = $this->db->query("SELECT *,1 as direct_play from bhajan where status=0 and artist_name='$artist_name' ")->result_array();
		if ($bhajans) {
			foreach ($bhajans as $bhajan) {
				$liked = $this->is_already_like_bhajan(["user_id" => $user_id, "bhajan_id" => $bhajan['id']]);
				$bhajan['is_like'] = '0';
				if ($liked) {
					$bhajan['is_like'] = '1';
				}
				$f_v[] = $bhajan;
			}
		}
		$bhajans = $f_v;

		return $bhajans;
	}

	public function get_related_guru_audios($data) {
		$user_id = $data['user_id'];
		$guru_id = $data['guru_id'];

		$f_v = array();
		$audios = $this->db->query('SELECT * from bhajan where
   								  FIND_IN_SET(' . $guru_id . ',related_guru)
   								  and status=0
   								  order by id desc
   								  limit 0,10')->result_array();
		if ($audios) {
			foreach ($audios as $audio) {
				$liked = $this->is_already_like_bhajan(["user_id" => $user_id, "bhajan_id" => $audio['id']]);
				$audio['is_like'] = '0';
				if ($liked) {
					$audio['is_like'] = '1';
				}
				$f_v[] = $audio;
			}
		}

		// if($videos){
		// 	foreach ($videos as $video) {
		// 		$liked = $this->is_already_like_video(["user_id"=>$user_id,"video_id"=>$video['id']]);
		// 		$video['is_like']= '0';
		// 		if($liked){
		// 			$video['is_like']= '1';
		// 		}
		// 		$final[] = $video;
		// 	}
		// }
		$audios = $f_v;
		return $audios;
	}

	public function is_already_like_bhajan($data) {
		$this->db->where('bhajan_id', $data['bhajan_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("bhajan_likes")->row_array();
		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function is_already_dislike_bhajan($data) {
		$this->db->where('bhajan_id', $data['bhajan_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("bhajan_dislikes")->row_array();
		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function like_bhajan($data) {

		$data['creation_time'] = milliseconds();

		$this->db->insert('bhajan_likes', $data);
		$this->db->insert_id();

		$count = $this->db->query('SELECT count(id) as total from bhajan_likes where bhajan_id =' . $data['bhajan_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['bhajan_id']);
		$this->db->set('likes', $count);
		$this->db->update("bhajan");
	}
	public function playlist($user_id, $bhajan_id, $type) {
		$creation_time = milliseconds();
		$data = array(
			'user_id' => $user_id,
			'bhajan_id' => $bhajan_id,
			'creation_time' => $creation_time,
		);
		if ($type == 1) {
			$this->db->insert('playlist', $data);
			$this->db->insert_id();
		} else {
			$count = $this->db->query('DELETE FROM playlist where user_id = ' . $user_id . ' AND bhajan_id = ' . $bhajan_id);
		}

	}
	public function unlike_bhajan($data) {
		$this->db->where('bhajan_id', $data['bhajan_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->delete("bhajan_likes");

		$count = $this->db->query('SELECT count(id) as total from bhajan_likes where bhajan_id =' . $data['bhajan_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['bhajan_id']);
		$this->db->set('likes', $count);
		$this->db->update("bhajan");

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	#######bhajan search##########
	public function get_search_bhajan_list($data) {
		$with_is_like = array();
		$search_content = $data['search_content'];
		if ($search_content != "") {
			$where_search = " and (description LIKE '%$search_content%'
   										OR title LIKE '%$search_content%'
   										OR artist_name LIKE '%$search_content%'
   										) ";
		}

		$sql = "SELECT *,1 as direct_play from bhajan where
										1=1 and
   									    status=0
   										$where_search
   										order by id desc
   										limit 0,20";
		$bhajans = $this->db->query($sql)->result_array();
		if ($bhajans) {
			foreach ($bhajans as $bhajan) {
				$liked = $this->is_already_like_bhajan(["user_id" => $data['user_id'], "bhajan_id" => $bhajan['id']]);
				$bhajan['is_like'] = '0';
				if ($liked) {
					$bhajan['is_like'] = '1';
				}
				$with_is_like[] = $bhajan;
			}
		}
		$bhajans = $with_is_like;
		return $bhajans;
	}
	#######end of bhajan search##########
	public function playlist_get($user_id) {
		$final = [];
		$f_v = [];
		$bhajans = $this->db->query('select bhajan.* from playlist as p  join   bhajan on  p.bhajan_id = bhajan.id where p.user_id =' . $user_id)->result_array();
		//print_r($bhajans);die;
		if ($bhajans) {
			foreach ($bhajans as $bhajan) {
				$liked = $this->is_already_like_bhajan(["user_id" => $user_id, "bhajan_id" => $bhajan['id']]);
				$bhajan['is_like'] = '0';
				if ($liked) {
					$bhajan['is_like'] = '1';
				}
				$f_v[] = $bhajan;
			}
		}
		$cat['bhajan'] = $f_v;
		$final[] = $cat;
		return $final;
	}

	public function get_bhajan_list_keyword($data) {
		$user_id = $data['user_id'];
		$keyword = $data['keyword'];
		$f_v = [];

		$bhajans = $this->db->query('select * from bhajan  where bhajan.status=0 AND MATCH(`title`, `description`, `artist_name`) AGAINST ("' . $keyword . '" IN NATURAL LANGUAGE MODE)  ')->result_array();

		if ($bhajans) {
			foreach ($bhajans as $bhajan) {
				$liked = $this->is_already_like_bhajan(["user_id" => $user_id, "bhajan_id" => $bhajan['id']]);
				$bhajan['is_like'] = '0';
				if ($liked) {
					$bhajan['is_like'] = '1';
				}
				$f_v[] = $bhajan;
			}
		}
		$cat['bhajan'] = $f_v;
		return $cat;

	}
}