<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Video_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	#######video search##########
	public function get_videos_by_category_search_and_video_id($data) {
		$with_is_like = array();
		$search_content = $data['search_content'];
		$last_video_id = $data['last_video_id'];
		$video_category = $data['video_category'];
		$where_cat = '';
		$where_search = '';
		$where_id = '';
		if ($last_video_id == '') {
			$sql = "SELECT * from video_category where
   									    status=0";
			$result['category'] = $this->db->query($sql)->result_array();
		}
		if ($search_content != "") {
			$where_search = " and (video_desc LIKE '%$search_content%'
   										OR video_title LIKE '%$search_content%'
   										OR author_name LIKE '%$search_content%'
   										OR tags LIKE '%$search_content%'
   										) ";
		}
		if ($last_video_id != "") {
			$where_id = " and id < $last_video_id ";
		}
		if ($video_category != "") {
			$where_cat = " and FIND_IN_SET($video_category,category) ";
		}
		$sql = "SELECT * from video_master where
										1=1 and
   									    status=0
										$where_cat
   										$where_search
   										$where_id
   										order by id desc
   										limit 0,20";
		$videos = $this->db->query($sql)->result_array();
		$todays_date = date("Y-m-d");
		$result['banners'] = $this->db->query("SELECT * from banner
									where status=0 and published_date <= '$todays_date'
									order by position asc limit 0,5")->result_array();
		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				$with_is_like[] = $video;
			}
		}
		$result['videos'] = $with_is_like;
		return $result;
	}
	#######end of video search##########
	public function get_live_channels($data) {
		$user_id = $data['user_id'];
		//live channel list
		$result = $this->db->query("SELECT * from live_channel where status=1")->result_array();
		return $result;
	}
	public function get_live_channels_keyword($data) {
		$user_id = $data['user_id'];
		$keyword = $data['keyword'];

		//$videos = $this->db->query('SELECT * FROM video_master where status=0 and  MATCH(`video_title`, `author_name`, `video_desc`, `tags`) AGAINST ("' . $keyword . '" IN NATURAL LANGUAGE MODE) ')->result_array();

		$result = $this->db->query("SELECT * from live_channel where status=1 and  MATCH(`name`, `description`) AGAINST ('" . $keyword . "' IN NATURAL LANGUAGE MODE)")->result_array();
		return $result;
	}
	public function get_home_page_videos() {

		//live channel list
		$result = $this->db->query("SELECT * from home_video order by id desc ")->row();
		return $result;
	}
	public function get_home_videos($data) {
		$user_id = $data['user_id'];

		$sql = "SELECT id,category_name as category from video_category where
   									    status=0";
		$category = $this->db->query($sql)->result_array();
		$cats = array(array("id" => "10000", "category" => "Trending Video"), array("id" => "10001", "category" => "Recently View"));
		$cats = array_merge($cats, $category);
		//pre($cats); die;
		if ($cats) {
			foreach ($cats as $cat) {
				$f_v = [];
				if ($cat['category'] == "Trending Video") {
					$videos = $this->db->query('SELECT * from video_master WHERE status=0 order by views desc limit 0,3')->result_array();
				}
				if ($cat['category'] == "Recently View") {
					$videos = $this->db->query('SELECT video_master.* from video_master join user_meta on video_master.id=user_meta.media_id where user_meta.type=2 and user_meta.user_id=' . $user_id . ' and video_master.status=0 order by user_meta.creation_time desc limit 0,3')->result_array();
				}
				if ($cat['category'] != "Recently View" and $cat['category'] != "Trending Video") {
					$cat_id = $cat['id'];
					$sql = "SELECT * from video_master where
										1=1 and
   									    status=0 and
										FIND_IN_SET($cat_id,category)
   										order by id desc
   										limit 0,3";
					$videos = $this->db->query($sql)->result_array();
				}
				if ($videos) {
					foreach ($videos as $video) {
						$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
						$video['is_like'] = '0';
						if ($liked) {
							$video['is_like'] = '1';
						}
						$video['video'] = $this->get_related_videos(array('user_id' => $user_id, 'video_id' => $video['id']));

						$f_v[] = $video;
					}
				}
				$cat['videos'] = $f_v;
				$final[] = $cat;
			}
		}
		return $final;
	}

	public function get_home_sankirtan($data) {
		$user_id = $data['user_id'];
		$cats = array(array("id" => "1", "category" => "Trending Video"), array("id" => "2", "category" => "Recently View"));
		//pre($cats); die;
		if ($cats) {
			foreach ($cats as $cat) {
				$f_v = [];
				if ($cat['id'] == "1") {
					$videos = $this->db->query("SELECT * from video_master WHERE status=0 and is_sankirtan=1 order by views desc limit 0,3")->result_array();
				}
				if ($cat['id'] == "2") {
					$videos = $this->db->query("SELECT video_master.* from video_master join user_meta on video_master.id=user_meta.media_id where user_meta.type=2 and user_meta.user_id=$user_id and video_master.status=0 and video_master.is_sankirtan=1 order by user_meta.creation_time desc limit 0,3")->result_array();
				}
				if ($videos) {
					foreach ($videos as $video) {
						$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
						$video['is_like'] = '0';
						if ($liked) {
							$video['is_like'] = '1';
						}
						$f_v[] = $video;
					}
				}
				$cat['videos'] = $f_v;
				$final[] = $cat;
			}
		}
		return $final;
	}

	public function home_page_view_all_videos($data) {
		$with_is_like = array();
		$total_received_count = $data['total_received_count'];
		$user_id = $data['user_id'];
		$video_category = $data['video_category'];
		// $where_id='';
		//       if($last_video_id != ""){
		//           $where_id  = " and video_master.id < $last_video_id ";
		//       }
		if ($total_received_count == "") {
			$total_received_count = 0;
		}
		if ($video_category == "10000") {

			$videos = $this->db->query("SELECT * from video_master where 1=1 and status=0 order by views desc limit $total_received_count,10")->result_array();
		}
		if ($video_category == "10001") {
			$videos = $this->db->query("SELECT video_master.* from video_master join user_meta on video_master.id=user_meta.media_id where 1=1 and video_master.status=0 and user_meta.type=2 and user_meta.user_id=$user_id order by user_meta.creation_time desc limit $total_received_count,10")->result_array();
		}
		if ($video_category != "10000" and $video_category != "10001") {
			$videos = $this->db->query("SELECT * from video_master where
										1=1 and
   									    status=0 and
										FIND_IN_SET($video_category,category)
   										order by id desc
   										limit $total_received_count,10")->result_array();
		}
		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				$with_is_like[] = $video;
			}
		}
		$result['videos'] = $with_is_like;
		return $result;
	}

	public function view_all_sankirtan_by_category($data) {
		$with_is_like = array();
		$total_received_count = $data['total_received_count'];
		$user_id = $data['user_id'];
		$video_category = $data['video_category'];
		$videos = '';
		if ($total_received_count == "") {
			$total_received_count = 0;
		}
		if ($video_category == "1") {
			$videos = $this->db->query("SELECT * from video_master where status=0 and is_sankirtan=1 order by views desc limit $total_received_count,10")->result_array();
		}
		if ($video_category == "2") {
			$videos = $this->db->query("SELECT video_master.* from video_master join user_meta on video_master.id=user_meta.media_id where user_meta.type=2 and user_meta.user_id=$user_id and video_master.status=0 and video_master.is_sankirtan=1 order by user_meta.creation_time desc limit $total_received_count,10")->result_array();
		}
		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				$with_is_like[] = $video;
			}
		}
		$result['videos'] = $with_is_like;
		return $result;
	}

	public function update_recent_view_video($data) {
		$data['creation_time'] = milliseconds();
		$this->db->where('fk_user_id', $data['fk_user_id']);
		$this->db->order_by("id", "desc");
		$result = $this->db->get('video_view')->result_array();
		//echo '<pre>'; print_r($result); die;
		if ($data['fk_video_id'] == $result[0]['fk_video_id']) {
			$this->db->where('id', $result[0]['id']);
			$this->db->update('video_view', $data);
			$updated_status = $this->db->affected_rows();
			if ($updated_status) {
				return $result[0]['id'];
			}
		} else {
			$this->db->insert('video_view', $data);
			return $this->db->insert_id();
		}
	}

	public function get_related_guru_videos($data) {
		$user_id = $data['user_id'];
		$guru_id = $data['guru_id'];
		$final = [];
		$videos = $this->db->query('SELECT * from video_master where
   								  FIND_IN_SET(' . $guru_id . ',related_guru)
   								  and status=0
   								  order by id desc
   								  limit 0,10')->result_array();

		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $user_id, "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				$final[] = $video;
			}
		}
		return $final;
	}

	public function get_related_videos($data) {
		$user_id = $data['user_id'];
		$video_id = $data['video_id'];
		$final = [];
		$sql = 'SELECT *
				from video_master
				where match(video_master.tags) against((select tags
				from video_master
				where id = ' . $video_id . ' ) in boolean mode)
				and id != ' . $video_id . '
				limit 0,4';
		$videos = $this->db->query($sql)->result_array();
		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $user_id, "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				$final[] = $video;
			}
		}
		return $final;
	}

	public function view_video($data) {

		$data['creation_time'] = milliseconds();
		$this->db->insert('video_views', $data);
		$this->db->insert_id();

		$count = $this->db->query('SELECT count(id) as total from video_views where video_id =' . $data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['video_id']);
		$this->db->set('views', $count);
		$this->db->update("video_master");

	}

	public function is_already_view_video($data) {
		$this->db->where('video_id', $data['video_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("video_views")->row_array();
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function like_video($data) {

		$data['creation_time'] = milliseconds();

		$this->db->insert('video_likes', $data);
		$this->db->insert_id();

		$count = $this->db->query('SELECT count(id) as total from video_likes where video_id =' . $data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['video_id']);
		$this->db->set('likes', $count);
		$this->db->update("video_master");
	}

	public function is_already_like_video($data) {
		$this->db->where('video_id', $data['video_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->get("video_likes")->row_array();
		if ($result) {
			return TRUE;
		}
		return FALSE;
	}

	public function unlike_video($data) {
		$this->db->where('video_id', $data['video_id']);
		$this->db->where('user_id', $data['user_id']);
		$result = $this->db->delete("video_likes");

		$count = $this->db->query('SELECT count(id) as total from video_likes where video_id =' . $data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['video_id']);
		$this->db->set('likes', $count);
		$this->db->update("video_master");

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_meta_count_of_media_and_user($data) {
		$user_id = $data['user_id'];
		$type = $data['type'];
		$id = $data['media_id'];
		$result = $this->db->query('SELECT count(id) as total from user_meta where user_id=' . $user_id . ' and type=' . $type . ' and media_id=' . $id)->result_array();
		//print_r($result); die;
		return $result[0]['total'];

	}

	public function update_user_meta($data) {
		$this->db->where('type', $data['type']);
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('media_id', $data['media_id']);
		$result = $this->db->update('user_meta', $data);
		if ($result) {
			return true;
		}
		return false;

	}
	public function insert_user_meta($data) {
		$result = $this->db->insert('user_meta', $data);
		if ($result) {
			return true;
		}
		return false;

	}

	public function add_comment($data) {

		$data['time'] = milliseconds();

		$this->db->insert('video_comments', $data);
		$comment_id = $this->db->insert_id();

		$count = $this->db->query('select count(id) as total from video_comments where video_id =' . $data['video_id'])->row_array();
		$count = ($count && $count['total'] > 0) ? $count['total'] : 0;

		$this->db->where('id', $data['video_id']);
		$this->db->set('comments', $count);
		$this->db->update("video_master");
		/* check if comment is child of another comment */
		return $comment_id;
	}

	public function get_post_comment($data) {
		$where = "";
		if ($data['last_comment_id'] != "") {$where = " And video_comments.id > " . $data['last_comment_id'];}

		$query = $this->db->query(" SELECT video_comments.id AS id,
									video_id, user_id,
									comment , time,
									users.username AS name, users.profile_picture AS profile_picture
							FROM `video_comments`
							JOIN users ON video_comments.user_id = users.id
							WHERE video_comments.video_id = " . $data['video_id'] . "
							$where
							ORDER BY video_comments.id ASC
							LIMIT 0 , 10"
		);
		return $query->result_array();

	}

	public function get_banners($data) {
		$todays_date = date("Y-m-d");
		$query = $this->db->query("SELECT * from banner
									where status=0 and published_date <= '$todays_date'
									order by position asc limit 0,5"
		);
		return $query->result_array();

	}

// public function get_home_videos($data){
	// 	$user_id=$data['user_id'];
	// 	$this->db->order_by("id", "desc");
	// 	$videos=$this->db->get('video_master')->result_array();
	// 	//echo '<pre>'; print_r($videos); die;
	// 	if($videos){
	// 		foreach ($videos as $video) {
	// 			$liked = $this->is_already_like_video(["user_id"=>$user_id,"video_id"=>$video['id']]);
	// 			$video['is_like']= '0';
	// 			if($liked){
	// 				$video['is_like']= '1';
	// 			}
	// 			$final['trending_video'][] = $video;
	// 		}
	// 	}
	// 	$recent_videos=$this->db->query('select video_master.* from video_master join user_meta on video_master.id=user_meta.media_id where user_meta.type=2 and user_meta.user_id='.$user_id.' order by user_meta.id desc limit 0,5')->result_array();
	// 	if($recent_videos){
	// 		foreach ($recent_videos as $recent) {
	// 			$liked = $this->is_already_like_video(["user_id"=>$user_id,"video_id"=>$recent['id']]);
	// 			$recent['is_like']= '0';
	// 			if($liked){
	// 				$recent['is_like']= '1';
	// 			}
	// 			$final['recent_view'][] = $recent;
	// 		}
	// 	}
	// 	return $final;
	// }

// public function get_videos_by_category($data){
	// 	$with_is_like[]='';
	// 	$search_content=$data['search_content'];
	// 	$video_category=$data['video_category'];
	// 	$last_video_id=$data['last_video_id'];
	// 	if($search_content=='' and $last_video_id==''){
	// 		$sql =  "select * from video_master where
	//   										FIND_IN_SET($video_category,category) and status=0
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}
	// 	if($search_content=='' and $last_video_id!=''){
	// 		$sql =  "select * from video_master where
	//   										FIND_IN_SET($video_category,category) and status=0 and id >$last_video_id
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}
	// 	if($search_content!='' and $last_video_id==''){
	// 		$sql =  "select * from video_master where
	//   										FIND_IN_SET($video_category,category) and status=0
	//   										and (video_desc LIKE '%$search_content%'
	//   										OR video_title LIKE '%$search_content%')
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}
	// 	if($search_content!='' and $last_video_id!=''){
	// 		$sql =  "select * from video_master where
	//   										FIND_IN_SET($video_category,category) and status=0
	//   										and id > $last_video_id
	//   										and (video_desc LIKE '%$search_content%'
	//   										OR video_title LIKE '%$search_content%')
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}

// 	if($videos){
	// 		foreach ($videos as $video) {
	// 			$liked = $this->is_already_like_video(["user_id"=>$data['user_id'],"video_id"=>$video['id']]);
	// 			$video['is_like']= '0';
	// 			if($liked){
	// 				$video['is_like']= '1';
	// 			}
	// 			$with_is_like[] = $video;
	// 		}
	// 	}
	// 	$videos=$with_is_like;
	// 	$result['videos']=$videos;
	// 	return $result;
	// }

// public function get_videos_without_category($data){
	// 	$with_is_like[]='';
	// 	$search_content=$data['search_content'];
	// 	$last_video_id=$data['last_video_id'];
	// 	if($search_content!='' and $last_video_id==''){
	// 		$sql =  "select * from video_master where
	//   									    status=0
	//   										and (video_desc LIKE '%$search_content%'
	//   										OR video_title LIKE '%$search_content%')
	//   										order by id desc
	//   										limit 0,20";
	// 	}
	// 	if($search_content=='' and $last_video_id!=''){
	// 		$sql =  "select * from video_master where
	//   										status=0 and id >$last_video_id
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}
	// 	if($search_content!='' and $last_video_id!=''){
	// 		$sql =  "select * from video_master where
	//   										status=0
	//   										and (video_desc LIKE '%$search_content%'
	//   										OR video_title LIKE '%$search_content%')
	//   										and id >$last_video_id
	//   										order by id desc
	//   										limit 0,20";
	// 		$videos = $this->db->query($sql)->result_array();
	// 	}

// 	if($videos){
	// 		foreach ($videos as $video) {
	// 			$liked = $this->is_already_like_video(["user_id"=>$data['user_id'],"video_id"=>$video['id']]);
	// 			$video['is_like']= '0';
	// 			if($liked){
	// 				$video['is_like']= '1';
	// 			}
	// 			$with_is_like[] = $video;
	// 		}
	// 	}
	// 	$videos=$with_is_like;
	// 	$result['videos']=$videos;
	// 	return $result;
	// }
	// public function get_all_videos_with_category($data){
	// 		$result['category'] = $this->db->get('video_category')->result_array();
	// 		$videos=$this->db->query('select * from video_master where status=0
	//    										order by id desc
	//    										limit 0,20')->result_array();
	// 		if($videos){
	// 			foreach ($videos as $video) {
	// 				$liked = $this->is_already_like_video(["user_id"=>$data['user_id'],"video_id"=>$video['id']]);
	// 				$video['is_like']= '0';
	// 				if($liked){
	// 					$video['is_like']= '1';
	// 				}
	// 				$with_is_like[] = $video;
	// 			}
	// 		}
	// 		$videos=$with_is_like;
	// 		$result['videos']=$videos;
	// 		return $result;
	// 	}

// public function get_videos($data){
	// 	$cats = $this->db->get('video_category')->result_array();
	//   		if($cats){
	//   			foreach ($cats as $cat) {
	//   				$f_v = [];
	//   				$query=$this->db->query('SELECT * from video_master where
	//   										FIND_IN_SET('.$cat['id'].',category) and status=0
	//   										order by id desc
	//   										limit 0,5');
	// 			$videos=$query->result_array();
	// 			if($videos){
	// 				foreach ($videos as $video) {
	// 					$liked = $this->is_already_like_video(["user_id"=>$data['user_id'],"video_id"=>$video['id']]);
	// 					$video['is_like']= '0';
	// 					if($liked){
	// 						$video['is_like']= '1';
	// 					}
	// 					$f_v[] = $video;
	// 				}
	// 			}
	// 			$cat['videos']=$f_v;
	// 			$final[]=$cat;
	// 		}
	//   		}
	// 	return $final;
	// }
	public function get_home_videos_keyword($data) {

		$keyword = $data['keyword'];

		$videos = $this->db->query('SELECT * FROM video_master where status=0 and  MATCH(`video_title`, `author_name`, `video_desc`, `tags`) AGAINST ("' . $keyword . '" IN NATURAL LANGUAGE MODE) ')->result_array();

		$f_v = [];

		if ($videos) {
			foreach ($videos as $video) {
				$liked = $this->is_already_like_video(["user_id" => $data['user_id'], "video_id" => $video['id']]);
				$video['is_like'] = '0';
				if ($liked) {
					$video['is_like'] = '1';
				}
				//$video['video'] = $this->get_related_videos(array('user_id' =>$data['user_id'], 'video_id' => $video['id']));

				$f_v[] = $video;
			}

		}
		$cat['videos'] = $f_v;
		return $cat;
	}

}
