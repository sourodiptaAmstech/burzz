<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Plan_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_plan_list($data) {
		return $this->db->get_where("packages", array('status' => 2))->result_array();
	}

}