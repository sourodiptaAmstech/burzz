<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('guru_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	public function get_guru_list() {
		//last_guru_id
		$this->validate_guru_list();
		$user_id = $this->input->post('user_id');
		if ($user_id) {
			$result = $this->guru_model->get_guru_list($this->input->post());
			return_data(true, 'Success.', $result);
		} else {
			return_data(false, 'User ID required.', array());
		}

	}
	public function search_guru() {
		//last_guru_id

		$user_id = $this->input->post('user_id');
		$keyword = $this->input->post('keyword');
		if ($user_id) {
			$result = $this->guru_model->get_guru_list_keyword(array('user_id' => $user_id, 'keyword' => $keyword));
			return_data(true, 'Success.', $result);
		} else {
			return_data(false, 'User ID required.', array());
		}

	}

	private function validate_guru_list() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function follow_guru() {

		$this->validate_follow_guru();
		// first check if already follow
		if ($this->guru_model->is_already_follow_guru($this->input->post())) {
			return_data(false, 'User Already Followed.');
		}

		$this->guru_model->guru_follow($this->input->post());
		return_data(true, 'User Followed.');
	}

	public function unfollow_guru() {

		$this->validate_follow_guru();
		$this->guru_model->unfollow_guru($this->input->post());

		return_data(true, 'Unfollow Successfully.');
	}

	public function like_guru() {

		$this->validate_follow_guru();
		// first check if already follow
		if ($this->guru_model->is_already_like_guru($this->input->post())) {
			return_data(false, 'User Already Liked.');
		}

		$this->guru_model->guru_like($this->input->post());
		return_data(true, 'User Liked.');
	}

	public function unlike_guru() {

		$this->validate_follow_guru();
		$this->guru_model->unlike_guru($this->input->post());

		return_data(true, 'Unlike Successfully.');
	}

	private function validate_follow_guru() {

		post_check();

		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('guru_id', 'guru_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

}