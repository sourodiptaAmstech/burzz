<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paystack extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("services");
        $this->load->library('session');
    }
    
    
    public function webhook() { 
        // only a post with paystack signature header gets our attention        
        if ((strtoupper($_SERVER['REQUEST_METHOD']) != 'POST' ) || !array_key_exists('x-paystack-signature', $_SERVER)) {
            $event  = @file_get_contents("php://input");
            
            $sql    = "INSERT INTO webhooks SET response = '".$event."', created='".date('Y-m-d H:i:s')."'";
            $this->db->query($sql);

            $event = json_decode($event,true);
            if (isset($event) && isset($event['event']) && $event['event'] == 'charge.success') {
                $this->db->where('pre_transaction_id', $event['data']['reference']);
                $this->db->update('transaction_record', array('post_transaction_id' => $event['data']['id'], 'transaction_status' => 1));
            }
            if (isset($event) && $event['event'] == 'subscription.create') {
                $array = array(
                    'user' => $event['data']['customer']['email'],
                    'plan' => $event['data']['plan']['plan_code'],
                    'subscription_code' => $event['data']['subscription_code'],
                    'email_token' => $event['data']['email_token'],
                    'payment_date' => $event['data']['created_at'],
                    'next_payment_date' => $event['data']['created_at'],
                    'status' => 1
                );
                $this->db->insert('user_subscription', $array);
            }
           
        } else {
            
        }
        // validate event do all at once to avoid timing attack
        if ($_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] !== hash_hmac('sha512', $input, PAYSTACK_SECRET_KEY))
            exit();

        http_response_code(200);

        
        exit();
    }


}
    