<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Setting_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	public function get_terms_conditions() {

		$url = base_url() . 'terms.html';

		return_data(true, 'Success.', array('url' => $url));

	}
	public function get_privacy_policy() {

		$url = base_url() . 'privacy.html';

		return_data(true, 'Success.', array('url' => $url));

	}

	public function live() {
		$live = $this->db->get('live')->row_array();
		return_data(true, 'Success.', array('golive' => $live['live']));

	}

}