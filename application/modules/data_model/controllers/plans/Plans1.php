<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans1 extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('plan_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	public function get_plan_list() {
		$this->validate_plan_list();
		$user_id = $this->input->post('user_id');
		if ($user_id) {
			$result = $this->plan_model->get_plan_list($this->input->post());
			return_data(true, 'Success.', $result);
		} else {
			return_data(false, 'User ID required.', array());
		}

	}

	private function validate_plan_list() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	private function validate_initialize_transaction() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('package_id', 'package_id', 'trim|required');
		$this->form_validation->set_rules('package_price', 'package_price', 'trim|required');
		$this->form_validation->set_rules('currency', 'currency', 'trim|required');
		$this->form_validation->set_rules('pay_via', 'pay_via', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
		if ($this->input->get('pay_via') != "") {
			if ($this->input->get('pay_via') !== "PAY_STACK" || $this->input->get('pay_via') !== "FLUTTER_WAVE" || $this->input->get('PAY_PAL') !== "PAY_PAL") {
				return_data(false, "Please provide valid payment gateway !", array(), array());
			}

		}
	}

	public function initialize_transaction() {
		$this->validate_initialize_transaction();
		$user_id = $this->input->post('user_id');
		$package_id = $this->input->post('package_id');
		$key = rand(111, 999) . substr(md5(time()), 5);
		$cv = $this->db->select('validity')->where('id', $package_id)->get('packages')->row();

		$array = array(
			'user_id' => $user_id,
			'package_id' => $package_id,
			'pre_transaction_id' => $key,
			'package_price' => $this->input->post('package_price'),
			'currency' => $this->input->post('currency'),
			'creation_time' => milliseconds(),
		);
		if ($cv->validity > 0) {
			$array['validity'] = $array['creation_time'] + ($cv->validity * 24 * 60 * 60 * 1000);
		}
		/*
			         * get course instructor and add his share w.r.t. course
		*/

		if ($this->input->post('pay_via') == "PAY_STACK") {
			$array['pay_via'] = 'PAY_STACK';
		}
		if ($this->input->post('pay_via') == "FLUTTER_WAVE") {
			$array['pay_via'] = 'FLUTTER_WAVE';
		}
		if ($this->input->post('pay_via') == "PAY_PAL") {
			$array['pay_via'] = 'PAY_PAL';
		} else {
			$array['pay_via'] = 'OTHER';
		}

		$this->db->insert('transaction_record', $array);
		return_data(true, "Payment initialized.", array('pre_transaction_id' => $key));
	}

	private function validate_complete_transaction() {
		post_check();
		$this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
		$this->form_validation->set_rules('pre_transaction_id', 'pre_transaction_id', 'trim|required');
		$this->form_validation->set_rules('post_transaction_id', 'post_transaction_id', 'trim|required');
		//$this->form_validation->set_rules('package_id', 'package_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if ($error) {
			return_data(false, array_values($error)[0], array(), $error);
		}
	}

	public function complete_transaction() {
		$this->validate_complete_transaction();
		$user_id = $this->input->post('user_id');
		$pre_transaction_id = $this->input->post('pre_transaction_id');
		$post_transaction_id = $this->input->post('post_transaction_id');
		$array = array(
			'post_transaction_id' => $post_transaction_id,
			'transaction_status' => 1,
		);

		$this->db->where('pre_transaction_id', $pre_transaction_id);
		$this->db->update('transaction_record', $array);

		/* update learner for this course we have course id */

		/* send message on mobile about it */
		//modules::run("data_model/user/mobile_auth/send_message_on_purchase", array('user_id' => $user_id));
		/* send message on email about it */
		//modules::run("data_model/emailer/payment_email/payment_done", array('user_id' => $user_id, 'pre_transaction_id' => $pre_transaction_id));

		return_data(true, "Payment complete.", array());
	}

}