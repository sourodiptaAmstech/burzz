<?php //live
defined('BASEPATH') OR exit('No direct script access allowed');//live

class Subscription extends MX_Controller {//live
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper("services");
		$this->load->library('session');
		$this->load->model("notification_model");
	}

    public function disable_subscription(){
        $this->validate_disable_subscription();
        $input = $this->input->post();
        $user = $this->db->get_where('users',array('id'=>$input['user_id']))->row_array();
        $this->db->where('user',$user['email']);
        $this->db->where('plan',$input['plan_code']);
        $this->db->where('user_subscription.status',1);
        $this->db->order_by('id','desc');
        $exist         = $this->db->get('user_subscription')->row_array();
        if(!$exist){
            return_data(false, "No plan subscribed", array());
        }
        $input['code']  =  $exist['subscription_code'];
        $input['token'] =  $exist['email_token'];
        $ch = curl_init("https://api.paystack.co/subscription/disable");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Bearer '. PAYSTACK_SECRET_KEY));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result   = json_decode($result);
        if($result){
            if($result->status){
                $this->db->where('id',$exist['id']);
                $this->db->update('user_subscription',array('status'=>2));
                $plan = $this->db->get_where('packages',array('plan_code'=>$input['plan_code']))->row_array();
                $this->db->where('user_id',$user['id']);
                $this->db->where('package_id',$plan['id']);
                $this->db->update('transaction_record',array('plan_status'=>2));
            }
           return_data(true, $result->message,array());
        }
    }

    private function validate_disable_subscription() {
        post_check();
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        $this->form_validation->set_rules('plan_code', 'plan_code', 'trim|required');
        $this->form_validation->run();
        $error = $this->form_validation->get_all_errors();
        if ($error) {
            return_data(false, array_values($error)[0], array(), $error);
        }
    }
}
