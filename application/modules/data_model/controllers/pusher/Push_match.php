<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_match extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper("push_helper");
	}
	/*****************************   PUSH CODE FROM 101 ******************************/

	public function push_on_interest($data) {

		//print_r($data);
		$query = "SELECT * FROM users WHERE users.id = " . $data[0];
		$users = $this->db->query($query)->result_array();
		//print_r($users);

		foreach ($users as $data) {
			$push_data = json_encode(
				array(
					'notification_code' => 100,
					'message' => $data['name'] . " has liked you!",
					'data' => '', //array('post_id' => $post_id),
				)
			);

			$token = $data['device_token'];
			$device = $data['device_type'];
			if ($data['notification'] != 1) {
				$result = generatePush($device, $token, $push_data);
			}

		}

	}

	public function push_on_match($data) {

		//print_r($data);
		$query = "SELECT * FROM users WHERE users.id in (" . $data[0] . "," . $data[1] . ")";
		$users = $this->db->query($query)->result_array();
		//print_r($users);

		foreach ($users as $data) {
			$push_data = json_encode(
				array(
					'notification_code' => 101,
					'message' => "You got a new match.",
					'data' => '', //array('post_id' => $post_id),
				)
			);

			$token = $data['device_token'];
			$device = $data['device_type'];
			if ($data['notification'] != 1) {
				$result = generatePush($device, $token, $push_data);
			}
		}

	}

	public function push_on_chat($input) {
		if ($input[0]['type'] == 'image') {
			$input[0]['msg'] = 'image';
		}

		$push_data = json_encode(
			array(
				'notification_code' => 109,
				'message' => $input[0]['msg'],
				'sender_name' => $input[2]['name'],
				'sender_id' => $input[2]['id'],
				'data' => array('recipient' => $input[1], 'type' => $input[0]['type']),
			)
		);
		//die;
		$token = $input[1]['device_token'];
		$device = $input[1]['device_type'];
		if ($input[1]['notification'] != 1) {
			$result = generatePush($device, $token, $push_data);
		}
	}

	public function ios($tokken = "") {
		$push_data = json_encode(
			array(
				'notification_code' => 101,
				'message' => "testing of ios push",
				'data' => array('post_id' => 1),
			)
		);
		$token = $tokken;
		$device = "ios";
		generatePush($device, $token, $push_data);

	}

}