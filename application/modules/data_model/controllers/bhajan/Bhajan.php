<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bhajan extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('bhajan_model');
		$this->load->helper("services");
		$this->load->library('session');
	}

	public function get_bhajan_list(){
		$this->validate_bhajan_list();
		$user_id=$this->input->post('user_id');
		$result=$this->bhajan_model->get_bhajan_list(array('user_id'=>$user_id));
		if($result){
			return_data(true,'Success.',$result);
		}else{
			return_data(false,'Failed.',array());
		}
		
	}

	private function validate_bhajan_list(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function search_bhajan(){
		$this->validate_search_bhajan();
		$user_id=$this->input->post('user_id');
		$search_content=$this->input->post('search_content');
		$result=$this->bhajan_model->get_search_bhajan_list(array('user_id'=>$user_id,'search_content'=>$search_content));
		return_data(true,'Success.',$result);
		
	}

	private function validate_search_bhajan(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('search_content','search_content', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	public function get_bhajan_list_by_category(){
		$this->validate_bhajan_list_by_category();
		$user_id=$this->input->post('user_id');
		$category=$this->input->post('category');
		$result=$this->bhajan_model->get_bhajan_list_by_category(array('user_id'=>$user_id,'category'=>$category));
		if($result){
			return_data(true,'Success.',$result);
		}else{
			return_data(false,'Failed.',array());
		}
		
	}

	private function validate_bhajan_list_by_category(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('category','category', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_related_guru_audios(){ //user_id,guru_id
		$this->validate_get_related_guru_audios();
		$user_id=$this->input->post('user_id');
		$guru_id=$this->input->post('guru_id');
		$result=$this->bhajan_model->get_related_guru_audios(array('user_id'=>$user_id,'guru_id'=>$guru_id));
		return_data(true,'Success.',$result);	
		
	}

	private function validate_get_related_guru_audios(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('guru_id','guru_id', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function like_bhajan(){ //user_id,bhajan_id

		$this->validate_like_bhajan();
		// first check if already view
		if($this->bhajan_model->is_already_like_bhajan($this->input->post())){
			return_data(false,'User Already liked.');
		}

		$this->bhajan_model->like_bhajan($this->input->post());						
		return_data(true,'User liked.');
	}

	public function unlike_bhajan(){

		$this->validate_like_bhajan();
		$this->bhajan_model->unlike_bhajan($this->input->post());

		return_data(true,'Unlike Successfully.');
	}


	private function validate_like_bhajan(){

		post_check();

		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('bhajan_id','bhajan_id', 'trim|required');

		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}

	public function get_bhajan_of_artist(){
		$this->validate_get_bhajan_of_artist();
		$user_id=$this->input->post('user_id');
		$artist_name=$this->input->post('artist_name');
		$result=$this->bhajan_model->get_bhajan_of_artist(array('user_id'=>$user_id,'artist_name'=>$artist_name));
		if($result){
			return_data(true,'Success.',$result);
		}
		
	}

	private function validate_get_bhajan_of_artist(){
		post_check();
		$this->form_validation->set_rules('user_id','user_id', 'trim|required');
		$this->form_validation->set_rules('artist_name','artist_name', 'trim|required');
		$this->form_validation->run();
		$error = $this->form_validation->get_all_errors();

		if($error){
			return_data(false,array_values($error)[0],array(),$error);
		}
	}


	

}