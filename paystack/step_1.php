<?php
#INITIALIZE
$result = array();
//Set other parameters as keys in the $postdata array

$input = $_POST;
$postdata =  array('email' => $input['email'], 'amount' => $input['amount'],"reference" => $input['reference']);

$url = "https://api.paystack.co/transaction/initialize";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
  'Authorization: Bearer sk_test_fbc719564aa816cdb60d1469aaee8e7d6c6ff685',
  'Content-Type: application/json',

];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$request = curl_exec ($ch);

curl_close ($ch);

if ($request) {
 $result = json_decode($request, true);
}
print_r($result);
//Use the $result array to get redirect URL